//
//  MTC163NewsViewController.m
//  MTCProject-A
//
//  Created by dllo on 16/6/14.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTC163NewsViewController.h"
#import "MTCPlayerViewController.h"
#import "MTC163NewsTVCOOfBasic.h"
#import "MTCSearchViewController.h"
#import "Masonry.h"
#import "MTC163NewsChannelModel.h"
#import "MTCHotNewsViewController.h"
#import "MTCHeadNewsViewController.h"

@interface MTC163NewsViewController ()<UICollectionViewDelegate, UICollectionViewDataSource>

//频道
@property (nonatomic, retain) UIView *titleView;
//新闻
@property (nonatomic, retain) UIScrollView *scrollView;
//频道数据

@property (nonatomic, strong) NSArray *channelArr;
//现有的频道
@property (nonatomic, strong) NSMutableArray *channelNowArr;
//删除的频道
@property (nonatomic, strong) NSMutableArray *channelDelArr;


@end

@implementation MTC163NewsViewController

- (void)dealloc{
    [super dealloc];
    [_scrollView release];
    [_channelArr release];
    [_channelNowArr release];
    [_channelDelArr release];
    [_titleView release];

}





- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{

    self= [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        [self createTitleView];
    }
    return self;


}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self creatScollView];
    [self configTabBarItem];
    [self scrollViewConfig];
    

    

}
#pragma make - 标题 搜索和直播
- (void)createTitleView {
    
    UINavigationBar *bar = [UINavigationBar appearance];
    
    bar.barTintColor = [UIColor redColor];
    self.titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 70)];
    
    [self.view addSubview:self.titleView];
    
}
- (void)creatScollView {
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 104, SCREEN_WIDTH, SCREEN_HEIGHT - 104 - 49)];
    
    [self.view addSubview:self.scrollView];

    self.scrollView.pagingEnabled = YES;
    self.scrollView.showsHorizontalScrollIndicator = NO;
   
    self.scrollView.bounces = NO;
    

}
- (void)scrollViewConfig {

    MTC163NewsTVCOOfBasic *fieNewsVC = [[MTC163NewsTVCOOfBasic alloc] init];
    MTCHotNewsViewController *hotNewsVC = [[MTCHotNewsViewController alloc] init];
    MTCHeadNewsViewController *headNewsVC = [[MTCHeadNewsViewController alloc] init];
    
    self.scrollView.contentSize = CGSizeMake(SCREEN_WIDTH * 3, SCREEN_HEIGHT - 49 - 104);
    fieNewsVC.view.frame = self.scrollView.bounds;
    [self.scrollView addSubview:fieNewsVC.view];
    
 
    
    hotNewsVC.view.frame = CGRectMake(SCREEN_WIDTH, 0, SCREEN_WIDTH, CGRectGetHeight(self.scrollView.bounds));
    [self.scrollView addSubview:hotNewsVC.view];

    
    headNewsVC.view.frame = CGRectMake(SCREEN_WIDTH * 2, 0, SCREEN_WIDTH, CGRectGetHeight(self.scrollView.bounds));
    [self.scrollView addSubview:headNewsVC.view];
  
    
    [self addChildViewController:fieNewsVC];
    [self addChildViewController:hotNewsVC];
    [self addChildViewController:headNewsVC];
    

}


- (void)configTabBarItem {
    self.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"新闻" image:[UIImage imageNamed:@"News.png"] tag:100];
    
    self.navigationItem.title = @"網易";
    
    UIBarButtonItem *item1 = [[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"vidicon.png"] style:UIBarButtonItemStylePlain target:self action:@selector(handleVidicon:)] autorelease];
    
    UIBarButtonItem *item2 = [[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"search.png"] style:UIBarButtonItemStylePlain target:self action:@selector(handleSearch:)] autorelease];
    
    
    self.navigationItem.rightBarButtonItems = @[item1, item2];
    
    
}
- (void)handleVidicon:(UIBarButtonItem *)barbutton {

    MTCPlayerViewController *pvc = [[MTCPlayerViewController alloc] init];
    
    [self.navigationController pushViewController:pvc animated:YES];
    
    [pvc release];


}
- (void)handleSearch:(UIBarButtonItem *)barbutton{
    MTCSearchViewController *svc = [[MTCSearchViewController alloc] init];
    
    [self.navigationController pushViewController:svc animated:YES];
    
    [svc release];
 
}












@end
