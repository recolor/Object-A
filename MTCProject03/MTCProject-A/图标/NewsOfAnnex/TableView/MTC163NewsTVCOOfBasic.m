//
//  MTC163NewsTVCOOfBasic.m
//  MTCProject-A
//
//  Created by dllo on 16/6/15.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTC163NewsTVCOOfBasic.h"
#import "NetworkingHandler.h"
#import "MTC163NewsNormalCell.h"
#import "MTC163News3PicCell.h"
#import "MTC163NewsChannelModel.h"
#import "UIImageView+WebCache.h"
#import "MTC163NewsBigCell.h"

@interface MTC163NewsTVCOOfBasic ()
@property (nonatomic, retain) NSMutableArray *newsArr;
@end

@implementation MTC163NewsTVCOOfBasic

- (void)viewDidLoad {
    [super viewDidLoad];

    [self tableViewConfig];
    [self handleData];

    
}
- (void)handleData {
    
    self.newsArr = [NSMutableArray array];
    
    NSString *str = @"http://c.m.163.com/nc/article/headline/T1348647909107/0-20.html?from=toutiao&size=20&prog=LTitleA&fn=2&passport=&devId=1srtuLBfZ2iAHaF2R6bm5w%3D%3D&lat=Kcfy7ystb3A%2Bg%2FlxxbvrcA%3D%3D&lon=CZjZGc52u767xrx19rrUhw%3D%3D&version=10.0&net=wifi&ts=1464855170&sign=EeAS5QkNPYxYIXOqRj0w8Y10pnc1sPYkUVaaPUZdad148ErR02zJ6%2FKXOnxX046I&encryption=1&canal=miliao_news&mac=wWH4%2FMNJ4iOohL%2BSaWBtA0n4xdWy8S3keUAmEYPgEfc%3D";
    
    NetworkingHandler *nt = [[NetworkingHandler alloc] init];
    
    [nt netWorkingHandlerGETWithURL:str completion:^(id result, NSData *data, NSURLResponse *response, NSError *error) {
        
        NSArray *arr = [result objectForKey:@"T1348647909107"];
        
        for (NSDictionary *dic in arr) {
            if (![dic objectForKey:@"ads"]) {
                
                MTC163NewsChannelModel *model = [[MTC163NewsChannelModel alloc] init];
                
                [model setValuesForKeysWithDictionary:dic];
                
                [self.newsArr addObject:model];
            }
            
        }
        
        [self.tableView reloadData];
        
    }];
}
- (void)tableViewConfig {

    [self.tableView registerClass:[MTC163NewsNormalCell class] forCellReuseIdentifier:@"pool"];
  
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;

}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return self.newsArr.count;

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MTC163NewsNormalCell *cell = [tableView dequeueReusableCellWithIdentifier:@"pool" forIndexPath:indexPath];
    
    cell.model = [self.newsArr objectAtIndex:indexPath.row];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 80;
    

}






@end
