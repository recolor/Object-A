//
//  MTC163NewsViewController.m
//  MTCProject-A
//
//  Created by dllo on 16/6/14.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTC163NewsViewController.h"
#import "MTCPlayerViewController.h"
#import "MTC163NewsTVCOOfBasic.h"
#import "MTCSearchViewController.h"
#import "MTC163NewsChannelModel.h"
#import "MTCHotNewsViewController.h"
#import "MTCHeadNewsViewController.h"
#import "MTCLiveViewController.h"
#import "Masonry.h"

@interface MTC163NewsViewController ()<UICollectionViewDelegate, UICollectionViewDataSource>

//频道
@property (nonatomic, retain) UIView *titleView;
//新闻
@property (nonatomic, retain) UIScrollView *scrollView;
//频道数据

@property (nonatomic, strong) NSArray *channelArr;
//现有的频道
@property (nonatomic, strong) NSMutableArray *channelNowArr;
//删除的频道
@property (nonatomic, strong) NSMutableArray *channelDelArr;


@end

@implementation MTC163NewsViewController

- (void)dealloc{
    [super dealloc];
    [_scrollView release];
    [_channelArr release];
    [_channelNowArr release];
    [_channelDelArr release];
    [_titleView release];

}





- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{

    self= [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        [self createTitleView];
    }
    return self;


}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self creatScollView];
    [self configTabBarItem];
    [self scrollViewConfig];
    

    

}
#pragma make - 标题 搜索和直播
- (void)createTitleView {
    
    UINavigationBar *bar = [UINavigationBar appearance];
    
    bar.barTintColor = [UIColor redColor];
    UIScrollView *smallScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 64,SCREEN_WIDTH * 3, 40)];
    smallScrollView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:smallScrollView];
    [smallScrollView release];
    
    UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
    button1.frame = CGRectMake(10, 64, 40, 40);

    [button1 setTitle:@"首页" forState:UIControlStateNormal];
    [button1 setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button2 setTitle:@"段子" forState:UIControlStateNormal];
    [button2 setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    button2.frame = CGRectMake((SCREEN_WIDTH - 20) / 3 - 10 , 64, 40, 40);
    UIButton *button3 = [UIButton buttonWithType:UIButtonTypeCustom];
    button3.frame = CGRectMake(SCREEN_WIDTH - 50, 64, 40, 40);
    [button3 setTitle:@"直播" forState:UIControlStateNormal];
    [button3 setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    
    UIButton *button4 = [UIButton buttonWithType:UIButtonTypeCustom];
    button4.frame = CGRectMake((SCREEN_WIDTH - 20) / 3 * 2 - 10, 64, 40, 40);
    [button4 setTitle:@"图片" forState:UIControlStateNormal];
    [button4 setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    
    [self.view addSubview:button4];
    [button4 release];
    
    [self.view addSubview:button3];
    [button3 release];
    
    [self.view addSubview:button2];
    [button2 release];
    
    [self.view addSubview:button1];
    [button1 release];
    
//    self.titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 84)];
//    
//    [self.view addSubview:self.titleView];
    
}
- (void)creatScollView {
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 104, SCREEN_WIDTH, SCREEN_HEIGHT - 104 - 49)];
    
    [self.view addSubview:self.scrollView];

    self.scrollView.pagingEnabled = YES;
    self.scrollView.showsHorizontalScrollIndicator = NO;
   
    self.scrollView.bounces = NO;
    [_scrollView release];
    self.automaticallyAdjustsScrollViewInsets = NO;
    

}

//添加
- (void)scrollViewConfig {

    MTC163NewsTVCOOfBasic *fieNewsVC = [[MTC163NewsTVCOOfBasic alloc] init];
    MTCHotNewsViewController *hotNewsVC = [[MTCHotNewsViewController alloc] init];
    MTCHeadNewsViewController *headNewsVC = [[MTCHeadNewsViewController alloc] init];
    MTCLiveViewController *carNewsVC = [[MTCLiveViewController alloc] init];
    
    self.scrollView.contentSize = CGSizeMake(SCREEN_WIDTH * 4, SCREEN_HEIGHT - 49 - 104);
    fieNewsVC.view.frame = self.scrollView.bounds;
    [self.scrollView addSubview:fieNewsVC.view];
    [fieNewsVC release];
 
    
    hotNewsVC.view.frame = CGRectMake(SCREEN_WIDTH, 0, SCREEN_WIDTH, CGRectGetHeight(self.scrollView.bounds));
    [self.scrollView addSubview:hotNewsVC.view];
    [hotNewsVC release];

    
    headNewsVC.view.frame = CGRectMake(SCREEN_WIDTH * 2, 0, SCREEN_WIDTH, CGRectGetHeight(self.scrollView.bounds));
    [self.scrollView addSubview:headNewsVC.view];
    [headNewsVC release];
    carNewsVC.view.frame = CGRectMake(SCREEN_WIDTH * 3, 0, SCREEN_WIDTH, CGRectGetHeight(self.scrollView.bounds));
    [self.scrollView addSubview:carNewsVC.view];
    [carNewsVC release];
  
    
    [self addChildViewController:fieNewsVC];
    [self addChildViewController:hotNewsVC];
    [self addChildViewController:headNewsVC];
    [self addChildViewController:carNewsVC];
    
    

}


- (void)configTabBarItem {
    self.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"新闻" image:[UIImage imageNamed:@"News.png"] tag:100];
    
    self.navigationItem.title = @"網易";
    
    UIBarButtonItem *item1 = [[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"vidicon.png"] style:UIBarButtonItemStylePlain target:self action:@selector(handleVidicon:)] autorelease];
    
    UIBarButtonItem *item2 = [[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"search.png"] style:UIBarButtonItemStylePlain target:self action:@selector(handleSearch:)] autorelease];
    
    
    self.navigationItem.rightBarButtonItems = @[item1, item2];
    
    
}
- (void)handleVidicon:(UIBarButtonItem *)barbutton {

    MTCPlayerViewController *pvc = [[MTCPlayerViewController alloc] init];
    
    [self.navigationController pushViewController:pvc animated:YES];
    
    [pvc release];


}
- (void)handleSearch:(UIBarButtonItem *)barbutton{
    MTCSearchViewController *svc = [[MTCSearchViewController alloc] init];
    
    [self.navigationController pushViewController:svc animated:YES];
    
    [svc release];
 
}












@end
