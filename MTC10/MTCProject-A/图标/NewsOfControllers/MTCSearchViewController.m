//
//  MTCSearchViewController.m
//  MTCProject-A
//
//  Created by dllo on 16/6/14.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCSearchViewController.h"

@interface MTCSearchViewController ()

@end

@implementation MTCSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self creatBackView];
    
   
}
- (void)creatBackView {

    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStylePlain target:self action:@selector(handleBack)];
    

}
- (void)handleBack {
    [self.navigationController popToViewController:self.navigationController.viewControllers[0] animated:YES];
    
}



@end
