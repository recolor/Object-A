//
//  MTCLiveCellOther.m
//  MTCProject-A
//
//  Created by dllo on 16/6/24.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCLiveCellOther.h"
#import "Masonry.h"
#import "UIImageView+WebCache.h"
#import "MTCLiveModel.h"

@interface MTCLiveCellOther()
@property (nonatomic, retain) UIImageView *imgsrc;
@property (nonatomic, retain) UILabel *title;
@property (nonatomic, retain) UILabel *source;

@end

@implementation MTCLiveCellOther
-(void)dealloc{
    [super dealloc];
    [_imgsrc release];
    [_title release];
    [_source release];


    
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self config];
    }

    return self;

}
- (void)config{
    
    self.imgsrc = [[UIImageView alloc] init];
    self.title = [[UILabel alloc] init];
    self.source = [[UILabel alloc] init];
    
    [self.contentView addSubview:self.imgsrc];
    [self.contentView addSubview:self.title];
    [self.contentView addSubview:self.source];
    
    self.source.font = [UIFont systemFontOfSize:12];
    [self.title setNumberOfLines:2];

    [self.imgsrc mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).with.offset(10);
        make.left.equalTo(self.contentView).with.offset(10);
        make.bottom.equalTo(self.contentView).with.offset(-10);
        make.width.equalTo(self.imgsrc.mas_height).multipliedBy(4.0 / 3.0f);
        
    }];
    [self.title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).with.offset(10);
        make.left.equalTo(self.imgsrc.mas_right).with.offset(10);
        make.right.equalTo(self.contentView).with.offset(-10);
        make.height.offset(60);
        
    }];
    [self.source mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.title.mas_bottom).with.offset(10);
        make.left.equalTo(self.imgsrc.mas_right).with.offset(10);
        make.bottom.equalTo(self.contentView).with.offset(-10);
        make.width.offset(80);
        
    }];
    [_imgsrc release];
    [_title release];
    [_source release];




}
- (void)setModel:(MTCLiveModel *)model{
    if (_model != model) {
        [_model release];
        [_model retain];
    }
    self.title.text = model.title;
    self.source.text = model.source;
    
    [self.imgsrc sd_setImageWithURL:[NSURL URLWithString:model.imgsrc] placeholderImage:nil];
    
//    self.title.text = [[model.specialextra objectAtIndex:0] objectForKey:@"titlt"];
//    self.source.text = [[model.specialextra objectAtIndex:0] objectForKey:@"source"];
//    NSString *str1 = [[model.specialextra objectAtIndex:0] objectForKey:@"imgsrc"];
//    
//    [self.imgsrc sd_setImageWithURL:[NSURL URLWithString:str1] placeholderImage:nil];

}





@end
