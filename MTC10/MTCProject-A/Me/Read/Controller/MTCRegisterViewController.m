//
//  MTCRegisterViewController.m
//  MTCProject-A
//
//  Created by dllo on 16/6/22.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCRegisterViewController.h"

#import "Masonry.h"
@interface MTCRegisterViewController ()

@end

@implementation MTCRegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self creatView];
    self.view.backgroundColor = COLOR;
}
- (void)creatView{
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"登录" style:UIBarButtonItemStylePlain target:self action:@selector(handleBack)];
    
    
    
    
    UIView *viewOfRegister = [[UIView alloc] init];
    [self.view addSubview:viewOfRegister];
    viewOfRegister.backgroundColor = [UIColor whiteColor];
    
    [viewOfRegister mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).with.offset(80);
        make.left.equalTo(self.view).with.offset(20);
        make.right.equalTo(self.view).with.offset(-20);
        make.height.offset(130);
        
    }];
    
    
    UIImageView *imageOfAccount = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"me.png"]];
    [viewOfRegister addSubview:imageOfAccount];
    
    [imageOfAccount mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).with.offset(100);
        make.left.equalTo(self.view).with.offset(30);
        make.height.offset(20);
        make.width.offset(20);
        
        
    }];
    UIImageView *imageOfSuo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"suo.png"]];
    [viewOfRegister addSubview:imageOfSuo];
    
    [imageOfSuo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).with.offset(30);
        make.top.equalTo(imageOfAccount.mas_bottom).with.offset(50);
        make.height.offset(20);
        make.width.offset(20);
        
    }];
    
    UILabel *labelOfOneH = [[UILabel alloc] init];
    labelOfOneH.backgroundColor = [UIColor blackColor];
    [viewOfRegister addSubview:labelOfOneH];
    
    [labelOfOneH mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(viewOfRegister).with.offset(64);
        make.left.equalTo(self.view).with.offset(70);
        make.right.equalTo(self.view).with.offset(-70);
        make.height.offset(1);
        
        
    }];
    UILabel *labelOfRegister = [[UILabel alloc] init];
    [viewOfRegister addSubview:labelOfRegister];
    labelOfRegister.text = @"网易邮箱/手机号";
    labelOfRegister.textColor = COLOR;
    
    [labelOfRegister mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(viewOfRegister).with.offset(20);
        make.left.equalTo(self.view).with.offset(70);
        make.right.equalTo(self.view).with.offset(-70);
        make.height.offset(40);
        
    }];
    
    
    
    
    [imageOfAccount release];
    [imageOfSuo release];
    [viewOfRegister release];
    [labelOfOneH release];
    
    
    
    
 
    
    
    
    
    
}
- (void)handleBack{
    [self.navigationController popToViewController:self.navigationController.viewControllers[0] animated:YES];


}



@end
