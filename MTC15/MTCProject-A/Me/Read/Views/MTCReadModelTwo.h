//
//  MTCReadModelTwo.h
//  MTCProject-A
//
//  Created by dllo on 16/6/23.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MTCReadModelTwo : NSObject
@property (nonatomic, copy) NSString *imgsrc;
//标题
@property (nonatomic, copy) NSString *title;
//来源
@property (nonatomic, copy) NSString *source;
//图的数组
@property (nonatomic, retain) NSMutableArray *imgnewextra;
@end
