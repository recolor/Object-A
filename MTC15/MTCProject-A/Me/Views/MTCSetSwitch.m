//
//  MTCSetSwitch.m
//  MTCProject-A
//
//  Created by dllo on 16/6/30.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCSetSwitch.h"
#import "Masonry.h"


@interface MTCSetSwitch()


@property(nonatomic, retain)UILabel *label;
@property(nonatomic, retain)UISwitch *open;


@end
@implementation MTCSetSwitch
- (void)dealloc{

    [super dealloc];
    [_label release];
    [_open release];


}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        //通知
        [[NSNotificationCenter defaultCenter]addObserverForName:@"changeBackgroundColor" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
            
            self.backgroundColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1];
        }];
        //通知
        [[NSNotificationCenter defaultCenter]addObserverForName:@"changeBackgroundColor1" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
            
            self.backgroundColor = [UIColor whiteColor];
        }];

        
        
        
        [self creatSubViews];
    }

    return self;


}
- (void)creatSubViews{
    
    self.label = [[UILabel alloc]initWithFrame:CGRectMake(15, 0, 100, 45)];
    self.label.text = @"夜间模式";
    [self.contentView addSubview:self.label];
    self.open = [[UISwitch alloc]initWithFrame:CGRectMake(300, 5, 35, 20)];
    [self.contentView addSubview:self.open];
    [self.open addTarget:self action:@selector(changeColor) forControlEvents:UIControlEventValueChanged];
    [self.label release];
    [self.open release];
}

- (void)changeColor{

    if (self.open.on) {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"changeBackgroundColor" object:nil userInfo:nil];
    }else{
        [[NSNotificationCenter defaultCenter]postNotificationName:@"changeBackgroundColor1"object:nil userInfo:nil];
    }



}













@end
