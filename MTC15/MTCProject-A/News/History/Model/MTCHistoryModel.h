//
//  MTCHistoryModel.h
//  MTCProject-A
//
//  Created by dllo on 16/6/28.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MTCHistoryModel : NSObject
//三个图片的
@property(nonatomic, retain)NSString *source;//来源
@property(nonatomic, retain)NSString *replyCount;//跟帖
@property(nonatomic, retain)NSArray *imgextra;//数组中有两个图片

//一个图的
@property(nonatomic, retain)NSString *digest;


//标题
@property(nonatomic, retain)NSString *title;

@property(nonatomic, retain)NSString *imgsrc;//所有类型的第一个图片

@property(nonatomic, retain)NSString *TAGS;//头部直播预告


@end
