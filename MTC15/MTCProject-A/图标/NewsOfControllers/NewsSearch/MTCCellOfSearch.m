//
//  MTCCellOfSearch.m
//  MTCProject-A
//
//  Created by dllo on 16/7/1.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCCellOfSearch.h"
#import "Masonry.h"
#import "ModelOfSearch.h"
@interface MTCCellOfSearch()

@property (nonatomic, retain) UILabel *title;

@end
@implementation MTCCellOfSearch
- (void)dealloc{
    [_title release];

    [super dealloc];


}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self config];
    }
    return self;


}
- (void)config{

    self.title = [[UILabel alloc] init];
    [self.contentView addSubview:self.title];
    
    [self.title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).with.offset(5);
        make.bottom.equalTo(self.contentView).with.offset(-5);
        make.left.equalTo(self.contentView).with.offset(5);
        make.right.equalTo(self.contentView).with.offset(-5);
        
        
    }];
    [_title release];
    

}
- (void)setModel:(ModelOfSearch *)model{

    self.title.text = model.title;



}





@end
