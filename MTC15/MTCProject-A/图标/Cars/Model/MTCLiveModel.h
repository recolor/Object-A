//
//  MTCLiveModel.h
//  MTCProject-A
//
//  Created by dllo on 16/6/23.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MTCLiveModel : NSObject
//正文
@property (nonatomic, copy) NSString *title;
//来源
@property (nonatomic, copy) NSString *source;
//图片
@property (nonatomic, copy) NSString *imgsrc;
//分类
@property (nonatomic, copy) NSString *digest;
//万人参与
@property (nonatomic, copy) NSString *user_count;

@property (nonatomic, retain) NSMutableArray *specialextra;

@end
