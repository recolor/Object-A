//
//  pictureScrollView.h
//  UI06_UIScrollView
//
//  Created by Apple on 16/5/10.
//  Copyright © 2016年 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface pictureScrollView : UIScrollView

- (void)addPictures:(NSArray <UIImage *>*)pictures;
@end
