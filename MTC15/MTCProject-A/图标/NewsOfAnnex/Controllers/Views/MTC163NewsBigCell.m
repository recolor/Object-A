//
//  MTC163NewsBigCell.m
//  MTCProject-A
//
//  Created by dllo on 16/6/15.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTC163NewsBigCell.h"
#import "Masonry.h"
#import "UIImageView+WebCache.h"
#import "MTC163NewsChannelModel.h"

@interface MTC163NewsBigCell()
@property (nonatomic, retain) UIImageView *imgView;
@property (nonatomic, retain) UILabel *title;
@property (nonatomic, retain) UILabel *type;
@property (nonatomic, retain) UILabel *count;

@end

@implementation MTC163NewsBigCell
- (void)dealloc{

    [super dealloc];
    [_imgView release];
    [_title release];
    [_type release];
    [_count release];
    [_model release];
 
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self configView];
    }
    return self;

}
- (void)configView {

    self.imgView = [[UIImageView alloc] init];
    self.title = [[UILabel alloc] init];
    self.type = [[UILabel alloc] init];
    self.count = [[UILabel alloc] init];
    
    [self.contentView addSubview:self.imgView];
    [self.contentView addSubview:self.title];
    [self.contentView addSubview:self.type];
    [self.contentView addSubview:self.count];
    
    self.imgView.contentMode = UIViewContentModeScaleToFill;
    self.imgView.layer.masksToBounds = YES;
    self.type.font = [UIFont systemFontOfSize:12];
    self.count.font = [UIFont systemFontOfSize:12];
    
    [self.imgView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.contentView.mas_top).with.offset(10);
//        make.bottom.equalTo(self.contentView.mas_bottom).with.offset(-10);
//        make.left.equalTo(self.contentView.mas_left).with.offset(10);
//        make.right.equalTo(self.contentView.mas_right).with.offset(-10);
        make.edges.equalTo(self.contentView).with.insets(UIEdgeInsetsMake(10, 10, 10, 10));
        
        
    }];

    [self.title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).with.offset(10);
        make.left.equalTo(self.contentView.mas_left).with.offset(10);
        make.right.equalTo(self.contentView.mas_right).with.offset(-10);
        make.height.mas_equalTo(@20);
        
        
        
    }];

    int padding = 10;
    [self.type mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).with.offset(padding);
        make.bottom.equalTo(self.contentView.mas_bottom).with.offset(-padding);
        make.top.equalTo(self.imgView.mas_bottom).with.offset(padding);
        make.height.equalTo(@20);
        
        
        
        
    }];

    [self.count mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView.mas_right).with.offset(-10);
        make.bottom.equalTo(self.contentView.mas_bottom).with.offset(-10);
        make.top.equalTo(self.imgView.mas_bottom).with.offset(10);
        make.height.equalTo(@20);
        
    }];
    [_count release];
    [_type release];
    [_imgView release];
    [_title release];
    


}

- (void)setModel:(MTC163NewsChannelModel *)model{
    
    if (_model != model) {
        [_model release];
        [_model retain];
    }
    
    
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:model.img] placeholderImage:nil];
    self.title.text = model.title;
    
    if (![model.recSource isEqualToString:@"#"]) {
        self.type.text = model.recSource;
    }

    //拼接跟帖上
    self.count.text = [model.replyCount.description stringByAppendingString:@"跟帖"];
    //右对齐
    self.count.textAlignment = NSTextAlignmentRight;
    


}









@end
