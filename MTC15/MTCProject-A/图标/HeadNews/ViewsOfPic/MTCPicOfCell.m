//
//  MTCPicOfCell.m
//  MTCProject-A
//
//  Created by dllo on 16/6/20.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCPicOfCell.h"
#import "Masonry.h"
#import "MTCPicOfModels.h"
#import "UIImageView+WebCache.h"

@interface MTCPicOfCell()
//标题
@property (nonatomic, retain) UILabel *setname;
//几张图
@property (nonatomic, retain) UILabel *imgsum;
//跟帖
@property (nonatomic, retain) UILabel *replynum;
//
@property (nonatomic, retain) UIImageView *image5;
@end
@implementation MTCPicOfCell

-(void)dealloc {
    [super dealloc];
    [_model release];
    [_imageView1 release];
    [_imageView2 release];
    [_imageView3 release];
    [_setname release];
    [_imgsum release];
    [_replynum release];
    [_image5 release];


}
//在编辑风格时 使用约束方法
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self config];
    }
    return self;


}
// 约束 手累
- (void)config {
    self.imageView1 = [[UIImageView alloc] init];
    self.imageView2 = [[UIImageView alloc] init];
    self.imageView3 = [[UIImageView alloc] init];
    self.image5 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"activity.png"]];
                   
    self.setname = [[UILabel alloc] init];
    self.imgsum = [[UILabel alloc] init];
    self.replynum = [[UILabel alloc] init];
    self.imageView1.contentMode = UIViewContentModeScaleAspectFill;
    self.imageView2.contentMode = UIViewContentModeScaleAspectFill;
    self.imageView3.contentMode = UIViewContentModeScaleAspectFill;
    
    self.imageView1.layer.masksToBounds = YES;
    self.imageView2.layer.masksToBounds = YES;
    self.imageView3.layer.masksToBounds = YES;
    
    
    
    [self.contentView addSubview:self.imageView1];
    [self.contentView addSubview:self.imageView2];
    [self.contentView addSubview:self.imageView3];
    [self.contentView addSubview:self.setname];
    [self.imageView3 addSubview:self.imgsum];
    [self.imgsum addSubview:self.image5];
    [self.contentView addSubview:self.replynum];
    
    self.imgsum.font = [UIFont systemFontOfSize:12];
    self.replynum.font = [UIFont systemFontOfSize:12];
    
    [self.imageView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).with.offset(10);
        make.left.equalTo(self.contentView).with.offset(10);
        make.bottom.equalTo(self.setname.mas_top).with.offset(-10);
        make.width.equalTo(self.imageView1.mas_height).multipliedBy(4.0/3.0f);
        
        
    }];
    
    [self.imageView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).with.offset(10);
        make.left.equalTo(self.imageView1.mas_right).with.offset(5);
        make.right.equalTo(self.contentView).with.offset(-10);
        make.bottom.equalTo(self.imageView1.mas_centerY).with.offset(-2.5);
        
        
    }];
    
    [self.imageView3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.imageView2.mas_bottom).with.offset(5);
        make.left.equalTo(self.imageView1.mas_right).with.offset(5);
        make.right.equalTo(self.contentView).with.offset(-10);
        make.bottom.equalTo(self.imageView1.mas_bottom);
        
    }];
    [self.image5 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.imgsum);
        make.right.equalTo(self.contentView).with.offset(-55);
        make.size.mas_equalTo(CGSizeMake(20, 20));
        
    }];
    
    
    
    [self.imgsum mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.imageView3.mas_bottom).with.offset(-10);
        make.right.equalTo(self.imageView3.mas_right).with.offset(-10);
        make.size.mas_equalTo(CGSizeMake(55, 20));
        
    }];

    [self.setname mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.imageView1.mas_bottom).with.offset(10);
        make.bottom.equalTo(self.contentView).with.offset(-10);
        make.left.equalTo(self.contentView).with.offset(10);
        make.width.offset(300);
        
    }];
    [self.replynum mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).with.offset(-10);
        make.bottom.equalTo(self.contentView).with.offset(-10);
        make.size.mas_equalTo(CGSizeMake(80, 20));
        
        
    }];
    
    [_imageView1 release];
    [_imageView2 release];
    [_imageView3 release];
    [_setname release];
    [_imgsum release];
    [_replynum release];
    [_image5 release];



}
// model 
- (void)setModel:(MTCPicOfModels *)model{
    if (_model != model) {
        [_model release];
        [_model retain];
    }

    self.setname.text = model.setname;
    self.imgsum.text = [model.imgsum.description stringByAppendingString:@"Pics"];
    self.imgsum.backgroundColor = [UIColor lightGrayColor];
    

    self.imgsum.alpha = 0.8;
    self.imgsum.textColor = [UIColor whiteColor];
    self.imgsum.textAlignment = NSTextAlignmentRight;
    self.replynum.text = [model.replynum.description stringByAppendingString:@"跟帖"];
    self.replynum.textAlignment = NSTextAlignmentRight;
    
    

}












@end
