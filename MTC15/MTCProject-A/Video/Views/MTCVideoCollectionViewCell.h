//
//  MTCVideoCollectionViewCell.h
//  MTCProject-A
//
//  Created by dllo on 16/6/20.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MTCVideoOfModel;

@interface MTCVideoCollectionViewCell : UICollectionViewCell

@property (nonatomic, retain) MTCVideoOfModel *model;

@end
