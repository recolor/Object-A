//
//  MTCTopicCell.m
//  MTCProject-A
//
//  Created by dllo on 16/6/21.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCTopicCell.h"
#import "MTCTopicModel.h"
#import "Masonry.h"
#import "UIImageView+WebCache.h"

@interface MTCTopicCell()

@property (nonatomic, retain) UILabel *name;
@property (nonatomic, retain) UILabel *classifcation;
@property (nonatomic, retain) UILabel *talkCount;
@property (nonatomic, retain) UILabel *concernCount;
@property (nonatomic, retain) UIButton *add;

@property (nonatomic, retain) UIImageView *talkPicture1;
@property (nonatomic, retain) UIImageView *talkPicture2;
@property (nonatomic, retain) UIImageView *talkPicture3;

@end
@implementation MTCTopicCell
-(void)dealloc{
    [super dealloc];
    [_model release];
    [_talkPicture1 release];
    [_talkPicture2 release];
    [_talkPicture3 release];
    [_name release];
    [_classifcation release];
    [_talkCount release];
    [_concernCount release];
    [_add release];



}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self config];
    }
    return  self;

}

- (void)config{
    self.talkPicture1 = [[UIImageView alloc] init];
    self.talkPicture2 = [[UIImageView alloc] init];
    self.talkPicture3 = [[UIImageView alloc] init];
    self.name = [[UILabel alloc] init];
    //生活
    self.classifcation = [[UILabel alloc] init];
    //讨论
    self.talkCount = [[UILabel alloc] init];
    //关注
    self.concernCount = [[UILabel alloc] init];
    
    self.add = [UIButton buttonWithType:UIButtonTypeCustom];
//    [self.add setImage:[UIImage imageNamed:@"add.png"] forState:UIControlStateNormal];
    self.add.backgroundColor = [UIColor redColor];
    //加圆角
    self.add.layer.cornerRadius = 15;
    //覆盖超出部分
    self.add.layer.masksToBounds = YES;
    [self.add setTitle:@"+关注" forState:UIControlStateNormal];
    [self.add setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    
    //字体
    self.concernCount.font = [UIFont systemFontOfSize:12];
    self.talkCount.font = [UIFont systemFontOfSize:12];
    self.classifcation.font = [UIFont systemFontOfSize:12];
    
    [self.contentView addSubview:self.talkPicture1];
    [self.contentView addSubview:self.talkPicture2];
    [self.contentView addSubview:self.talkPicture3];
    [self.contentView addSubview:self.name];
    [self.contentView addSubview:self.classifcation];
    [self.contentView addSubview:self.talkCount];
    [self.contentView addSubview:self.concernCount];
    [self.contentView addSubview:self.add];
    

    [self.name mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).with.offset(10);
        make.left.equalTo(self.contentView).with.offset(10);
        make.height.offset(20);
        make.width.offset(200);
                        
    }];
    [self.talkPicture1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).with.offset(40);
        make.left.equalTo(self.contentView).with.offset(10);
        make.width.offset((SCREEN_WIDTH - 40) / 3);
        make.bottom.equalTo(self.contentView).with.offset(-40);
        
        
    }];
    [self.talkPicture2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).with.offset(40);
        make.left.equalTo(self.talkPicture1.mas_right).with.offset(10);
        make.width.offset((SCREEN_WIDTH - 40) / 3);
        make.bottom.equalTo(self.contentView).with.offset(-40);
        
        
    }];
    [self.talkPicture3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).with.offset(40);
        make.left.equalTo(self.talkPicture2.mas_right).with.offset(10);
        make.right.equalTo(self.contentView).with.offset(-10);
        make.bottom.equalTo(self.contentView).with.offset(-40);
        
    }];
    
    [self.classifcation mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.talkPicture1.mas_bottom).with.offset(10);
        make.left.equalTo(self.contentView).with.offset(10);
        make.bottom.equalTo(self.contentView).with.offset(-10);
        make.width.offset(40);
        
    }];
    [self.concernCount mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.talkPicture1.mas_bottom).with.offset(10);
        make.left.equalTo(self.classifcation.mas_right).with.offset(10);
        make.bottom.equalTo(self.contentView).with.offset(-10);
        make.width.offset(80);
    }];
    [self.talkCount mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.talkPicture1.mas_bottom).with.offset(10);
        make.left.equalTo(self.concernCount.mas_right).with.offset(10);
        make.bottom.equalTo(self.contentView).with.offset(-10);
        make.width.offset(80);
       
        
    }];
    [self.add mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.talkPicture3.mas_bottom).with.offset(5);
        make.right.equalTo(self.contentView).with.offset(-5);
        make.bottom.equalTo(self.contentView).with.offset(-5);
        make.width.offset(80);
        
        
    }];
    
    
    
    [_talkPicture1 release];
    [_talkPicture2 release];
    [_talkPicture3 release];
    [_name release];
    [_classifcation release];
    [_talkCount release];
    [_concernCount release];
    [_add release];



}
- (void)setModel:(MTCTopicModel *)model{
    if (_model != model) {
        [_model release];
        [_model retain];
    }
    
    NSString *str = @"#";
    self.name.text = [str stringByAppendingString:[model.name.description stringByAppendingString:@"#"]];;
    
    self.classifcation.text = model.classification;
    
    self.concernCount.text = [model.concernCount.description stringByAppendingString:@"关注"];
    self.talkCount.text = [model.talkCount.description stringByAppendingString:@"讨论"];
    //根据index 分别给三个图片赋值
    NSString *model1 = [model.talkPicture objectAtIndex:0];
    [self.talkPicture1 sd_setImageWithURL:[NSURL URLWithString:model1]];
    NSString *model2 = [model.talkPicture objectAtIndex:1];
    [self.talkPicture2 sd_setImageWithURL:[NSURL URLWithString:model2]];
    NSString *model3 = [model.talkPicture objectAtIndex:2];
    [self.talkPicture3 sd_setImageWithURL:[NSURL URLWithString:model3]];

}
























@end
