//
//  MTCTopicCellOfPic.m
//  MTCProject-A
//
//  Created by dllo on 16/6/21.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCTopicCellOfPic.h"
#import "Masonry.h"
#import "UIImageView+WebCache.h"
#import "MTCTopicModel.h"

@interface MTCTopicCellOfPic()
@property (nonatomic, retain) UILabel *name;
@property (nonatomic, retain) UILabel *content1;
@property (nonatomic, retain) UILabel *content2;
//关注
@property (nonatomic, retain) UILabel *concernCount;
@property (nonatomic, retain) UILabel *talkCount;
@property (nonatomic, retain) UILabel *classification;
@property (nonatomic, retain) UIButton *add;
@property (nonatomic, retain) UIImageView *userHeadPicUrl1;
@property (nonatomic, retain) UIImageView *userHeadPicUrl2;


@end


@implementation MTCTopicCellOfPic
- (void)dealloc{
    [super dealloc];
    [_model release];
    [_name release];
    [_content1 release];
    [_content2 release];
    [_concernCount release];
    [_talkCount release];
    [_classification release];
    [_add release];
    [_userHeadPicUrl1 release];
    [_userHeadPicUrl2 release];
    

}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self config];
    }
    return self;

}
-(void)config{
    self.name = [[UILabel alloc] init];
    self.content1 = [[UILabel alloc] init];
    self.content2 = [[UILabel alloc] init];
    self.concernCount = [[UILabel alloc] init];
    self.talkCount = [[UILabel alloc] init];
    self.classification = [[UILabel alloc] init];
    self.add = [[UIButton alloc] init];
    [self.add setTitle:@"+关注" forState:UIControlStateNormal];
    self.add.backgroundColor = [UIColor redColor];
    
    self.add.layer.cornerRadius = 10;
    self.add.layer.masksToBounds = YES;
    
    self.userHeadPicUrl1 = [[UIImageView alloc] init];
    self.userHeadPicUrl2 = [[UIImageView alloc] init];
    
    [self.contentView addSubview:self.name];
    [self.contentView addSubview:self.content1];
    [self.contentView addSubview:self.content2];
    [self.contentView addSubview:self.talkCount];
    [self.contentView addSubview:self.concernCount];
    [self.contentView addSubview:self.classification];
    [self.contentView addSubview:self.add];
    [self.contentView addSubview:self.userHeadPicUrl1];
    [self.contentView addSubview:self.userHeadPicUrl2];
    
    [self.name mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).with.offset(10);
        make.left.equalTo(self.contentView).with.offset(10);
        make.height.offset(20);
        make.width.offset(200);
        
    }];
    
    [self.userHeadPicUrl1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).with.offset(40);
        make.left.equalTo(self.contentView).with.offset(10);
        make.bottom.equalTo(self.userHeadPicUrl2.mas_top).with.offset(-10);
        make.width.offset(40);
        
    }];
    [self.content1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).with.offset(40);
        make.left.equalTo(self.userHeadPicUrl1.mas_right).with.offset(15);
        make.bottom.equalTo(self.content2.mas_top).with.offset(-10);
        make.width.offset(200);
        
    }];
    [self.userHeadPicUrl2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.userHeadPicUrl1.mas_bottom).with.offset(10);
        make.left.equalTo(self.contentView).with.offset(10);
        make.bottom.equalTo(self.contentView).with.offset(-40);
        make.width.offset(40);
        
    }];
    [self.content2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.content2.mas_bottom).with.offset(10);
        make.left.equalTo(self.userHeadPicUrl2.mas_right).with.offset(15);
        make.bottom.equalTo(self.contentView).with.offset(-40);
        make.width.offset(200);
        
    }];

    [self.classification mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.userHeadPicUrl2.mas_bottom).with.offset(10);
        make.left.equalTo(self.contentView).with.offset(10);
        make.bottom.equalTo(self.contentView).with.offset(-10);
        make.width.offset(40);
        
    }];
    [self.concernCount mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.userHeadPicUrl2.mas_bottom).with.offset(10);
        make.left.equalTo(self.classification.mas_right).with.offset(10);
        make.bottom.equalTo(self.contentView).with.offset(-10);
        make.width.offset(80);
    }];
    [self.talkCount mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.content2.mas_bottom).with.offset(10);
        make.left.equalTo(self.concernCount.mas_right).with.offset(10);
        make.bottom.equalTo(self.contentView).with.offset(-10);
        make.width.offset(80);
        
        
    }];
    [self.add mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.content2.mas_bottom).with.offset(5);
        make.right.equalTo(self.contentView).with.offset(-5);
        make.bottom.equalTo(self.contentView).with.offset(-5);
        make.width.offset(80);
        
        
    }];

    [_name release];
    [_content1 release];
    [_content2 release];
    [_concernCount release];
    [_talkCount release];
    [_classification release];
    [_add release];
    [_userHeadPicUrl1 release];
    [_userHeadPicUrl2 release];




}
- (void)setModel:(MTCTopicModel *)model{
    
    if (_model != model) {
        [_model release];
        [_model retain];
    }
    
    
    NSString *str = @"#";
    self.name.text = [[str stringByAppendingString:model.name] stringByAppendingString:@"#"];
    self.classification.text = model.classification;
    self.concernCount.text = [model.concernCount stringByAppendingString:@"关注"];
    self.talkCount.text = [model.talkCount stringByAppendingString:@"讨论"];
    //数组中套字典
    self.content1.text = [[model.talkContent objectAtIndex:0] objectForKey:@"content"];
    
    self.content2.text = [[model.talkContent objectAtIndex:1] objectForKey:@"content"];
    
    NSString *model1 = [[model.talkContent objectAtIndex:0] objectForKey:@"userHeadPicUrl"];
    
    [self.userHeadPicUrl1 sd_setImageWithURL:[NSURL URLWithString:model1 ]  placeholderImage:nil];
    NSString *model2 = [[model.talkContent objectAtIndex:1] objectForKey:@"userHeadPicUrl"];
    
    [self.userHeadPicUrl2 sd_setImageWithURL:[NSURL URLWithString:model2] placeholderImage:nil];
    
   
}




@end
