//
//  MTCReadOnePicCell.m
//  MTCProject-A
//
//  Created by dllo on 16/6/22.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCReadOnePicCell.h"
#import "Masonry.h"
#import "UIImageView+WebCache.h"
#import "MTCReadModel.h"

@interface MTCReadOnePicCell()
@property (nonatomic, retain) UILabel *title;
@property (nonatomic, retain) UILabel *source;
@property (nonatomic, retain) UIImageView *img;

@end

@implementation MTCReadOnePicCell

-(void)dealloc{
    [super dealloc];
    [_title release];
    [_source release];
    [_img release];
    [_model release];
 
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self config];
    }
    return self;


}
- (void)config{
    self.title = [[UILabel alloc] init];
    self.source = [[UILabel alloc] init];
    self.img = [[UIImageView alloc] init];
    self.title.numberOfLines = 2;
    self.source.font = [UIFont systemFontOfSize:12];
    
    [self.contentView addSubview:self.title];
    [self.contentView addSubview:self.source];
    [self.contentView addSubview:self.img];
    
    [self.img mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).with.offset(10);
        make.left.equalTo(self.contentView).with.offset(10);
        make.bottom.equalTo(self.contentView).with.offset(-40);
        make.width.equalTo(self.img.mas_height).multipliedBy(4.0 / 3.0f);
        
    }];
    [self.title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).with.offset(10);
        make.left.equalTo(self.img.mas_right).with.offset(10);
        make.right.equalTo(self.contentView).with.offset(-10);
        make.height.offset(20);
        
    }];
    [self.source mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.img.mas_bottom).with.offset(10);
        make.bottom.equalTo(self.contentView).with.offset(-10);
        make.left.equalTo(self.contentView).with.offset(10);
        make.width.offset(100);
        
    }];


    [_title release];
    [_source release];
    [_img release];



}

- (void)setModel:(MTCReadModel *)model{
    
    self.title.text = model.title;
    
    self.source.text = model.source;
    
    [self.img sd_setImageWithURL:[NSURL URLWithString:model.img] placeholderImage:nil];
    









}




@end
