//
//  MTCReadViewController.m
//  MTCProject-A
//
//  Created by dllo on 16/6/22.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCReadViewController.h"
#import "Masonry.h"
#import "MTCRegisterViewController.h"
#import "MTCReadModel.h"
#import "UIImageView+WebCache.h"
#import "MTCReadOnePicCell.h"



@interface MTCReadViewController ()
@property (nonatomic, retain) UIButton *buttonRegister;
@property (nonatomic, retain) NSMutableArray *arrayOfRead;





@end

@implementation MTCReadViewController
- (void)dealloc{

    [super dealloc];
    [_buttonRegister release];
    [_arrayOfRead release];

}
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self config];
    }
    return self;

}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self creatView];
    [self configTableView];
    
}
- (void)config{
    self.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"阅读" image:[UIImage imageNamed:@"readread.png"] tag:200];
    self.view.backgroundColor = [UIColor lightGrayColor];
    
 
}
- (void)creatView{
    UIView *viewOfRead= [[UIView alloc] init];
    [self.view addSubview:viewOfRead];
    viewOfRead.frame = CGRectMake(0, 64, SCREEN_WIDTH, 100);
    viewOfRead.backgroundColor = COLOR;
    
    self.buttonRegister = [UIButton buttonWithType:UIButtonTypeCustom];
    [viewOfRead addSubview:self.buttonRegister];
    [self.buttonRegister mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(viewOfRead.mas_centerX);
        make.centerY.equalTo(viewOfRead.mas_centerY);
        make.width.offset(300);
        make.height.offset(40);
    }];
    self.buttonRegister.backgroundColor = [UIColor whiteColor];
    [self.buttonRegister setTitle:@"立即登录" forState:UIControlStateNormal];
    [self.buttonRegister setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [self.buttonRegister addTarget:self action:@selector(handleRegister) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *name = [[UILabel alloc] init];
    name.text = @"获取更符合口味的推荐";
    [viewOfRead addSubview:name];
    [name mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(viewOfRead.mas_centerX);
        make.top.equalTo(self.buttonRegister.mas_bottom).with.offset(5);
        make.width.offset(300);
        make.height.offset(20);
        
        
    }];
    name.textColor = [UIColor colorWithRed:165 / 255.0f green:165 / 255.0f blue:165 / 255.0f alpha:1];
    name.textAlignment = NSTextAlignmentCenter;
    [name release];
    [self.buttonRegister release];
    

    
 

}
- (void)handleRegister{
    
    MTCRegisterViewController *registerVC = [[MTCRegisterViewController alloc] init];
    
    [self.navigationController pushViewController:registerVC animated:YES];
    

}

- (void)configTableView{

    UITableView *tableView = [[UITableView alloc] init];
    tableView.frame = CGRectMake(0, 100, SCREEN_WIDTH, SCREEN_HEIGHT);
    

}
- (void)configData{
    self.arrayOfRead = [NSMutableArray array];
    
    NSString *str =@"http://c.3g.163.com/recommend/getSubDocPic?from=yuedu&size=20&passport=&devId=LTYcwrpXScWqtr8Nf%2BI8Pg%3D%3D&lat=zfDL4gTiHLhCL%2Bds9mCzaw%3D%3D&lon=mcWpYrydyuQWpOGaR94gHg%3D%3D&version=10.0&net=wifi&ts=1466578997&sign=1V8U0HXsbi%2FvnRozirxalYF1NR8Y%2BmEqnZkgBWBt5dV48ErR02zJ6%2FKXOnxX046I&encryption=1&canal=baidu_news&mac=TloFeR%2BBcRtLweZ9kML0cyKCZ0vHdesSTBiDEVNplbY%3D";
    





}












@end
