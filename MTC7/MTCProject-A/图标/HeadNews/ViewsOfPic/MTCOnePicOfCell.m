//
//  MTCOnePicOfCell.m
//  MTCProject-A
//
//  Created by dllo on 16/6/20.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCOnePicOfCell.h"
#import "Masonry.h"
#import "MTCPicOfModels.h"
#import "UIImageView+WebCache.h"

@interface MTCOnePicOfCell()
//标题
@property (nonatomic, retain)UILabel *setname;
//几张图
@property (nonatomic, retain)UILabel *imgsum;
//跟帖
@property (nonatomic, retain)UILabel *replynum;
//图片
@property (nonatomic, retain)UIImageView *imgViewOfone;
@property (nonatomic, retain) UIImageView *image5;

@end
@implementation MTCOnePicOfCell
- (void)dealloc{
    [super dealloc];
    [_model release];
    [_setname release];
    [_imgsum release];
    [_replynum release];
    [_imgViewOfone release];
    [_image5 release];





}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self config];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}
- (void)config {

    self.imgViewOfone = [[UIImageView alloc]init];
    
    self.imgViewOfone.contentMode = UIViewContentModeScaleAspectFill;
    
    self.imgViewOfone.layer.masksToBounds = YES;
    self.image5 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"activity.png"]];
    
    
    self.setname = [[UILabel alloc]init];
    self.imgsum = [[UILabel alloc]init];
    self.replynum = [[UILabel alloc]init];
    [self.contentView addSubview:self.imgViewOfone];
    
    [self.contentView addSubview:self.setname];
    [self.imgViewOfone addSubview:self.imgsum];
    [self.imgsum addSubview:self.image5];
    
    
    [self.contentView addSubview:self.replynum];
    self.imgsum.font = [UIFont systemFontOfSize:12];
    self.replynum.font = [UIFont systemFontOfSize:12];
    
    [self.imgViewOfone mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).with.offset(10);
        make.left.equalTo(self.contentView.mas_left).with.offset(10);
        make.bottom.equalTo(self.contentView).with.offset(-30);
        make.right.equalTo(self.contentView.mas_right).with.offset(-10);
    }];
    
    
    [self.setname mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.contentView).with.offset(-10);
        make.left.equalTo(self.contentView).with.offset(10);
        make.size.mas_equalTo(CGSizeMake(self.contentView.bounds.size.width, 20));
    }];
    [self.image5 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.imgsum);
        make.right.equalTo(self.contentView).with.offset(-55);
        make.size.mas_equalTo(CGSizeMake(20, 20));
        
    }];
    
    [self.imgsum mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.imgViewOfone.mas_right).with.offset(-10);
        make.bottom.equalTo(self.imgViewOfone.mas_bottom).with.offset(-10);
        make.size.mas_equalTo(CGSizeMake(55, 20));
    }];
    
    [self.replynum mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView.mas_right).with.offset(-10);
        make.bottom.equalTo(self.contentView.mas_bottom).with.offset(-10);
        make.size.mas_equalTo(CGSizeMake(80, 20));
    }];
    
 
}
- (void)setModel:(MTCPicOfModels *)model{
    self.setname.text = model.setname;
    self.imgsum.text = [model.imgsum.description stringByAppendingString:@"Pics"];
    self.imgsum.backgroundColor = [UIColor lightGrayColor];
    self.imgsum.textColor =[UIColor whiteColor];
    self.imgsum.textAlignment = NSTextAlignmentRight;
    
    
    
    
    self.replynum.text = [model.replynum.description stringByAppendingString:@"跟帖"];
    self.replynum.textAlignment = NSTextAlignmentRight;
    
    [self.imgViewOfone sd_setImageWithURL:[NSURL URLWithString:model.cover]];
    

}










@end
