//
//  MTCReadViewController.m
//  MTCProject-A
//
//  Created by dllo on 16/6/22.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCReadViewController.h"
#import "Masonry.h"
#import "MTCRegisterViewController.h"
#import "MTCReadModel.h"
#import "UIImageView+WebCache.h"
#import "MTCReadOnePicCell.h"
#import "NetworkingHandler.h"
#import "MTCReadThreeCell.h"
//#import "MTCReadModelTwo.h"

@interface MTCReadViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, retain) UIButton *buttonRegister;
@property (nonatomic, retain) UITableView *tableView;
@property (nonatomic, retain) NSMutableArray *arrayOfRead;
@property (nonatomic, retain) NSMutableArray *mArrayRead;
@property (nonatomic, retain) MTCReadModel *model1;
//@property (nonatomic, retain) MTCReadModelTwo *model2;


@end

@implementation MTCReadViewController
- (void)dealloc{

    [super dealloc];
    [_buttonRegister release];
    [_arrayOfRead release];
    [_mArrayRead release];
    [_model1 release];

}
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self config];
    }
    return self;

}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self creatView];
    [self configTableView];
    [self configData];
    
}
- (void)config{
    self.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"阅读" image:[UIImage imageNamed:@"readread.png"] tag:200];
    self.view.backgroundColor = [UIColor lightGrayColor];
    
 
}
- (void)creatView{
    UIView *viewOfRead= [[UIView alloc] init];
    [self.view addSubview:viewOfRead];
    viewOfRead.frame = CGRectMake(0, 64, SCREEN_WIDTH, 100);
    viewOfRead.backgroundColor = COLOR;
    
    self.buttonRegister = [UIButton buttonWithType:UIButtonTypeCustom];
    [viewOfRead addSubview:self.buttonRegister];
    [self.buttonRegister mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(viewOfRead.mas_centerX);
        make.centerY.equalTo(viewOfRead.mas_centerY);
        make.width.offset(300);
        make.height.offset(40);
    }];
    self.buttonRegister.backgroundColor = [UIColor whiteColor];
    [self.buttonRegister setTitle:@"立即登录" forState:UIControlStateNormal];
    [self.buttonRegister setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [self.buttonRegister addTarget:self action:@selector(handleRegister) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *name = [[UILabel alloc] init];
    name.text = @"获取更符合口味的推荐";
    [viewOfRead addSubview:name];
    [name mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(viewOfRead.mas_centerX);
        make.top.equalTo(self.buttonRegister.mas_bottom).with.offset(5);
        make.width.offset(300);
        make.height.offset(20);
        
        
    }];
    name.textColor = [UIColor colorWithRed:165 / 255.0f green:165 / 255.0f blue:165 / 255.0f alpha:1];
    name.textAlignment = NSTextAlignmentCenter;
    [name release];
    [self.buttonRegister release];
    
  

}
- (void)handleRegister{
    
    MTCRegisterViewController *registerVC = [[MTCRegisterViewController alloc] init];
    
    [self.navigationController pushViewController:registerVC animated:YES];
    

}

- (void)configTableView{

    self.tableView = [[UITableView alloc] init];
    self.tableView.frame = CGRectMake(0, 164, SCREEN_WIDTH, SCREEN_HEIGHT);
    
    [self.view addSubview:self.tableView];
    [self.tableView registerClass:[MTCReadOnePicCell class]forCellReuseIdentifier:@"pool"];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [self.tableView registerClass:[MTCReadThreeCell class] forCellReuseIdentifier:@"pool2"];

}
- (void)configData{
    self.arrayOfRead = [NSMutableArray array];
    self.mArrayRead = [NSMutableArray array];
    NSString *str =@"http://c.3g.163.com/recommend/getSubDocPic?from=yuedu&size=20&passport=&devId=LTYcwrpXScWqtr8Nf%2BI8Pg%3D%3D&lat=zfDL4gTiHLhCL%2Bds9mCzaw%3D%3D&lon=mcWpYrydyuQWpOGaR94gHg%3D%3D&version=10.0&net=wifi&ts=1466578997&sign=1V8U0HXsbi%2FvnRozirxalYF1NR8Y%2BmEqnZkgBWBt5dV48ErR02zJ6%2FKXOnxX046I&encryption=1&canal=baidu_news&mac=TloFeR%2BBcRtLweZ9kML0cyKCZ0vHdesSTBiDEVNplbY%3D";
    NetworkingHandler *nt = [[NetworkingHandler alloc] init];
    
    [nt netWorkingHandlerGETWithURL:str completion:^(id result, NSData *data, NSURLResponse *response, NSError *error) {
        NSArray *arr = [result objectForKey:@"推荐"];
        
        for (NSDictionary *dic in arr) {
           self.model1 = [[MTCReadModel alloc] init];
            [self.model1 setValuesForKeysWithDictionary:dic];
            [self.arrayOfRead addObject:self.model1];
            [_model1 release];
//
//            if ([dic objectForKey:@"imgnewextra"]) {
//                self.mArrayRead = [dic objectForKey:@"imgsrc"];
//                [self.model2 setValuesForKeysWithDictionary:dic];
//                [self.arrayOfRead addObject:self.model2];
//                [_model2 release];

                
//                
//            }
//            [self.arrayOfRead addObject:self.mArrayRead];
        }
        [self.tableView reloadData];
        
        
    }];
    
    [nt release];
    

}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;

}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return self.arrayOfRead.count;


}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    
//    if (self.model1.imgnewextra == nil) {
    
        MTCReadOnePicCell *cell = [tableView dequeueReusableCellWithIdentifier:@"pool" forIndexPath:indexPath];
        
        cell.model = [self.arrayOfRead objectAtIndex:indexPath.row];
        
        return cell;
//    }else {
//        
//        MTCReadThreeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"pool2" forIndexPath:indexPath];
//        
//        cell.model = [self.arrayOfRead objectAtIndex:indexPath.row];
//        
//     
//        [cell.image1 sd_setImageWithURL:[NSURL URLWithString:self.model.imgsrc] placeholderImage:nil];
//        
//      
//        
//        [cell.image2 sd_setImageWithURL:[NSURL URLWithString:self.model.imgnewextra.firstObject] placeholderImage:nil];
//    
//        
//        [cell.image3 sd_setImageWithURL:[NSURL URLWithString:self.model.imgnewextra.lastObject] placeholderImage:nil];
//    
//    
//    
//        return cell;
//    }
    
    
    
    
    
    

}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 150;


}






@end
