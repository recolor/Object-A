//
//  MTCLiveCell.m
//  MTCProject-A
//
//  Created by dllo on 16/6/23.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCLiveCell.h"
#import "Masonry.h"
#import "MTCLiveModel.h"
#import "UIImageView+WebCache.h"

@interface MTCLiveCell()

@property (nonatomic, retain) UILabel *title;
@property (nonatomic, retain) UILabel *source;
@property (nonatomic, retain) UIImageView *imgsrc;
@property (nonatomic, retain) UILabel *user_count;


@end


@implementation MTCLiveCell
- (void)dealloc{
    [super dealloc];
    [_title release];
    [_source release];
    [_imgsrc release];
    [_user_count release];

}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self config];
    }
    return self;

}

- (void)config{
    self.title = [[UILabel alloc] init];
    self.source = [[UILabel alloc] init];
    self.imgsrc = [[UIImageView alloc] init];
    self.user_count =[[UILabel alloc] init];
    self.user_count.font = [UIFont systemFontOfSize:12];
    self.source.font = [UIFont systemFontOfSize:12];
    
    [self.contentView addSubview:self.title];
    [self.contentView addSubview:self.source];
    [self.contentView addSubview:self.imgsrc];
    [self.contentView addSubview:self.user_count];
    [self.title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).with.offset(10);
        make.left.equalTo(self.contentView).with.offset(10);
        make.height.offset(20);
        make.width.equalTo(self.contentView);
    }];
    [self.imgsrc mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).with.offset(40);
        make.left.equalTo(self.contentView).with.offset(10);
        make.right.equalTo(self.contentView).with.offset(-10);
        make.bottom.equalTo(self.contentView).with.offset(-40);
    }];
    
    [self.source mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.imgsrc.mas_bottom).with.offset(10);
        make.left.equalTo(self.contentView).with.offset(10);
        make.bottom.equalTo(self.contentView).with.offset(-10);
        make.width.offset(150);
        
    }];
    [self.user_count mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.imgsrc.mas_bottom).with.offset(10);
        make.bottom.equalTo(self.contentView).with.offset(-10);
        make.right.equalTo(self.contentView).with.offset(-10);
        make.width.offset(150);
        
    }];
    
    [_title release];
    [_source release];
    [_imgsrc release];
    [_user_count release];

   



}
- (void)setModel:(MTCLiveModel *)model{
    if (_model != model) {
        [_model release];
        [_model retain];
    }
    
    self.title.text = model.title;
    self.source.text = model.source;
    self.user_count.text = model.user_count;
    
    
    
    
    [self.imgsrc sd_setImageWithURL:[NSURL URLWithString:model.imgsrc] placeholderImage:nil];
    

}














@end
