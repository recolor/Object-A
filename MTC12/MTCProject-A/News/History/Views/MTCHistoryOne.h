//
//  MTCHistoryOne.h
//  MTCProject-A
//
//  Created by dllo on 16/6/28.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCBasicNewsTVC.h"
@class MTCHistoryModel;
@interface MTCHistoryOne : MTCBasicNewsTVC
@property (nonatomic, retain) MTCHistoryModel *model;
@end
