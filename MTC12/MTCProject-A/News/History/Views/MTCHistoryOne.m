//
//  MTCHistoryOne.m
//  MTCProject-A
//
//  Created by dllo on 16/6/28.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCHistoryOne.h"
#import "Masonry.h"
#import "UIImageView+WebCache.h"
#import "MTCHistoryModel.h"

@interface MTCHistoryOne()

@property (nonatomic, retain)UILabel *title;
@property (nonatomic, retain)UILabel *subscrip;//出版社
@property (nonatomic, retain)UIImageView *img1;//图片
@property (nonatomic, retain)UIImageView *img2;
@property (nonatomic, retain)UIImageView *img3;
@property (nonatomic, retain)UILabel *num;//跟帖


@end

@implementation MTCHistoryOne

-(void)dealloc{
    
    [_img1 release];
    [_img2 release];
    [_img3 release];
    [_subscrip release];
    [_title release];
    [_num release];
    [super dealloc];
    
    
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        
        [self config];
        
    }
    
    
    return self;
    
}
- (void)config{
    self.title = [[UILabel alloc]init];
    [self.contentView addSubview:self.title];
    self.title.font = [UIFont fontWithName:@"Arial" size:15];
    self.title.numberOfLines = 0;

    [self.title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).with.offset(10);
        make.left.equalTo(self.contentView.mas_left).with.offset(10);
        make.right.equalTo(self.contentView.mas_right).with.offset(-10);
        make.height.offset(20);
    }];
    
    
    
    
    self.img1 = [[UIImageView alloc]init];
    [self.contentView addSubview:self.img1];
  
    
    [self.img1 mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.contentView.mas_left).with.offset(10);
        
        make.top.equalTo(self.title.mas_bottom).with.offset(10);
        make.height.offset(100);
        make.width.offset((SCREEN_WIDTH - 40) / 3);
        
    }];
    
    self.img2 = [[UIImageView alloc]init];
    [self.contentView addSubview:self.img2];

    [self.img2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.img1.mas_right).with.offset(10);
        make.top.equalTo(self.title.mas_bottom).with.offset(10);
        make.height.offset(100);
        make.width.offset((SCREEN_WIDTH - 40) / 3);
        
        
        
    }];
    
    
    
    self.img3 = [[UIImageView alloc]init];
    [self.contentView addSubview:self.img3];

    [self.img3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.img2.mas_right).with.offset(10);
        make.top.equalTo(self.title.mas_bottom).with.offset(10);
        make.height.offset(100);
        make.width.offset((SCREEN_WIDTH - 40) / 3);
    }];
    
    
    
    
    
    self.subscrip = [[UILabel alloc]init];
    self.subscrip.font = [UIFont fontWithName:@"Arial" size:12];
    [self.contentView addSubview:self.subscrip];
    [self.subscrip mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.contentView.mas_bottom).with.offset(-10);
        make.left.equalTo(self.contentView.mas_left).with.offset(10);
        make.height.offset(20);
        make.width.offset(50);
        
    }];
    
    
    
    
    
    
    
    self.num = [[UILabel alloc]init];
    [self.contentView addSubview:self.num];
    self.num.textAlignment = NSTextAlignmentCenter;
    self.num.font = [UIFont fontWithName:@"Arial" size:12];

    [self.num mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.contentView.mas_bottom).with.offset(-10);
        make.right.equalTo(self.contentView).with.offset(-10);
        make.height.offset(20);
        make.width.offset(80);
    }];
    
    [_img1 release];
    [_img2 release];
    [_img3 release];
    [_subscrip release];
    [_title release];
    [_num release];



}
- (void)setModel:(MTCHistoryModel *)model{
    if (_model != model) {
        [_model release];
        _model = [model retain];
        
    }
    
    self.num.text = [model.replyCount.description stringByAppendingString:@"跟帖"];
    
    self.title.text = model.title;
    
    self.subscrip.text = model.source;
    
    [self.img1 sd_setImageWithURL:[NSURL URLWithString:model.imgsrc]];
    [self.img2 sd_setImageWithURL:[NSURL URLWithString:[model.imgextra[0]objectForKey:@"imgsrc"]]placeholderImage:nil];
    [self.img3 sd_setImageWithURL:[NSURL URLWithString:[model.imgextra[1]objectForKey:@"imgsrc"]]placeholderImage:nil];
    


}











@end
