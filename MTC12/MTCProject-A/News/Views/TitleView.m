//
//  TitleView.m
//  UI25_KVO
//
//  Created by Scott on 16/6/6.
//  Copyright © 2016年 Scott. All rights reserved.
//

#import "TitleView.h"





@implementation TitleView

#pragma mark - override
- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        
        [self createSubviews];
    }
    return self;
}

- (void)setTitles:(NSArray *)titles {
    
    for (int i = 0; i < 6; i++) {
        
        [self.subviews[i] setTitle:titles[i] forState:UIControlStateNormal];

    }
}

#pragma mark - private
- (void)createSubviews {
    
    for (int i = 0; i < 6; i++) {

        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [button setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        
        button.frame = CGRectMake(CGRectGetWidth(self.bounds) / 6 * i, 0, CGRectGetWidth(self.bounds) / 6, CGRectGetHeight(self.bounds));
        
        [self addSubview:button];
    }
}


@end
