//
//  MTC163NewsHotPicCell.m
//  MTCProject-A
//
//  Created by dllo on 16/6/16.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTC163NewsHotPicCell.h"
#import "Masonry.h"
#import "MTCModelOfHot.h"

@interface MTC163NewsHotPicCell()

@property (nonatomic, retain) UILabel *ladigest;
@property (nonatomic, retain) UILabel *ladownTimes;
@property (nonatomic, retain) UILabel *laupTimes;
@property (nonatomic, retain) UILabel *lareplyCount;
@property (nonatomic, retain) UILabel *laberZero;
@property (nonatomic, retain) UIButton *zanButton;
@property (nonatomic, retain) UIButton *laheiButton;
@property (nonatomic, retain) UIButton *xiaoxiButton;

@end

@implementation MTC163NewsHotPicCell
- (void)dealloc{
    [super dealloc];
    [_ladigest release];
    [_ladownTimes release];
    [_laupTimes release];
    [_lareplyCount release];
    [_laberZero release];
    [_zanButton release];
    [_laheiButton release];
    [_xiaoxiButton release];
    


}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self config];
        
    }
    return self;


}
- (void)config{
    self.ladigest = [[UILabel alloc] init];
    self.ladigest.font = [UIFont systemFontOfSize:17];
    [self.ladigest setNumberOfLines:10];
    
    self.ladownTimes = [[UILabel alloc] init];
    self.laupTimes = [[UILabel alloc] init];
    self.lareplyCount = [[UILabel alloc] init];
    self.laberZero = [[UILabel alloc] init];
    self.zanButton = [[UIButton alloc] init];
    self.laheiButton = [[UIButton alloc] init];
    self.xiaoxiButton = [[UIButton alloc] init];
    
    
    [self.contentView addSubview:self.ladigest];
    [self.contentView addSubview:self.laupTimes];
    [self.contentView addSubview:self.ladownTimes];
    [self.contentView addSubview:self.lareplyCount];
    [self.contentView addSubview:self.laberZero];
    [self.contentView addSubview:self.zanButton];
    [self.contentView addSubview:self.laheiButton];
    [self.contentView addSubview:self.xiaoxiButton];
    
    
    [self.ladigest mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).with.offset(0);
        make.left.equalTo(self.contentView).with.offset(10);
        make.right.equalTo(self.contentView).with.offset(-10);
        make.height.offset(150);
        
    }];
    
    [self.zanButton setImage:[UIImage imageNamed:@"赞.png"] forState:UIControlStateNormal];
    
    [self.zanButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.ladigest.mas_bottom).with.offset(31);
        make.left.equalTo(self.contentView).with.offset(10);
        make.bottom.equalTo(self.contentView).with.offset(-10);
        make.width.offset(20);
        
    }];
    
    [self.laupTimes mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.ladigest.mas_bottom).with.offset(31);
        make.left.equalTo(self.zanButton.mas_right).with.offset(10);
        make.bottom.equalTo(self.contentView).with.offset(-10);
        make.width.offset(40);
        
    }];

    self.laberZero.backgroundColor = [UIColor lightGrayColor];
    
    [self.laberZero mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.ladigest.mas_bottom).with.offset(20);
        make.left.equalTo(self.contentView).with.offset(30);
        make.right.equalTo(self.contentView).with.offset(-30);
        make.height.offset(1);
        
    }];
    [self.laheiButton setImage:[UIImage imageNamed:@"拉黑.png"] forState:UIControlStateNormal];
    
    [self.laheiButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.ladigest.mas_bottom).with.offset(31);
        make.left.equalTo(self.laupTimes.mas_right).with.offset(10);
        make.bottom.equalTo(self.contentView).with.offset(-10);
        make.width.offset(20);
        
    }];
    
    [self.ladownTimes mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.ladigest.mas_bottom).with.offset(31);
        make.bottom.equalTo(self.contentView).with.offset(-10);
        make.left.equalTo(self.laheiButton.mas_right).with.offset(10);
        make.width.offset(40);
        
    }];
    
    
    
    [self.lareplyCount mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.ladigest.mas_bottom).with.offset(31);
        make.right.equalTo(self.contentView).with.offset(-10);
        make.bottom.equalTo(self.contentView).with.offset(-10);
        make.width.offset(40);
        
    }];

    [self.xiaoxiButton setImage:[UIImage imageNamed:@"消息.png"] forState:UIControlStateNormal];
    
    [self.xiaoxiButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.ladigest.mas_bottom).with.offset(31);
        make.right.equalTo(self.lareplyCount.mas_left).with.offset(-10);
        make.bottom.equalTo(self.contentView).with.offset(-10);
        make.width.offset(20);

        
    }];
    [_ladigest release];
    [_ladownTimes release];
    [_laupTimes release];
    [_lareplyCount release];
    [_laberZero release];
    [_zanButton release];
    [_laheiButton release];
    [_xiaoxiButton release];

}

-(void)setModel:(MTCModelOfHot *)model{
    if (_model != model) {
        [_model release];
        [_model retain];
    }
    
    
    
    //正文
    self.ladigest.text = model.digest;
    //点赞 左对齐
    self.laupTimes.text = model.upTimes.description;
    self.laupTimes.textAlignment = NSTextAlignmentLeft;
    //拉黑 左对齐
    self.ladownTimes.text = model.downTimes.description;
    self.ladownTimes.textAlignment = NSTextAlignmentLeft;
    //信息
    self.lareplyCount.text = model.replyCount.description;

}






@end
