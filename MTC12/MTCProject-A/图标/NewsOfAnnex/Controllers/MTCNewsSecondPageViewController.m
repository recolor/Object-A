//
//  MTCNewsSecondPageViewController.m
//  MTCProject-A
//
//  Created by dllo on 16/6/25.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCNewsSecondPageViewController.h"

@interface MTCNewsSecondPageViewController ()

@end

@implementation MTCNewsSecondPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createWebView];
}

- (void)createWebView {
    
    NSString *str = [NSString stringWithFormat:@"http://3g.163.com/ntes/special/0034073A/article_share.html?docid=%@&spst=0&spss=newsapp&spsf=qq&spsw=1",_replyid];
    
    NSURL *url = [NSURL URLWithString:str];
    
    UIWebView *web = [[UIWebView alloc] initWithFrame:self.view.bounds];
    [self.view addSubview:web];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [web loadRequest:request];
    web.scalesPageToFit = YES;
    [web release];
    
}

@end
