//
//  MTCModelOfHot.h
//  MTCProject-A
//
//  Created by dllo on 16/6/16.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MTCModelOfHot : NSObject


//点赞
@property (nonatomic, retain) NSNumber *upTimes;
//拉黑
@property (nonatomic, retain) NSNumber *downTimes;
//标题名字
@property (nonatomic, copy) NSString *digest;
//小图
@property (nonatomic, copy) NSString *img;
//跟帖
@property (nonatomic, retain) NSNumber *replyCount;

@end
