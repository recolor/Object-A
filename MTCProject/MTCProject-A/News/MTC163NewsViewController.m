//
//  MTC163NewsViewController.m
//  MTCProject-A
//
//  Created by dllo on 16/6/14.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTC163NewsViewController.h"
#import "MTCPlayerViewController.h"
#import "MTC163NewsCollectionViewCell.h"
#import "MTCSearchViewController.h"

@interface MTC163NewsViewController ()



@end

@implementation MTC163NewsViewController


- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{

    self= [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        [self configTabBarItem];
        [self createTitleView];
    }
    return self;


}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    

}
- (void)configTabBarItem {
    self.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"新闻" image:[UIImage imageNamed:@"News.png"] tag:100];
    
    
    UIBarButtonItem *item1 = [[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"vidicon.png"] style:UIBarButtonItemStylePlain target:self action:@selector(handleVidicon:)] autorelease];
    
    UIBarButtonItem *item2 = [[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"search.png"] style:UIBarButtonItemStylePlain target:self action:@selector(handleSearch:)] autorelease];
    
    
    self.navigationItem.rightBarButtonItems = @[item1, item2];
    
    
}
- (void)handleVidicon:(UIBarButtonItem *)barbutton {

    MTCPlayerViewController *pvc = [[MTCPlayerViewController alloc] init];
    
    [self.navigationController pushViewController:pvc animated:YES];
    
    [pvc release];


}
- (void)handleSearch:(UIBarButtonItem *)barbutton{
    MTCSearchViewController *svc = [[MTCSearchViewController alloc] init];
    
    [self.navigationController pushViewController:svc animated:YES];
    
    [svc release];
 
}

- (void)createTitleView {
    
    UINavigationBar *bar = [UINavigationBar appearance];

    bar.barTintColor = [UIColor redColor];
}









@end
