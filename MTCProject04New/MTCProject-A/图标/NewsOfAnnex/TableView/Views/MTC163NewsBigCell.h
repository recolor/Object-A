//
//  MTC163NewsBigCell.h
//  MTCProject-A
//
//  Created by dllo on 16/6/15.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCBasicNewsTVC.h"
@class ModelOfHeadNews;
@interface MTC163NewsBigCell : MTCBasicNewsTVC
@property (nonatomic, retain) ModelOfHeadNews *model;
@end
