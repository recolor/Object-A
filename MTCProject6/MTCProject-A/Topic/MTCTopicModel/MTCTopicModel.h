//
//  MTCTopicModel.h
//  MTCProject-A
//
//  Created by dllo on 16/6/21.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MTCTopicModel : NSObject
//标题
@property (nonatomic, copy) NSString *name;
//生活 分类
@property (nonatomic, copy) NSString *classification;
//关注
@property (nonatomic, copy) NSString *concernCount;
//讨论
@property (nonatomic, copy) NSString *talkCount;
//图片
@property (nonatomic, retain) NSMutableArray *talkPicture;
//观点 发言
@property (nonatomic, copy) NSString *talkContent;
//观点内容
@property (nonatomic, copy) NSString *content;
//发言人头像
@property (nonatomic, copy) NSString *userHeadPicUrl;




@end
