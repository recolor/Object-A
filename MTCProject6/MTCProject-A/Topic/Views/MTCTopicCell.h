//
//  MTCTopicCell.h
//  MTCProject-A
//
//  Created by dllo on 16/6/21.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCBasicNewsTVC.h"
@class MTCTopicModel;
@interface MTCTopicCell : MTCBasicNewsTVC

@property (nonatomic, retain) UIImageView *talkPicture1;
@property (nonatomic, retain) UIImageView *talkPicture2;
@property (nonatomic, retain) UIImageView *talkPicture3;

@property (nonatomic, retain) MTCTopicModel *model;

@end
