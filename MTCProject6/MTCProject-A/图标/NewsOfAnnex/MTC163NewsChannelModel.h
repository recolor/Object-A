//
//  MTC163NewsChannelModel.h
//  MTCProject-A
//
//  Created by dllo on 16/6/14.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MTC163NewsChannelModel : NSObject
//大图
@property (nonatomic, copy) NSString *imgsrc;
//小图
@property (nonatomic, copy) NSString *img;
//标题
@property (nonatomic, copy) NSString *title;
//跟帖
@property (nonatomic, copy) NSString *replyCount;
//新闻热点
@property (nonatomic, copy) NSString *recSource;
//
@property (nonatomic, copy) NSString *imgenewextra;
//点赞
@property (nonatomic, copy) NSString *upTimes;
//拉黑
@property (nonatomic, copy) NSString *downTimes;
//标题名字
@property (nonatomic, copy) NSString *digest;



@end
