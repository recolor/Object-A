//
//  MTCReadViewController.m
//  MTCProject-A
//
//  Created by dllo on 16/6/22.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCReadViewController.h"
#import "Masonry.h"
#import "MTCRegisterViewController.h"
#import "MTCReadModel.h"
#import "UIImageView+WebCache.h"
#import "MTCReadOnePicCell.h"
#import "NetworkingHandler.h"



@interface MTCReadViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, retain) UIButton *buttonRegister;
@property (nonatomic, retain) UITableView *tableView;
@property (nonatomic, retain) NSMutableArray *arrayOfRead;

@property (nonatomic, retain) MTCReadModel *model1;
@property(nonatomic, retain) UIView *viewOfRead;



@end

@implementation MTCReadViewController
- (void)dealloc{

    [super dealloc];
    [_buttonRegister release];
    [_arrayOfRead release];
    [_model1 release];
    [_viewOfRead release];
    [_tableView release];

}
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self config];
    }
    return self;

}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self creatView];
    [self configTableView];
    [self configData];
    //固定
    self.automaticallyAdjustsScrollViewInsets = NO;
    
}
- (void)config{
    self.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"阅读" image:[UIImage imageNamed:@"readread.png"] tag:200];
    self.view.backgroundColor = [UIColor lightGrayColor];
    
 
}
- (void)creatView{
    self.viewOfRead= [[UIView alloc] init];
    [self.view addSubview:self.viewOfRead];
    self.viewOfRead.frame = CGRectMake(0, 64, SCREEN_WIDTH, 100);
    self.viewOfRead.backgroundColor = COLOR;
    [self.viewOfRead release];
    
    self.buttonRegister = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.viewOfRead addSubview:self.buttonRegister];
    [self.buttonRegister mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.viewOfRead.mas_centerX);
        make.centerY.equalTo(self.viewOfRead.mas_centerY);
        make.width.offset(300);
        make.height.offset(40);
    }];
    self.buttonRegister.backgroundColor = [UIColor whiteColor];
    [self.buttonRegister setTitle:@"立即登录" forState:UIControlStateNormal];
    [self.buttonRegister setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [self.buttonRegister addTarget:self action:@selector(handleRegister) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *name = [[UILabel alloc] init];
    name.text = @"获取更符合口味的推荐";
    [self.viewOfRead addSubview:name];
    [name mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.viewOfRead.mas_centerX);
        make.top.equalTo(self.buttonRegister.mas_bottom).with.offset(5);
        make.width.offset(300);
        make.height.offset(20);
        
        
    }];
    name.textColor = [UIColor colorWithRed:165 / 255.0f green:165 / 255.0f blue:165 / 255.0f alpha:1];
    name.textAlignment = NSTextAlignmentCenter;
    [name release];
    [self.buttonRegister release];
    
  

}
- (void)handleRegister{
    
    MTCRegisterViewController *registerVC = [[MTCRegisterViewController alloc] init];
    
    [self.navigationController pushViewController:registerVC animated:YES];
    

}

- (void)configTableView{

    self.tableView = [[UITableView alloc] init];
    self.tableView.frame = CGRectMake(0, 64, SCREEN_WIDTH, SCREEN_HEIGHT);
    
    [self.view addSubview:self.tableView];
    [self.tableView registerClass:[MTCReadOnePicCell class]forCellReuseIdentifier:@"pool"];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    //上边的view 加到tableView 头部
    self.tableView.tableHeaderView = self.viewOfRead;
    [self.tableView release];
   
}
- (void)configData{
    self.arrayOfRead = [NSMutableArray array];

    NSString *str =@"http://c.3g.163.com/recommend/getSubDocPic?from=yuedu&size=20&passport=&devId=LTYcwrpXScWqtr8Nf%2BI8Pg%3D%3D&lat=zfDL4gTiHLhCL%2Bds9mCzaw%3D%3D&lon=mcWpYrydyuQWpOGaR94gHg%3D%3D&version=10.0&net=wifi&ts=1466578997&sign=1V8U0HXsbi%2FvnRozirxalYF1NR8Y%2BmEqnZkgBWBt5dV48ErR02zJ6%2FKXOnxX046I&encryption=1&canal=baidu_news&mac=TloFeR%2BBcRtLweZ9kML0cyKCZ0vHdesSTBiDEVNplbY%3D";
    NetworkingHandler *nt = [[NetworkingHandler alloc] init];
    
    [nt netWorkingHandlerGETWithURL:str completion:^(id result, NSData *data, NSURLResponse *response, NSError *error) {
        NSArray *arr = [result objectForKey:@"推荐"];
        
        for (NSDictionary *dic in arr) {
           self.model1 = [[MTCReadModel alloc] init];
            [self.model1 setValuesForKeysWithDictionary:dic];
            [self.arrayOfRead addObject:self.model1];
            [_model1 release];

        }
        [self.tableView reloadData];
        
        
    }];
    
    [nt release];
    

}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;

}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return self.arrayOfRead.count;


}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    
        MTCReadOnePicCell *cell = [tableView dequeueReusableCellWithIdentifier:@"pool" forIndexPath:indexPath];
        
        cell.model = [self.arrayOfRead objectAtIndex:indexPath.row];
        
        return cell;


}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 150;


}






@end
