//
//  MTCReadOnePicCell.m
//  MTCProject-A
//
//  Created by dllo on 16/6/22.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCReadOnePicCell.h"
#import "Masonry.h"
#import "UIImageView+WebCache.h"
#import "MTCReadModel.h"

@interface MTCReadOnePicCell()
@property (nonatomic, retain) UILabel *title;
@property (nonatomic, retain) UILabel *source;
@property (nonatomic, retain) UIImageView *img;
@property (nonatomic, retain) UILabel *labelOne;

@end

@implementation MTCReadOnePicCell

-(void)dealloc{
    [super dealloc];
    [_title release];
    [_source release];
    [_img release];
    [_model release];
    [_labelOne release];
 
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self config];
    }
    return self;


}
- (void)config{
    self.title = [[UILabel alloc] init];
    self.source = [[UILabel alloc] init];
    self.img = [[UIImageView alloc] init];
    self.labelOne = [[UILabel alloc] init];
    
    self.title.font = [UIFont systemFontOfSize:17];
    [self.title setNumberOfLines:3];
    self.source.font = [UIFont systemFontOfSize:12];
    self.labelOne.backgroundColor = COLOR;
    
    [self.contentView addSubview:self.title];
    [self.contentView addSubview:self.source];
    [self.contentView addSubview:self.img];
    [self.contentView addSubview:self.labelOne];
    [self.img mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).with.offset(10);
        make.left.equalTo(self.contentView).with.offset(10);
        make.bottom.equalTo(self.contentView).with.offset(-40);
        make.width.equalTo(self.img.mas_height).multipliedBy(4.2 / 4.0f);
        
    }];
    [self.labelOne mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.img.mas_bottom).with.offset(9);
        make.left.equalTo(self.contentView).with.offset(30);
        make.right.equalTo(self.contentView).with.offset(-30);
        make.height.offset(1);
        
    }];
    
    [self.title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).with.offset(10);
        make.left.equalTo(self.img.mas_right).with.offset(10);
        make.right.equalTo(self.contentView).with.offset(-10);
        make.height.offset(80);
        
    }];
    [self.source mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.img.mas_bottom).with.offset(10);
        make.bottom.equalTo(self.contentView).with.offset(-10);
        make.left.equalTo(self.contentView).with.offset(10);
        make.width.offset(100);
        
    }];


    [_title release];
    [_source release];
    [_img release];
    [_labelOne release];



}

- (void)setModel:(MTCReadModel *)model{
    if (_model != model) {
        [_model release];
        [_model retain];
    }
    
    self.title.text = model.title;
    
    self.source.text = model.source;
    
    [self.img sd_setImageWithURL:[NSURL URLWithString:model.img] placeholderImage:nil];
    









}




@end
