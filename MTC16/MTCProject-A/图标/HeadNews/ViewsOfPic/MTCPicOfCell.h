//
//  MTCPicOfCell.h
//  MTCProject-A
//
//  Created by dllo on 16/6/20.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCBasicNewsTVC.h"
@class MTCPicOfModels;

@interface MTCPicOfCell : MTCBasicNewsTVC
@property (nonatomic, retain) MTCPicOfModels *model;

@property (nonatomic, retain) UIImageView *imageView1;
@property (nonatomic, retain) UIImageView *imageView2;
@property (nonatomic, retain) UIImageView *imageView3;

@end
