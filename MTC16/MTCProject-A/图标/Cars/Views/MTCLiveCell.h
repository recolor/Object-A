//
//  MTCLiveCell.h
//  MTCProject-A
//
//  Created by dllo on 16/6/23.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCBasicNewsTVC.h"

@class MTCLiveModel;

@interface MTCLiveCell : MTCBasicNewsTVC

@property (nonatomic, retain) MTCLiveModel *model;

@end
