//
//  MTCReadThreeCell.h
//  MTCProject-A
//
//  Created by dllo on 16/6/23.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCBasicNewsTVC.h"
@class MTCReadModelTwo;

@interface MTCReadThreeCell : MTCBasicNewsTVC

@property (nonatomic, retain) MTCReadModelTwo *model;
@property (nonatomic, retain) UIImageView *image1;
@property (nonatomic, retain) UIImageView *image2;
@property (nonatomic, retain) UIImageView *image3;

@end
