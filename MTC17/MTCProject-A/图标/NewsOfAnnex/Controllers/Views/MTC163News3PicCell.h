//
//  MTC163News3PicCell.h
//  MTCProject-A
//
//  Created by dllo on 16/6/15.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCBasicNewsTVC.h"
@class MTC163NewsChannelModel;

@interface MTC163News3PicCell : MTCBasicNewsTVC
@property (nonatomic, retain) MTC163NewsChannelModel *model;

@end
