//
//  MTC163NewsTVCOOfBasic.m
//  MTCProject-A
//
//  Created by dllo on 16/6/15.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTC163NewsTVCOOfBasic.h"
#import "NetworkingHandler.h"
#import "MTC163NewsNormalCell.h"
#import "MTC163NewsChannelModel.h"
#import "UIImageView+WebCache.h"
#import "picView.h"
#import "MTCNewsSecondPageViewController.h"
#import "MJRefresh.h"

@interface MTC163NewsTVCOOfBasic ()
@property (nonatomic, retain) NSMutableArray *newsArr;
@property (nonatomic, retain) NSMutableArray *mArrOfScrollView;
@property (nonatomic, retain) NSMutableArray *arr;
@property (nonatomic, assign) int i;


@end

@implementation MTC163NewsTVCOOfBasic
- (void)dealloc{
    self.i = 0;
    [super dealloc];
    [_newsArr release];
    [_mArrOfScrollView release];
    [_arr release];
  

}



- (void)viewDidLoad {
    [super viewDidLoad];

    [self tableViewConfig];
    [self handleData];
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        
        [self refreshData];
        
    }];
    
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        
        [self loadMoreData];
    }];
    


    
}
//下拉刷新
-(void)refreshData{
    
    NSString *str = @"http://c.m.163.com/nc/article/headline/T1348647853363/0-20.html?from=toutiao&fn=1&prog=LTitleA&passport=&devId=mBsLCfXm2lv8YSK2GLd21dwQ4rRpthDoBU0dy0QojpY%2F7ws6tlbT%2F9ohltwYpi4o&size=20&version=10.0&spever=false&net=wifi&lat=FcFDZ%2BcrBYKb1nLActnVXQ%3D%3D&lon=zVlpwKnWqGzeCuU1NGb5kw%3D%3D&ts=1465800962&sign=GhmXCJ8VWT1YGhTbRnMInB%2B%2BW%2BIIMgHC3hedoSR2an548ErR02zJ6%2FKXOnxX046I&encryption=1&canal=appstore";
    
    NetworkingHandler *nt = [[NetworkingHandler alloc] init];
    
    [nt netWorkingHandlerGETWithURL:str completion:^(id result, NSData *data, NSURLResponse *response, NSError *error) {
        
        self.arr = [result objectForKey:@"T1348647853363"];
        [self.newsArr removeAllObjects];
        
        
        for (NSDictionary *dic in _arr) {
            if (![dic objectForKey:@"ads"]) {
                
                MTC163NewsChannelModel *model = [[MTC163NewsChannelModel alloc] init];
                [model setValuesForKeysWithDictionary:dic];
                
                [self.newsArr addObject:model];
                [model release];
            } else {
                NSMutableArray *arrOfScrollView = [NSMutableArray array];
                self.mArrOfScrollView = [NSMutableArray array];
                arrOfScrollView = [dic objectForKey:@"ads"];
                
                for (NSDictionary *dic in arrOfScrollView) {
                    NSURL *url = [NSURL URLWithString:[dic objectForKey:@"imgsrc"]];
                    NSData *dataOfSV = [NSData dataWithContentsOfURL:url];
                    UIImage *image = [UIImage imageWithData:dataOfSV];
                    [self.mArrOfScrollView addObject:image];
                    
                    
                }
                [self configScrollView];
                
            }
            
        }
        
        [self.tableView reloadData];
        [self.tableView.mj_header endRefreshing];
        
    }];
    [nt release];



}
//加载
- (void)loadMoreData{
    
    self.i++;
    
    
    NSString *str = [NSString stringWithFormat:@"http://c.m.163.com/nc/article/headline/T1348647909107/%d-%d.html",self.i * 10, self.i *10 + 10];
    
    NSString *str1 = @"?from=toutiao&size=20&prog=LTitleA&fn=2&passport=&devId=1srtuLBfZ2iAHaF2R6bm5w%3D%3D&lat=Kcfy7ystb3A%2Bg%2FlxxbvrcA%3D%3D&lon=CZjZGc52u767xrx19rrUhw%3D%3D&version=10.0&net=wifi&ts=1464855170&sign=EeAS5QkNPYxYIXOqRj0w8Y10pnc1sPYkUVaaPUZdad148ErR02zJ6%2FKXOnxX046I&encryption=1&canal=miliao_news&mac=wWH4%2FMNJ4iOohL%2BSaWBtA0n4xdWy8S3keUAmEYPgEfc%3D";
    
    NSString *str2 = [str stringByAppendingString:str1];
    NetworkingHandler *nt = [[NetworkingHandler alloc] init];
    
    [nt netWorkingHandlerGETWithURL:str2 completion:^(id result, NSData *data, NSURLResponse *response, NSError *error) {
        
       NSArray *arr1 = [result objectForKey:@"T1348647909107"];
        
        
        
        for (NSDictionary *dic in arr1) {
            [self.arr addObject:dic];
            if (![dic objectForKey:@"ads"]) {
                
                MTC163NewsChannelModel *model = [[MTC163NewsChannelModel alloc] init];
                [model setValuesForKeysWithDictionary:dic];
                
                [self.newsArr addObject:model];
                [model release];
            } else {
                NSMutableArray *arrOfScrollView = [NSMutableArray array];
                self.mArrOfScrollView = [NSMutableArray array];
                arrOfScrollView = [dic objectForKey:@"ads"];
                
                for (NSDictionary *dic in arrOfScrollView) {
                    NSURL *url = [NSURL URLWithString:[dic objectForKey:@"imgsrc"]];
                    NSData *dataOfSV = [NSData dataWithContentsOfURL:url];
                    UIImage *image = [UIImage imageWithData:dataOfSV];
                    [self.mArrOfScrollView addObject:image];
                    
                    
                }
                [self configScrollView];
                
            }
            
        }
        
        [self.tableView reloadData];
        [self.tableView.mj_footer endRefreshing];
        
    }];
    [nt release];






}


//处理数据
- (void)handleData {
    
    self.newsArr = [NSMutableArray array];
    


    NSString *str = @"http://c.m.163.com/nc/article/headline/T1348647853363/0-20.html?from=toutiao&fn=1&prog=LTitleA&passport=&devId=mBsLCfXm2lv8YSK2GLd21dwQ4rRpthDoBU0dy0QojpY%2F7ws6tlbT%2F9ohltwYpi4o&size=20&version=10.0&spever=false&net=wifi&lat=FcFDZ%2BcrBYKb1nLActnVXQ%3D%3D&lon=zVlpwKnWqGzeCuU1NGb5kw%3D%3D&ts=1465800962&sign=GhmXCJ8VWT1YGhTbRnMInB%2B%2BW%2BIIMgHC3hedoSR2an548ErR02zJ6%2FKXOnxX046I&encryption=1&canal=appstore";
    
    NetworkingHandler *nt = [[NetworkingHandler alloc] init];
    
    [nt netWorkingHandlerGETWithURL:str completion:^(id result, NSData *data, NSURLResponse *response, NSError *error) {
        
        self.arr = [result objectForKey:@"T1348647853363"];
        
  
        
        for (NSDictionary *dic in _arr) {
            if (![dic objectForKey:@"ads"]) {
                
                MTC163NewsChannelModel *model = [[MTC163NewsChannelModel alloc] init];
                [model setValuesForKeysWithDictionary:dic];
 
                [self.newsArr addObject:model];
                [model release];
            } else {
                NSMutableArray *arrOfScrollView = [NSMutableArray array];
                self.mArrOfScrollView = [NSMutableArray array];
                arrOfScrollView = [dic objectForKey:@"ads"];
                
                for (NSDictionary *dic in arrOfScrollView) {
                    NSURL *url = [NSURL URLWithString:[dic objectForKey:@"imgsrc"]];
                    NSData *dataOfSV = [NSData dataWithContentsOfURL:url];
                    UIImage *image = [UIImage imageWithData:dataOfSV];
                    [self.mArrOfScrollView addObject:image];
                    
                  
                }
                [self configScrollView];

            }
            
        }
        
        [self.tableView reloadData];
        
    }];
    [nt release];
}

- (void)configScrollView {

    picView *bigView = [[picView alloc] initWithFrame:
                          CGRectMake(0, 40, SCREEN_WIDTH, 200)];
    [bigView addPictures:self.mArrOfScrollView];
    bigView.backgroundColor = [UIColor blueColor];
    
    self.tableView.tableHeaderView = bigView;
    [bigView release];
    
    
    

}


- (void)tableViewConfig {

    [self.tableView registerClass:[MTC163NewsNormalCell class] forCellReuseIdentifier:@"pool1"];
  
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;

}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return self.newsArr.count;

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
        MTC163NewsNormalCell *cell = [tableView dequeueReusableCellWithIdentifier:@"pool1" forIndexPath:indexPath];
        
        cell.model = [self.newsArr objectAtIndex:indexPath.row];
        
        return cell;
        
    }



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    
        return 100;
        
 
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    MTCNewsSecondPageViewController *ContentInfo = [[MTCNewsSecondPageViewController alloc] init];
    /** 数据传输 */
    ContentInfo.replyid = [self.arr[indexPath.row + 1] objectForKey:@"replyid"];
    
    /** push到二层界面 */
    [self.navigationController pushViewController:ContentInfo animated:YES];
    
    [ContentInfo release];
    
}




@end
