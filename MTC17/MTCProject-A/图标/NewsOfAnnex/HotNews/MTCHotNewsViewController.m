//
//  MTCHotNewsViewController.m
//  MTCProject-A
//
//  Created by dllo on 16/6/15.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCHotNewsViewController.h"
#import "MTCModelOfHot.h"
#import "NetworkingHandler.h"
#import "UIImageView+WebCache.h"
#import "MTC163NewsHotPicCell.h"
#import "MJRefresh.h"

@interface MTCHotNewsViewController ()

@property (nonatomic, retain) NSMutableArray *newsArr;
@property (nonatomic, retain) MTC163NewsHotPicCell *cell;


@end

@implementation MTCHotNewsViewController

-(void)dealloc {
    [super dealloc];
    [_newsArr release];
    [_cell release];




}



- (void)viewDidLoad {
    [super viewDidLoad];

    [self handleData];
    [self configTableView];
    self.tableView.separatorColor = [UIColor blackColor ];
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        
        [self refreshData];
        
    }];
    
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        
        [self loadMoreData];
    }];

}
//下拉刷新 拼接在原有的数据下面
- (void)refreshData{
    NSString *str = @"http://c.3g.163.com/recommend/getChanRecomNews?channel=duanzi&passport=FFh5WxU3p1EarBb67XYM4exu5Qgz9IwfxBMkTUgNz7o%3D&devId=PpbPMaO8nB1Bu3QwS33UWut4MJyEQ2%2FGVwsDoLfmO%2BdkAb1Kazpd5H3ScQnpE%2FUr&size=20&version=10.0&spever=false&net=wifi&lat=&lon=&ts=1465979959&sign=bGCG%2F4hTZ1I3ZY1fVC3d5MZraAgoBxn7rH22rWPM7XJ48ErR02zJ6%2FKXOnxX046I&encryption=1&canal=appstore";
    
    NetworkingHandler *nt  = [[NetworkingHandler alloc] init];
    
    [nt netWorkingHandlerGETWithURL:str completion:^(id result, NSData *data, NSURLResponse *response, NSError *error) {
        //下拉刷新时候把以前的数据移除
        [self.newsArr removeAllObjects];
        NSArray *array = [result objectForKey:@"段子"];
        
        for (NSDictionary *dic in array) {
            
            
            MTCModelOfHot *model = [[MTCModelOfHot alloc] init];
            [model setValuesForKeysWithDictionary:dic];
            
            [self.newsArr addObject:model];
            
            [model release];
            
            
        }
        
        [self.tableView reloadData];
        [self.tableView.mj_header endRefreshing];
        
        
    }];
    
    
    [nt release];
    
    
    
}

//上拉加载
- (void)loadMoreData{
    NSString *str = @"http://c.3g.163.com/recommend/getChanRecomNews?channel=duanzi&passport=FFh5WxU3p1EarBb67XYM4exu5Qgz9IwfxBMkTUgNz7o%3D&devId=PpbPMaO8nB1Bu3QwS33UWut4MJyEQ2%2FGVwsDoLfmO%2BdkAb1Kazpd5H3ScQnpE%2FUr&size=20&version=10.0&spever=false&net=wifi&lat=&lon=&ts=1465979959&sign=bGCG%2F4hTZ1I3ZY1fVC3d5MZraAgoBxn7rH22rWPM7XJ48ErR02zJ6%2FKXOnxX046I&encryption=1&canal=appstore";
    
    NetworkingHandler *nt  = [[NetworkingHandler alloc] init];
    
    [nt netWorkingHandlerGETWithURL:str completion:^(id result, NSData *data, NSURLResponse *response, NSError *error) {
        
        NSArray *array = [result objectForKey:@"段子"];
        
        for (NSDictionary *dic in array) {
            
            
            MTCModelOfHot *model = [[MTCModelOfHot alloc] init];
            [model setValuesForKeysWithDictionary:dic];
            
            [self.newsArr addObject:model];
            
            [model release];
            
            
        }
        
        [self.tableView reloadData];
        [self.tableView.mj_footer endRefreshing];
        
    }];
    
    
    [nt release];
    
    
    
    
    
    
}


// 美女 http://c.3g.163.com/recommend/getChanListNews?channel=T1456112189138&size=20&passport=&devId=LTYcwrpXScWqtr8Nf%2BI8Pg%3D%3D&lat=MBrtGyVBb3S46Yhu%2BurODw%3D%3D&lon=6Rn4ArfLGmPJaEhrvQowOw%3D%3D&version=10.0&net=wifi&ts=1466037065&sign=3Z4pOuO%2FW2ObOI0P3c3sIwEsPH5VfalA0eWI4v%2FrDKJ48ErR02zJ6%2FKXOnxX046I&encryption=1&canal=baidu_news&mac=TloFeR%2BBcRtLweZ9kML0cyKCZ0vHdesSTBiDEVNplbY%3D
// 段子 http://c.m.163.com/recommend/getChanListNews?channel=T1419316284722&size=20&passport=&devId=LTYcwrpXScWqtr8Nf%2BI8Pg%3D%3D&lat=pPfLP2kWA3X4TY4czM03jA%3D%3D&lon=TDkx7JwyswExc5muaav06Q%3D%3D&version=10.0&net=wifi&ts=1466076821&sign=LWcDL%2FX3lUt%2BldHIZfDnopgISDYKcBzNPR4GfSntstN48ErR02zJ6%2FKXOnxX046I&encryption=1&canal=baidu_news&mac=TloFeR%2BBcRtLweZ9kML0cyKCZ0vHdesSTBiDEVNplbY%3D













- (void)handleData {
    
      self.newsArr = [NSMutableArray array];
    
    NSString *str = @"http://c.3g.163.com/recommend/getChanRecomNews?channel=duanzi&passport=FFh5WxU3p1EarBb67XYM4exu5Qgz9IwfxBMkTUgNz7o%3D&devId=PpbPMaO8nB1Bu3QwS33UWut4MJyEQ2%2FGVwsDoLfmO%2BdkAb1Kazpd5H3ScQnpE%2FUr&size=20&version=10.0&spever=false&net=wifi&lat=&lon=&ts=1465979959&sign=bGCG%2F4hTZ1I3ZY1fVC3d5MZraAgoBxn7rH22rWPM7XJ48ErR02zJ6%2FKXOnxX046I&encryption=1&canal=appstore";
    
    NetworkingHandler *nt  = [[NetworkingHandler alloc] init];
    
    [nt netWorkingHandlerGETWithURL:str completion:^(id result, NSData *data, NSURLResponse *response, NSError *error) {
        
        NSArray *array = [result objectForKey:@"段子"];
        
        for (NSDictionary *dic in array) {
            
                
                MTCModelOfHot *model = [[MTCModelOfHot alloc] init];
                [model setValuesForKeysWithDictionary:dic];
            
                [self.newsArr addObject:model];
            
                [model release];
            
            
        }
        
        [self.tableView reloadData];
       
        
    }];
    
 
    [nt release];
    

}

- (void)configTableView{
    
    [self.tableView registerClass:[MTC163NewsHotPicCell class] forCellReuseIdentifier:@"pool"];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{

    return 1;
    

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{


    return self.newsArr.count;

}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    self.cell = [tableView dequeueReusableCellWithIdentifier:@"pool" forIndexPath:indexPath];
    self.cell.model = [self.newsArr objectAtIndex:indexPath.row];
    
    return self.cell;


}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    return 211;
    


}














@end
