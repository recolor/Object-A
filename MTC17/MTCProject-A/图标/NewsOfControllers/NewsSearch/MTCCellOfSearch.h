//
//  MTCCellOfSearch.h
//  MTCProject-A
//
//  Created by dllo on 16/7/1.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCBasicNewsTVC.h"

@class ModelOfSearch;

@interface MTCCellOfSearch : MTCBasicNewsTVC

@property (nonatomic, retain) ModelOfSearch *model;


@end
