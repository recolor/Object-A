//
//  ModelOfSearch.h
//  MTCProject-A
//
//  Created by dllo on 16/7/1.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ModelOfSearch : NSObject

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *ptime;
@property (nonatomic, copy) NSString *docid;


@end
