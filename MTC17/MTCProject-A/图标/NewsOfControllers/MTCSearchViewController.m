//
//  MTCSearchViewController.m
//  MTCProject-A
//
//  Created by dllo on 16/6/14.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCSearchViewController.h"
#import "ModelOfSearch.h"
#import "NetworkingHandler.h"
#import "UIImageView+WebCache.h"
#import "MTCCellOfSearch.h"
#import "SearchSecondViewController.h"


@import WebKit;

@interface MTCSearchViewController ()<UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, retain) UISearchBar *searchBar;
@property (nonatomic, retain) UIView *viewOfTop;
@property (nonatomic, retain) NSMutableArray *arr;
@property (nonatomic, retain) UITableView *tableView;
@property (nonatomic, retain) WKWebView *webView;

@end

@implementation MTCSearchViewController
- (void)dealloc{
    [super dealloc];
    [_searchBar release];
    [_viewOfTop release];
    [_arr release];
    [_tableView release];
    [_webView release];
    



}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self creatBackView];
    self.view.backgroundColor = [UIColor whiteColor];
    [self tableViewConfig];
    [self createSearch];

}
- (void)creatBackView {

    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStylePlain target:self action:@selector(handleBack)];
    

}
- (void)handleBack {
    [self.navigationController popToViewController:self.navigationController.viewControllers[0] animated:YES];
    
}
#pragma mark - searchBar
- (void)createSearch{
    self.searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 70, SCREEN_WIDTH, 50)];
    
    [self.view addSubview:self.searchBar];
    self.searchBar.layer.borderColor = [UIColor colorWithRed:100 green:100 blue:1 alpha:0].CGColor;
    self.searchBar.delegate = self;
    self.searchBar.layer.borderWidth = 1;
   
    self.searchBar.searchBarStyle = 1;
    self.searchBar.barTintColor = [UIColor colorWithRed:237/255.0 green:237.0/255.0 blue:237/255.0 alpha:1];
   
    self.searchBar.placeholder = @"请输入关键字";
    self.searchBar.tintColor = [UIColor blackColor];
    
    //自动弹出键盘
    [self.searchBar becomeFirstResponder];
    [_searchBar release];
    
}





#pragma mark - 转化为UTF8后搜索解析
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    self.arr = [NSMutableArray array];
    
    NSString *strOfSearch = [NSString stringWithFormat:@"http://c.3g.163.com/search/comp/MA==/40/%@.html?deviceId=ODYyMDk1MDIxNjc1NTcx&version=bmV3c2NsaWVudC41LjMuMy5hbmRyb2lk&channel=VDEzNDg2NDc5MDkxMDc=", [[[searchText dataUsingEncoding:NSUTF8StringEncoding] base64EncodedStringWithOptions:0] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
    
    NetworkingHandler *nt = [[NetworkingHandler alloc]init];
    [nt netWorkingHandlerGETWithURL:strOfSearch completion:^(id result, NSData *data, NSURLResponse *response, NSError *error){
        
        NSDictionary *dictionary = [result objectForKey:@"doc"];
        
        NSArray *Arr = [dictionary objectForKey:@"result"];
//        NSLog(@"111111%@", Arr);
        for (NSDictionary *dic in Arr) {
            
            ModelOfSearch *model = [[ModelOfSearch alloc]init];
            [model setValuesForKeysWithDictionary:dic];
            
            [self.arr addObject:model];
//            NSLog(@"3333%@", model);
//            
//            NSLog(@"6666%@", self.arr);
        }
        
        [self.tableView reloadData];
    }];

 
    
}
#pragma mark - tableView
- (void)tableViewConfig{
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 70, SCREEN_WIDTH, SCREEN_HEIGHT - 120) style:UITableViewStylePlain];
    self.tableView.dataSource= self;
    self.tableView.delegate = self;
    self.automaticallyAdjustsScrollViewInsets = YES;
    [self.view addSubview:self.tableView];
    //注册
    [self.tableView registerClass:[MTCCellOfSearch class] forCellReuseIdentifier:@"CellOfSearch"];
    [_tableView release];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.arr.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MTCCellOfSearch *cell = [tableView dequeueReusableCellWithIdentifier:@"CellOfSearch" forIndexPath:indexPath];
    
    cell.model = [self.arr objectAtIndex:indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    

    return 50;
    
}


- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [super touchesBegan:touches withEvent:event];
    [self.view endEditing:YES];
}


@end
