//
//  MTCTopicViewController.m
//  MTCProject-A
//
//  Created by dllo on 16/6/14.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCTopicViewController.h"
#import "NetworkingHandler.h"
#import "MTCTopicCell.h"
#import "MTCTopicModel.h"
#import "UIImageView+WebCache.h"
#import "Masonry.h"
//#import "MTCTopicCellOfPic.h"




@interface MTCTopicViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, retain) NSMutableArray *arrayOfTopic;

@property (nonatomic, retain) UITableView *tableView;



@end

@implementation MTCTopicViewController
-(void)dealloc{
    [super dealloc];
    [_arrayOfTopic release];
    [_tableView release];


}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self configTabBarItem];
    }
    return self;


}



- (void)viewDidLoad {
    [super viewDidLoad];
    [self creatTableView];
   
    [self configData];

   
    
    
 
    
}
- (void)creatTableView{
    
    self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds];

    [self.view addSubview:self.tableView];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;

    [self.tableView registerClass:[MTCTopicCell class] forCellReuseIdentifier:@"pool"];
    
    [_tableView release];
    
    
}
//初始化时候 调方法
- (void)configTabBarItem {

    self.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"话题" image:[UIImage imageNamed:@"topic.png"] tag:300];

  
}



- (void)configData{
    self.arrayOfTopic = [NSMutableArray array];


    
    NSString *str = @"http://topic.comment.163.com/topic/list/subject/0-10.html";
    NetworkingHandler *nt = [[NetworkingHandler alloc] init];
    [nt netWorkingHandlerGETWithURL:str completion:^(id result, NSData *data, NSURLResponse *response, NSError *error) {
        //字典 中套数组
        NSDictionary *dic = [result objectForKey:@"data"];
        NSArray *arr = [dic objectForKey:@"subjectList"];
        
//        NSLog(@"%@", result);
//        NSLog(@"%@", arr);
        for (NSDictionary *dic1 in arr) {
            
                 MTCTopicModel *model = [[MTCTopicModel alloc] init];
                 
                 [model setValuesForKeysWithDictionary:dic1];
                 
                 [self.arrayOfTopic addObject:model];
                 
                 [model release];
 
        
        }
        [self.tableView reloadData];
        
    }];
    
    [nt release];


}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;


}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return self.arrayOfTopic.count;


}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{


    
        
        MTCTopicCell *cell = [tableView dequeueReusableCellWithIdentifier:@"pool" forIndexPath:indexPath];
        
        cell.model = [self.arrayOfTopic objectAtIndex:indexPath.row];
        return cell;
    
    

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 200;

}








@end
