//
//  MTCMeTVCCell.m
//  MTCProject-A
//
//  Created by dllo on 16/6/17.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCMeTVCCell.h"
#import "Masonry.h"



@interface MTCMeTVCCell()

@property (nonatomic, retain) UIImageView *imageViewOfMe;
@property (nonatomic, retain) UILabel *labelOfMe;


@end



@implementation MTCMeTVCCell
- (void)dealloc{
    [super dealloc];
    [_imageViewOfMe release];
    [_labelOfMe release];
    [_pic release];
    [_content release];




}




//赋值
- (void)setContent:(NSString *)content{
    if (_content != content) {
        [_content release];
        _content = [content copy];
    }
    self.labelOfMe.text = content;
 
    
}
//赋值
- (void)setPic:(NSString *)pic {
    
    if (_pic != pic) {
        [_pic release];
        _pic = [pic retain];
    }
    self.imageViewOfMe.image = [UIImage imageNamed:pic];
    
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self config];
    }
    return self;



}


- (void)config{

    self.imageViewOfMe = [[UIImageView alloc] init];
  
    [self.contentView addSubview:self.imageViewOfMe];
    [_imageViewOfMe release];
    
    self.labelOfMe = [[UILabel alloc]init];
    [self.contentView addSubview:self.labelOfMe];
    [self.labelOfMe release];
    
    

}

- (void)layoutSubviews {
    
    [super layoutSubviews];
    
    [self.imageViewOfMe mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.contentView).with.offset(15);
                make.left.equalTo(self.contentView).with.offset(20);
                make.bottom.equalTo(self.contentView).with.offset(-15);
                make.width.offset(16);
             
             
            }];

    
    [self.labelOfMe mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.top.equalTo(self.contentView).with.offset(10);
                    make.left.equalTo(self.imageViewOfMe.mas_right).with.offset(10);
                    make.bottom.equalTo(self.contentView).with.offset(-10);
                    make.width.offset(80);
                    
                }];

    
}


@end
