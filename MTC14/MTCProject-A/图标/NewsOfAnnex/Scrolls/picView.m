//
//  picView.m
//  UI_06HOU
//
//  Created by Apple on 16/5/10.
//  Copyright © 2016年 Apple. All rights reserved.
//

#import "picView.h"
#import "pictureScrollView.h"
@interface picView()<UIScrollViewDelegate>
@property (nonatomic, retain)UIPageControl *page;
@property (nonatomic, retain)pictureScrollView *picSroll;
@end
@implementation picView
- (void)dealloc{
    [_page release];
    [_picSroll release];
    [super dealloc];
}
#pragma mark - override
- (instancetype)initWithFrame:(CGRect)frame{
    self =[super initWithFrame: frame];
    if (self) {
        //初始化子控件.
        self.picSroll = [[pictureScrollView alloc]initWithFrame:self.bounds];
        [self addSubview:self.picSroll];
       [_picSroll release];
        self.picSroll.delegate = self;
        self.page = [[UIPageControl alloc]initWithFrame:CGRectMake(0, CGRectGetHeight(self.bounds) - 40, CGRectGetWidth(self.bounds), 40)];
   
        [self addSubview:self.page];
       [_page release];
        [self.page addTarget:self action:@selector(hanlePage:) forControlEvents:UIControlEventValueChanged];
        
    }
    return self;
}
#pragma mark - page
- (void)hanlePage:(UIPageControl * )page{
    //设置scoll.contentOffset偏移量.
    [self.picSroll  setContentOffset:CGPointMake(page.currentPage *CGRectGetWidth(self.bounds), 0) animated:YES];
}


#pragma mark - scrollView Delegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    //白点
    self.page.currentPage = scrollView.contentOffset.x / CGRectGetWidth(self.bounds);
}
- (void)addPictures:(NSArray<UIImage *>*)pictures;{
    [self.picSroll addPictures:pictures];
    self.page.numberOfPages = pictures.count;
}














@end
