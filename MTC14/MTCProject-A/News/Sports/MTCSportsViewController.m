//
//  MTCSportsViewController.m
//  MTCProject-A
//
//  Created by dllo on 16/6/27.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCSportsViewController.h"
#import "picView.h"
#import "NetworkingHandler.h"
#import "MTC163NewsChannelModel.h"
#import "MTC163NewsNormalCell.h"
#import "MTC163News3PicCell.h"

@interface MTCSportsViewController ()
@property (nonatomic, retain) NSMutableArray *newsArr;
@property(nonatomic, retain)NSMutableArray *arrOfScrollView;
@property(nonatomic, retain)NSMutableArray *mArrOfScrollView;
@end

@implementation MTCSportsViewController
- (void)dealloc{
    [super dealloc];
    [_newsArr release];
    [_arrOfScrollView release];
    [_mArrOfScrollView release];



}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self configScrollView];
    [self handleData];
    [self tableViewConfig];
    
  
}
- (void)configScrollView{
    
    picView *viewHead = [[picView alloc]initWithFrame:CGRectMake(0, 70, SCREEN_WIDTH, 200)];
    
    [viewHead addPictures:self.mArrOfScrollView];
    
    viewHead.backgroundColor = [UIColor cyanColor];
    self.tableView.tableHeaderView = viewHead;
    [viewHead release];
    
}
- (void)handleData {
    
    self.newsArr = [NSMutableArray array];
    
    NSString *str = @"http://c.m.163.com/nc/article/list/T1348649079062/0-20.html";
    
    NetworkingHandler *nt = [[NetworkingHandler alloc] init];
    [nt netWorkingHandlerGETWithURL:str completion:^(id result, NSData *data, NSURLResponse *response, NSError *error) {
        NSArray *arr = [result objectForKey:@"T1348649079062"];
        for (NSDictionary *dic in arr) {
            if ([dic objectForKey:@"ads"]) {
                
                NSMutableArray *arrOfScrollView = [NSMutableArray array];
                self.mArrOfScrollView = [NSMutableArray array];
                arrOfScrollView = [dic objectForKey:@"ads"];
                
                for (NSDictionary *dic in arrOfScrollView) {
                    NSURL *url = [NSURL URLWithString:[dic objectForKey:@"imgsrc"]];
                    
                    NSData *dataOfSV = [NSData dataWithContentsOfURL:url];
                    UIImage *image = [UIImage imageWithData:dataOfSV];
                    
                    [self.mArrOfScrollView addObject:image];
                    
                }
                [self configScrollView];
            }else{
                
                
               MTC163NewsChannelModel *model = [[MTC163NewsChannelModel alloc] init];
                
                [model setValuesForKeysWithDictionary:dic];
                
                [self.newsArr addObject:model];
                [model release];
                
            }
        }
        [self.tableView reloadData];
    }];
    [nt release];
}
- (void)tableViewConfig {
    
    [self.tableView registerClass:[MTC163NewsNormalCell class]
        forCellReuseIdentifier:@"MTC163NewsNormalCell"];
    
    [self.tableView registerClass:[MTC163News3PicCell class] forCellReuseIdentifier:@"MTC163News3PicCell"];
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return self.newsArr.count;

}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MTC163NewsChannelModel *model = self.newsArr[indexPath.row];
    if (!model.imgextra) {
        MTC163NewsNormalCell *cell
        = [tableView dequeueReusableCellWithIdentifier:@"MTC163NewsNormalCell" forIndexPath:indexPath];
        cell.model = self.newsArr[indexPath.row];
        return cell;
    }else{
        MTC163News3PicCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MTC163News3PicCell" forIndexPath:indexPath];
        cell.model = self.newsArr[indexPath.row];
        return cell;
        
        
        
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 120;
}






@end
