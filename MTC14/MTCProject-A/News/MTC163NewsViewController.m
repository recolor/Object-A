//
//  MTC163NewsViewController.m
//  MTCProject-A
//
//  Created by dllo on 16/6/14.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTC163NewsViewController.h"
#import "MTCPlayerViewController.h"
#import "MTC163NewsTVCOOfBasic.h"
#import "MTCSearchViewController.h"
#import "MTC163NewsChannelModel.h"
#import "MTCHotNewsViewController.h"
#import "MTCHeadNewsViewController.h"
#import "MTCLiveViewController.h"
#import "MTCSportsViewController.h"
#import "Masonry.h"
#import "TitleView.h"
#import "MTCHistoryViewController.h"

@interface MTC163NewsViewController ()<UICollectionViewDelegate, UICollectionViewDataSource, UIScrollViewDelegate>

//频道
@property (nonatomic, retain) UIView *viewOfRedline;
@property (nonatomic, retain) TitleView *viewOfTitle;
//新闻
@property (nonatomic, retain) UIScrollView *scrollView;



@end

@implementation MTC163NewsViewController

- (void)dealloc{
    [super dealloc];
    [_scrollView release];

    [_viewOfRedline release];
    [_viewOfTitle release];

}





- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{

    self= [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        [self createTitleView];
    }
    return self;


}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self creatScollView];
    [self configTabBarItem];
    [self scrollViewConfig];
    [self createTitleViews];
    [self createViewOfRedLine];
    

    

}
//
- (void)createViewOfRedLine {
    
    self.viewOfRedline = [[UIView alloc] initWithFrame:CGRectMake(0, 102, CGRectGetWidth(self.view.bounds) / 6, 2)];
    
    [self.view addSubview:self.viewOfRedline];
    
    self.viewOfRedline.backgroundColor = [UIColor redColor];
    [_viewOfRedline release];
}
//头部view
- (void)createTitleViews {
    
    self.viewOfTitle = [[TitleView alloc] initWithFrame:CGRectMake(0, 64, CGRectGetWidth(self.view.bounds), 36)];
    
    self.viewOfTitle.titles = @[@"首页", @"段子", @"图片", @"直播", @"体育", @"历史"];
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.viewOfTitle];
    [_viewOfTitle release];
    
}
//KVO
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    
    // collectionView
    CGFloat x = [[change objectForKey:@"new"] CGPointValue].x / 6.0f;
    
    self.viewOfRedline.frame = CGRectMake(x, 100, CGRectGetWidth(self.view.bounds) / 6, 4);
    
    
    
}

#pragma make - 标题
- (void)createTitleView {
    
    UINavigationBar *bar = [UINavigationBar appearance];
    
    bar.barTintColor = [UIColor redColor];
    


    
}
- (void)creatScollView {
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 104, SCREEN_WIDTH, SCREEN_HEIGHT - 104 - 49)];
    
    [self.view addSubview:self.scrollView];

    self.scrollView.pagingEnabled = YES;
    self.scrollView.showsHorizontalScrollIndicator = NO;
   
    self.scrollView.bounces = NO;
    [_scrollView release];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    // KVO 观察collectionView的offset.
    [self.scrollView addObserver:self forKeyPath:@"contentOffset" options:NSKeyValueObservingOptionNew context:nil];
    

}

//添加
- (void)scrollViewConfig {

    MTC163NewsTVCOOfBasic *fieNewsVC = [[MTC163NewsTVCOOfBasic alloc] init];
    MTCHotNewsViewController *hotNewsVC = [[MTCHotNewsViewController alloc] init];
    MTCHeadNewsViewController *headNewsVC = [[MTCHeadNewsViewController alloc] init];
    MTCLiveViewController *carNewsVC = [[MTCLiveViewController alloc] init];
    
    MTCSportsViewController *sportsNewsVC = [[MTCSportsViewController alloc] init];
    MTCHistoryViewController *historyNewsVC = [[MTCHistoryViewController alloc] init];
    
    
    self.scrollView.contentSize = CGSizeMake(SCREEN_WIDTH * 6, SCREEN_HEIGHT - 49 - 104);
    
    
    fieNewsVC.view.frame = self.scrollView.bounds;
    [self.scrollView addSubview:fieNewsVC.view];
    [fieNewsVC release];
    
    hotNewsVC.view.frame = CGRectMake(SCREEN_WIDTH, 0, SCREEN_WIDTH, CGRectGetHeight(self.scrollView.bounds));
    [self.scrollView addSubview:hotNewsVC.view];
    [hotNewsVC release];
    
    headNewsVC.view.frame = CGRectMake(SCREEN_WIDTH * 2, 0, SCREEN_WIDTH, CGRectGetHeight(self.scrollView.bounds));
    [self.scrollView addSubview:headNewsVC.view];
    [headNewsVC release];
    
    carNewsVC.view.frame = CGRectMake(SCREEN_WIDTH * 3, 0, SCREEN_WIDTH, CGRectGetHeight(self.scrollView.bounds));
    [self.scrollView addSubview:carNewsVC.view];
    [carNewsVC release];
    
    sportsNewsVC.view.frame = CGRectMake(SCREEN_WIDTH * 4, 0, SCREEN_WIDTH, CGRectGetHeight(self.scrollView.bounds));
    [self.scrollView addSubview:sportsNewsVC.view];
    [sportsNewsVC release];
    
    historyNewsVC.view.frame = CGRectMake(SCREEN_WIDTH * 5, 0, SCREEN_WIDTH, CGRectGetHeight(self.scrollView.bounds));
    [self.scrollView addSubview:historyNewsVC.view];
    [historyNewsVC release];
    
  
    
    [self addChildViewController:fieNewsVC];
    [self addChildViewController:hotNewsVC];
    [self addChildViewController:headNewsVC];
    [self addChildViewController:carNewsVC];
    [self addChildViewController:sportsNewsVC];
    [self addChildViewController:historyNewsVC];
    
    

}

//網易 搜索 
- (void)configTabBarItem {
    self.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"新闻" image:[UIImage imageNamed:@"News.png"] tag:100];
    
    self.navigationItem.title = @"網易";
    
    
    UIBarButtonItem *item2 = [[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"search.png"] style:UIBarButtonItemStylePlain target:self action:@selector(handleSearch:)] autorelease];
    
    
    self.navigationItem.rightBarButtonItems = @[item2];
    
    
}

- (void)handleSearch:(UIBarButtonItem *)barbutton{
    MTCSearchViewController *svc = [[MTCSearchViewController alloc] init];
    
    [self.navigationController pushViewController:svc animated:YES];
    
    [svc release];
 
}












@end
