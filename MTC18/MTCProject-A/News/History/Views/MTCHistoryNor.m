//
//  MTCHistoryNor.m
//  MTCProject-A
//
//  Created by dllo on 16/6/28.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCHistoryNor.h"
#import "MTCHistoryModel.h"
#import "UIImageView+WebCache.h"
#import "Masonry.h"

@interface MTCHistoryNor()

@property (nonatomic, retain)UILabel *title;
@property (nonatomic, retain)UILabel *source;
@property (nonatomic, retain)UIImageView *img;
@property (nonatomic, retain)UILabel *replycount;


@end
@implementation MTCHistoryNor
-(void)dealloc{
    [_replycount release];
    [_img release];
    [_source release];
    [_title release];
 
    [_model release];
    [super dealloc];
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
      
        [self config];
        
    }
    
    
    return self;
    
}
- (void)config{
    self.img = [[UIImageView alloc]init];
    [self.contentView addSubview:self.img];

    [self.img mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).with.offset(-10);
        make.top.equalTo(self.contentView.mas_top).with.offset(15);
        make.bottom.equalTo(self.contentView.mas_bottom).with.offset(-15);
        make.width.offset(100);
    }];
    
    
    self.title = [[UILabel alloc]init];
    [self.contentView addSubview:self.title];
    self.title.font = [UIFont fontWithName:@"Arial" size:15];
    self.title.numberOfLines = 0;

    
    [self.title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.img.mas_left).with.offset(-10);
        make.top.equalTo(self.contentView.mas_top).with.offset(15);
        make.left.equalTo(self.contentView).with.offset(10);
        make.height.offset(36);
    }];
    
    
    
    
    self.source = [[UILabel alloc]init];
    
    [self.contentView addSubview:self.source];
    self.source.font = [UIFont fontWithName:@"Arial" size:11];
    
    [self.source mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.title.mas_bottom).with.offset(20);
        make.right.equalTo(self.img.mas_left).with.offset(-10);
        make.width.offset(100);
        make.bottom.equalTo(self.contentView.mas_bottom).with.offset(-15);
    }];
    
    
    
    
    self.replycount = [[UILabel alloc]init];
    [self.contentView addSubview:self.replycount];
    self.replycount.textAlignment = NSTextAlignmentCenter;
    self.replycount.font = [UIFont fontWithName:@"Arial" size:12];

    [self.replycount mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.title.mas_bottom).with.offset(20);
        make.left.equalTo(self.contentView).with.offset(10);
        make.width.offset(80);
        make.bottom.equalTo(self.contentView).with.offset(-10);
    }];
    [self.img release];
    [self.title release];
    [self.source release];
    [self.replycount release];

}
- (void)setModel:(MTCHistoryModel *)model{
    if (_model != model) {
        _model = [model retain];
        [_model release];
    }
    
    [self.img sd_setImageWithURL:[NSURL URLWithString:model.imgsrc]placeholderImage:nil];
    
    
    self.title.text = model.title;
    
    self.source.text = model.source;
    
    self.replycount.text = [model.replyCount.description stringByAppendingString:@"跟帖"];


}






@end
