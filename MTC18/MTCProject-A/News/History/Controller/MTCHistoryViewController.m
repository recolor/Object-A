//
//  MTCHistoryViewController.m
//  MTCProject-A
//
//  Created by dllo on 16/6/28.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCHistoryViewController.h"
#import "NetworkingHandler.h"
#import "MTCHistoryModel.h"
#import "MTCHistoryNor.h"
#import "MTCHistoryOne.h"
#import "MTCHistoryThree.h"
#import "Masonry.h"
#import "picView.h"


@interface MTCHistoryViewController ()

@property(nonatomic, retain)NSMutableArray *mArrOfRelax;
@property(nonatomic, retain)NSMutableArray *mArrOfScrollView;
@property(nonatomic, retain)NSMutableArray *data;
@property(nonatomic, retain)NSString *string;

@end

@implementation MTCHistoryViewController
-(void)dealloc{
    
    [super dealloc];
    [_mArrOfScrollView release];
    [_data release];
    [_string release];
    [_mArrOfRelax release];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createData];
    [self screateView];
    [self configTableView];
    self.tableView.frame = CGRectMake(0, 64, SCREEN_WIDTH, SCREEN_HEIGHT - 49);
    
}
-(void)createData{
    
    self.mArrOfRelax = [NSMutableArray array];
    
    NSString *str = @"http://c.m.163.com/nc/article/list/T1368497029546/0-20.html";
    
    NetworkingHandler *nt = [[NetworkingHandler alloc]init];
    
    [nt netWorkingHandlerGETWithURL:str completion:^(id result, NSData *data, NSURLResponse *response, NSError *error) {
        
 
        self.data= [result objectForKey:@"T1368497029546"];
  
        for (NSDictionary *dic in self.data) {
            if (![dic objectForKey:@"hasHead"] ) {
                MTCHistoryModel *model = [[MTCHistoryModel alloc] init];
                [model setValuesForKeysWithDictionary:dic];
                [self.mArrOfRelax addObject:model];
                
                [model release];
            }else {
                
                self.mArrOfScrollView = [NSMutableArray array];
      
                NSURL *url = [NSURL URLWithString:[dic objectForKey:@"imgsrc"]];
                self.string = [dic objectForKey:@"title"];
                
                NSData *dataOfSV = [NSData dataWithContentsOfURL:url];
                UIImage *image = [UIImage imageWithData:dataOfSV];
                
                [self.mArrOfScrollView addObject:image];
                
                
                [self screateView];
            }
            [self.tableView reloadData];
        }
    }];
    [nt release];
}
-(void)screateView{
    
    
    
    picView  *ViewHead = [[picView alloc]initWithFrame:CGRectMake(0, 72, SCREEN_WIDTH, 200)];
    UILabel *lable = [[UILabel alloc]init];
    
    
    [ViewHead addPictures:self.mArrOfScrollView];
    
    
    self.tableView.tableHeaderView = ViewHead;
    [ViewHead release];
    [lable release];
    
    
    
}
-(void)configTableView{
    
    [self.tableView registerClass:[MTCHistoryNor class] forCellReuseIdentifier:@"MTCHistoryNor"];
    
    [self.tableView registerClass:[MTCHistoryOne class] forCellReuseIdentifier:@"MTCHistoryOne"];
    
    
    [self.tableView registerClass:[MTCHistoryThree class] forCellReuseIdentifier:@"MTCHistoryThree"];
    
    
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
    
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.mArrOfRelax.count;
    
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MTCHistoryModel *model = [self.mArrOfRelax objectAtIndex:indexPath.row];
    
    if (model.imgextra) {
        
        MTCHistoryOne *cell = [tableView dequeueReusableCellWithIdentifier:@"MTCHistoryOne" forIndexPath:indexPath];
        cell.model = [self.mArrOfRelax objectAtIndex:indexPath.row];
        
        return cell;
        
    }else if (model.TAGS){
        
        MTCHistoryThree *cell = [tableView dequeueReusableCellWithIdentifier:@"MTCHistoryThree" forIndexPath:indexPath];
        cell.model = [self.mArrOfRelax objectAtIndex:indexPath.row];
        return cell;
        
        
    }else{
        
        MTCHistoryNor *cell = [tableView dequeueReusableCellWithIdentifier:@"MTCHistoryNor" forIndexPath:indexPath];
        
        cell.model = [self.mArrOfRelax objectAtIndex:indexPath.row];
        
        return cell;
        
    }
    
    
    
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MTCHistoryModel *model = [self.mArrOfRelax objectAtIndex:indexPath.row];
    
    if (model.imgextra) {
        return 200;//三个图
        
        
    }else if (model.TAGS){
        
        return 270;//一个大图
        
        
    }else{
        
        return 100;//一个小图
        
        
    }
    
    
    
    
}



@end
