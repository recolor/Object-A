//
//  MTCDBHandler.m
//  MTCProject-A
//
//  Created by dllo on 16/7/4.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCDBHandler.h"
#import <sqlite3.h>
#import "MTC163NewsChannelModel.h"
@implementation MTCDBHandler

+ (instancetype)sharedDBHandler {
    
    static MTCDBHandler *db = nil;
    
    if (db == nil) {
        db = [[MTCDBHandler alloc] init];
    }
    
    return db;
    
}

sqlite3 *db;

- (void)openDB {
    
    if (db) {
        NSLog(@"数据库已经打开");
        return;
    }
    
    NSString *path = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingString:@"Model.db"];
    
    NSLog(@"%@", path);
    
    //  Core API: sqlite3_apen
    int result = sqlite3_open(path.UTF8String, &db);
    
    if (SQLITE_OK == result) {
        NSLog(@"数据库打开成功!!");
    } else {
        
        NSLog(@"数据库打开失败, code:%d", result);
    }
    
}

- (void)createTable {
    
    NSString *sql = @"CREATE TABLE IF NOT EXISTS ContentPlist(number INTEGER PRIMARY KEY AUTOINCREMENT, recSource text, title text, imgsrc text)";
    
    //  Core API: sqlite3_exec
    int result = sqlite3_exec(db, sql.UTF8String, NULL, NULL, nil);
    
    if (result == SQLITE_OK) {
        NSLog(@"创建表格成功");
    } else {
        
        NSLog(@"创建表格失败. code:%d", result);
    }
    
}

- (void)insertModel:(MTC163NewsChannelModel *)model {
    
    NSString *sql = [NSString stringWithFormat:@"INSERT into ContentPlist(recSource, title, imgsrc) VALUES('%@', '%@', '%@')", model.recSource, model.title, model.imgsrc];
    
    int result = sqlite3_exec(db, sql.UTF8String, NULL, NULL, nil);
    
    if (result == SQLITE_OK) {
        NSLog(@"添加数据完成!");
    } else {
        
        NSLog(@"添加数据失败. code:%d", result);
    }
    
}

- (void)deleteModeltWithTitle:(NSString *)title {
    
    NSString *str = [NSString stringWithFormat:@"delete from ContentPlist where title = '%@'", title];
    
    //核心API
    
    int result = sqlite3_exec(db, str.UTF8String, NULL, NULL, nil);
    
    if (result == SQLITE_OK)
    {
        NSLog(@"删除成功");
    } else {
        NSLog(@"删除失败, code:%d", result);
    }
    
}

- (NSArray <MTC163NewsChannelModel *> *)getData {
    
    NSString *sql = [NSString stringWithFormat:@"SELECT * FROM ContentPlist"];
    NSLog(@"%@", sql);
    NSMutableArray *mArr = [NSMutableArray array];
    
    sqlite3_stmt *stmt = nil;
    int result = sqlite3_prepare_v2(db, sql.UTF8String, -1, &stmt, nil);
    
    if (result == SQLITE_OK)
    {
        NSLog(@"准备取出.....");
        while (sqlite3_step(stmt) == SQLITE_ROW)
        {
            //将匹配的行取出来
            const unsigned char *recSource = sqlite3_column_text(stmt, 1);
            const unsigned char *title = sqlite3_column_text(stmt, 2);
            const unsigned char *imgsrc = sqlite3_column_text(stmt, 3);
            MTC163NewsChannelModel *model = [[MTC163NewsChannelModel alloc]init];
            
            model.recSource = [NSString stringWithUTF8String:(const char *)recSource];
            
            model.title = [NSString stringWithUTF8String:(const char *)title];
            
            model.imgsrc = [NSString stringWithUTF8String:(const char *)imgsrc];
            [mArr addObject:model];
        }
    }
    
    else
    {
        NSLog(@"取出失败, code:%d", result);
        
    }
    
    
    return mArr;
    
}



- (NSArray<MTC163NewsChannelModel *> *)searchForTitle:(NSString *)title {
    
    NSMutableArray *mArr = [NSMutableArray array];
    
    NSString *sql = [NSString stringWithFormat:@"SELECT * FROM ContentPlist WHERE name = '%@'", title];
    
    // Core API:stmt
    sqlite3_stmt *stmt = nil;
    
    int result = sqlite3_prepare(db, sql.UTF8String, -1, &stmt, nil);
    
    if (SQLITE_OK == result) {
        NSLog(@"准备查询");
        
        while (sqlite3_step(stmt) == SQLITE_ROW) {
            
            //  将匹配的行的值取出来.
            const unsigned char *recSource = sqlite3_column_text(stmt, 1);
            const unsigned char *title = sqlite3_column_text(stmt, 2);
            const unsigned char *imgsrc = sqlite3_column_text(stmt, 3);
            MTC163NewsChannelModel *model = [[MTC163NewsChannelModel alloc]init];
            
            model.recSource = [NSString stringWithUTF8String:(const char *)recSource];
            
            model.title = [NSString stringWithUTF8String:(const char *)title];
            
            model.imgsrc = [NSString stringWithUTF8String:(const char *)imgsrc];
            [mArr addObject:model];
        }
        
    } else {
        
        NSLog(@"无法查询, code:%d", result);
    }
    
    sqlite3_finalize(stmt);
    return mArr;
    
}

- (void)dropTable {
    
    NSString *sql = @"Drop table ContentPlist";
    
    //  Core API
    int result = sqlite3_exec(db, sql.UTF8String, NULL, NULL, nil);
    
    if (result == SQLITE_OK) {
        
        NSLog(@"删除表格成功");
        
    } else {
        
        NSLog(@"删除表格失败");
        NSLog(@"%d", result);
        
    }
    
}

- (void)closeDB {
    
    //  Core API
    int result = sqlite3_close(db);
    
    if (result == SQLITE_OK) {
        
        db = nil;
        NSLog(@"关闭数据库成功");
        
    } else {
        
        NSLog(@"关闭数据库失败");
        NSLog(@"%d", result);
        
    }
    
}


@end
