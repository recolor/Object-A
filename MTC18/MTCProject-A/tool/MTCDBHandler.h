//
//  MTCDBHandler.h
//  MTCProject-A
//
//  Created by dllo on 16/7/4.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MTC163NewsChannelModel;

@interface MTCDBHandler : NSObject
/** 单例 */
+ (instancetype)sharedDBHandler;
/** 开启数据库. */
- (void)openDB;

/** 创建表格 */
- (void)createTable;

/** 插入数据 */
- (void)insertModel:(MTC163NewsChannelModel *)model;

/** 删除数据 */
- (void)deleteModeltWithTitle:(NSString *)title;

/** 查询 */
- (NSArray<MTC163NewsChannelModel *> *)searchForTitle:(NSString *)title;
/**获取数据*/
- (NSArray <MTC163NewsChannelModel *> *)getData;
/** 删除表格 */
- (void)dropTable;

/** 关闭数据库. */
- (void)closeDB;


@end
