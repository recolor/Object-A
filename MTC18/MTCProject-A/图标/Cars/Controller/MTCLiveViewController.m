//
//  MTCLiveViewController.m
//  MTCProject-A
//
//  Created by dllo on 16/6/23.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCLiveViewController.h"
#import "MTCLiveModel.h"
#import "MTCLiveCell.h"
//#import "MTCLiveCellOther.h"
#import "NetworkingHandler.h"
#import "UIImageView+WebCache.h"



@interface MTCLiveViewController ()

@property (nonatomic, retain) NSMutableArray *arrOfLive;
//@property (nonatomic, retain) NSMutableArray *mArr;

@end

@implementation MTCLiveViewController
- (void)dealloc{
    [super dealloc];
    [_arrOfLive release];
//    [_mArr release];


}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = COLOR;
    [self login];
    [self configData];
    
    
}
- (void)configData{

    self.arrOfLive = [NSMutableArray array];

    
    
    NSString *str = @"http://c.m.163.com/nc/live/list/5aSn6L%2Be/0-20.html";
    NetworkingHandler *nt = [[NetworkingHandler alloc] init];
    
    [nt netWorkingHandlerGETWithURL:str completion:^(id result, NSData *data, NSURLResponse *response, NSError *error) {
        
//        NSLog(@"%@", result);
        NSArray *arr = [result objectForKey:@"T1462958418713"];
        for (NSDictionary *dic in arr) {
          
            MTCLiveModel *model = [[MTCLiveModel alloc] init];
            
            [model setValuesForKeysWithDictionary:dic];
            
            [self.arrOfLive addObject:model];
            [model release];

        }
      
        [self.tableView reloadData];
        
    }];
    [nt release];


}
- (void)login{
    [self.tableView registerClass:[MTCLiveCell class] forCellReuseIdentifier:@"pool"];

  
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{

    return 1;

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.arrOfLive.count;


}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{


    
    
    MTCLiveCell *cell = [tableView dequeueReusableCellWithIdentifier:@"pool" forIndexPath:indexPath];
    cell.model = [self.arrOfLive objectAtIndex:indexPath.row];
    
    return cell;



}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 200;



}







@end
