//
//  MTCHeadNewsViewController.m
//  MTCProject-A
//
//  Created by dllo on 16/6/15.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCHeadNewsViewController.h"
#import "MTCPicOfModels.h"
#import "UIImageView+WebCache.h"
#import "MTCPicOfCell.h"
#import "MTCOnePicOfCell.h"
#import "NetworkingHandler.h"

@interface MTCHeadNewsViewController ()
@property (nonatomic, retain) NSMutableArray *arrayOfPic;
@property (nonatomic, retain) NSMutableArray *arrayOfAllPics;
@property (nonatomic, retain) NSMutableArray *mArrayOfPics;




@end

@implementation MTCHeadNewsViewController
//ARC用的 总忘释放内存
- (void)dealloc {
    [_arrayOfPic release];
    [_arrayOfAllPics release];
    [_mArrayOfPics release];
    [super dealloc];
 
}



- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.backgroundColor = [UIColor whiteColor];
    [self handleFData];
    [self login];
    
}
//看名知其意
- (void)handleFData {

    self.arrayOfPic = [NSMutableArray array];
    self.arrayOfAllPics = [NSMutableArray array];
    self.mArrayOfPics = [NSMutableArray array];
    NSString *strOfPic = @"http://c.m.163.com/photo/api/morelist/0096/4GJ60096/96425.json";
    
    NetworkingHandler *nt = [[NetworkingHandler alloc] init];
    
    [nt netWorkingHandlerGETWithURL:strOfPic completion:^(id result, NSData *data, NSURLResponse *response, NSError *error) {
        
        for (NSDictionary *dic in result) {
            MTCPicOfModels *model = [[MTCPicOfModels alloc] init];
            [model setValuesForKeysWithDictionary:dic];
            
            [self.arrayOfPic addObject:model];
            
            if ([dic objectForKey:@"desc"]) {
                self.mArrayOfPics = [dic objectForKey:@"pics"];
            }
            [self.arrayOfAllPics addObject:self.mArrayOfPics];
//            //总忘 !!!
            [model release];
            
        }//回调
        [self.tableView reloadData];
        
        
    }];
    [nt release];
    

}
//注册login
- (void)login{

    [self.tableView registerClass:[MTCPicOfCell class] forCellReuseIdentifier:@"MTCPicOfCell"];
    

    [self.tableView registerClass:[MTCOnePicOfCell class] forCellReuseIdentifier:@"MTCOnePicOfCell"];
    

}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
    
    

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.arrayOfPic.count;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    NSArray *arr = self.arrayOfAllPics[indexPath.row];

    if (arr.count == 0) {
        MTCOnePicOfCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"MTCOnePicOfCell" forIndexPath:indexPath];
        cell.model = [self.arrayOfPic objectAtIndex:indexPath.row];
        
        return cell;
    }else{
    
        MTCPicOfCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MTCPicOfCell" forIndexPath:indexPath];
        cell.model = [self.arrayOfPic objectAtIndex:indexPath.row];
        
        NSString *str0 = [self.arrayOfAllPics[indexPath.row] objectAtIndex: 0];
        [cell.imageView1 sd_setImageWithURL:[NSURL URLWithString:str0] placeholderImage:nil];
        
        NSString *str1 = [self.arrayOfAllPics[indexPath.row] objectAtIndex:1];
        
        [cell.imageView2 sd_setImageWithURL:[NSURL URLWithString:str1] placeholderImage:nil];
        
        NSString *str2 = [self.arrayOfAllPics[indexPath.row] objectAtIndex:2];
        
        [cell.imageView3 sd_setImageWithURL:[NSURL URLWithString:str2] placeholderImage:nil];
        
        return cell;
       
    
    
    }


}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 250;
    

}






















@end
