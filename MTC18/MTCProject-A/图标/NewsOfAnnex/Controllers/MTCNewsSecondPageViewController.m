//
//  MTCNewsSecondPageViewController.m
//  MTCProject-A
//
//  Created by dllo on 16/6/25.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCNewsSecondPageViewController.h"
#import "UMSocialData.h"
#import "UMSocialSnsService.h"
#import "UMSocial.h"
#import "MTCDBHandler.h"
#import "MTC163NewsChannelModel.h"
#import "MTC163NewsTVCOOfBasic.h"


@interface MTCNewsSecondPageViewController ()<UMSocialUIDelegate>

@end

@implementation MTCNewsSecondPageViewController
- (void)dealloc{
    [super dealloc];
    [_dic release];
    [_mol release];


}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self createWebView];
    [self creatShare];
}

- (void)createWebView {
    
    NSString *str = [NSString stringWithFormat:@"http://3g.163.com/ntes/special/0034073A/article_share.html?docid=%@&spst=0&spss=newsapp&spsf=qq&spsw=1",_replyid];
    
    NSURL *url = [NSURL URLWithString:str];
    
    UIWebView *web = [[UIWebView alloc] initWithFrame:self.view.bounds];
    [self.view addSubview:web];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [web loadRequest:request];
    web.scalesPageToFit = YES;
    [web release];
    
}
- (void)creatShare{
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Share.png"] style:UIBarButtonItemStylePlain target:self action:@selector(handleShare)];
    
    UIBarButtonItem *item1 = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Share.png"] style: UIBarButtonItemStylePlain target:self action:@selector(handleShare)];
    
    UIBarButtonItem *item2 = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"shoucang.png"] style:UIBarButtonItemStylePlain target:self action:@selector(handleShoucang)];
    
    self.navigationItem.rightBarButtonItems = @[item1, item2];
    
    
    
    
}
//分享
- (void)handleShare{
    [UMSocialData defaultData].extConfig.title = @"分享的title";
    [UMSocialData defaultData].extConfig.qqData.url = @"http://baidu.com";
    [UMSocialSnsService presentSnsIconSheetView:self
                                         appKey:@"57733dbfe0f55a903f001c99"
                                      shareText:@"天启，http://umeng.com/social"
                                     shareImage:[UIImage imageNamed:@"icon"]
                                shareToSnsNames:@[UMShareToWechatSession,UMShareToWechatTimeline,UMShareToSina,UMShareToQQ,UMShareToQzone]
                                       delegate:self];
    
}
//收藏
- (void)handleShoucang{
    
   
    self.mol = [[MTC163NewsChannelModel alloc] init];
    [_mol setValuesForKeysWithDictionary:self.dic];
    
    

    [[MTCDBHandler sharedDBHandler] openDB];
    [[MTCDBHandler sharedDBHandler] createTable];
    [[MTCDBHandler sharedDBHandler] insertModel:_mol];
//    NSLog(@"%@", self.dic);
    




}
-(void)didFinishGetUMSocialDataInViewController:(UMSocialResponseEntity *)response
{
    //根据`responseCode`得到发送结果,如果分享成功
    if(response.responseCode == UMSResponseCodeSuccess)
    {
        //得到分享到的平台名
        NSLog(@"share to sns name is %@",[[response.data allKeys] objectAtIndex:0]);
    }
}




@end
