//
//  MTC163NewsNormalCell.h
//  MTCProject-A
//
//  Created by dllo on 16/6/15.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCBasicNewsTVC.h"

@class MTC163NewsChannelModel;

@interface MTC163NewsNormalCell : MTCBasicNewsTVC

@property (nonatomic, retain) MTC163NewsChannelModel *model;
//标题
@property (nonatomic, retain) UILabel *title;
//新闻热点
@property (nonatomic, retain) UILabel *source;
//跟帖
@property (nonatomic, retain) UILabel *count;
//小图
@property (nonatomic, retain) UIImageView *img;



@end
