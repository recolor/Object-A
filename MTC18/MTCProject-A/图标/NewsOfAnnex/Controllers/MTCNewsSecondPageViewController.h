//
//  MTCNewsSecondPageViewController.h
//  MTCProject-A
//
//  Created by dllo on 16/6/25.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MTC163NewsChannelModel;

@interface MTCNewsSecondPageViewController : UIViewController

@property(nonatomic, assign) NSNumber *replyid;
@property (nonatomic, retain) NSMutableDictionary *dic;
@property (nonatomic, retain) MTC163NewsChannelModel *mol;
@end
