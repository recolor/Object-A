//
//  MTC163NewsTVCOOfBasic.h
//  MTCProject-A
//
//  Created by dllo on 16/6/15.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCBasicNewsTVCO.h"
@class MTC163NewsChannelModel;

@interface MTC163NewsTVCOOfBasic : MTCBasicNewsTVCO

@property (nonatomic, retain) MTC163NewsChannelModel *model1;


@end
