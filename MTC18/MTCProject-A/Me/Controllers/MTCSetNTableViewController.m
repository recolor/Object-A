//
//  MTCSetNTableViewController.m
//  MTCProject-A
//
//  Created by dllo on 16/6/30.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCSetNTableViewController.h"
#import "MTCSetSwitch.h"
@interface MTCSetNTableViewController ()

@end

@implementation MTCSetNTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    [self.tableView registerClass:[MTCSetSwitch class] forCellReuseIdentifier:@"pool"];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.view.backgroundColor = COLOR;



}
- (void)viewWillAppear:(BOOL)animated {
    
    self.navigationController.navigationBarHidden = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor redColor];
    self.navigationItem.title= @"设置";
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}





- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 14;
    
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 3) {
        
        MTCSetSwitch *cell1 = [tableView dequeueReusableCellWithIdentifier:@"pool" forIndexPath:indexPath];
        [[NSNotificationCenter defaultCenter]addObserverForName:@"changeBackgroundColor" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
            
            cell1.backgroundColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1];
        }];
        
        [[NSNotificationCenter defaultCenter]addObserverForName:@"changeBackgroundColor1" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
            
            cell1.backgroundColor = [UIColor whiteColor];
        }];
        
        return cell1;
    }else{
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
        
        
        NSArray *arr1 = @[@"个人设置", @"字体设置", @"正文字号",@"自动设置夜间模式", @"推送设置", @"栏目插件设置", @"离线设置", @"智能头条",@"仅Wi-Fi网络下载图片", @"清理缓存",@"为网易新闻评分",@"态度封面",@"关于"];
        if (indexPath.row < 3) {
            
            cell.textLabel.text = arr1[indexPath.row];
        }else if(indexPath.row > 3) {
            cell.textLabel.text = arr1[indexPath.row - 1];
        }
        [[NSNotificationCenter defaultCenter]addObserverForName:@"changeBackgroundColor" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
            
            cell.backgroundColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1];
        }];
        
        [[NSNotificationCenter defaultCenter]addObserverForName:@"changeBackgroundColor1" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
            
            cell.backgroundColor = [UIColor whiteColor];
        }];
        
        
        return cell;
    }







}






@end
