//
//  MTCShouCangViewController.m
//  MTCProject-A
//
//  Created by dllo on 16/7/4.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCShouCangViewController.h"
#import "MTCDBHandler.h"
#import "MTC163NewsChannelModel.h"
#import "UIImageView+WebCache.h"
#import "MTC163NewsNormalCell.h"
#import "SearchSecondViewController.h"
@interface MTCShouCangViewController ()
@property (nonatomic, retain) NSMutableArray *mArr;

@end

@implementation MTCShouCangViewController
- (void)dealloc{
    [_mArr release];
    [super dealloc];


}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self createDB];
    
    
    
    self.title = @"我的收藏";
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [self.tableView registerClass:[MTC163NewsNormalCell class] forCellReuseIdentifier:@"pool"];
    
    
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}
- (void)createDB{

    [[MTCDBHandler sharedDBHandler] openDB];
    [[MTCDBHandler sharedDBHandler] createTable];
    self.mArr = [[MTCDBHandler sharedDBHandler] getData].mutableCopy;
    

}
//编辑
- (void)setEditing:(BOOL)editing animated:(BOOL)animated{

    [super setEditing:editing animated:animated];
    [self.tableView setEditing:editing animated:animated];




}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (editingStyle) {
        case UITableViewCellEditingStyleDelete:{
            
            MTC163NewsChannelModel *model = [self.mArr objectAtIndex:indexPath.row];
            [[MTCDBHandler sharedDBHandler] deleteModeltWithTitle:model.title];
            [self.mArr removeObjectAtIndex:indexPath.row];
            [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationTop];
            
            break;
        }
        default:
            break;
    }
    
}



- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return YES;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.mArr.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MTC163NewsNormalCell *cell = [tableView dequeueReusableCellWithIdentifier:@"pool"];
    
    
    MTC163NewsChannelModel *model = self.mArr[indexPath.row];
    
    
    [cell.img sd_setImageWithURL:[NSURL URLWithString:model.imgsrc] placeholderImage:nil];
    
    cell.title.text = model.title;
    
    cell.source.text = model.recSource;
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 100;
}


























@end
