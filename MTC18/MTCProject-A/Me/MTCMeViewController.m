//
//  MTCMeViewController.m
//  MTCProject-A
//
//  Created by dllo on 16/6/14.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCMeViewController.h"
#import "Masonry.h"
#import "MTCMeTVCO.h"
#import "MTCMeRegisterViewController.h"
#import "MTCSetNTableViewController.h"
#import "MTCShouCangViewController.h"

@interface MTCMeViewController ()
@property (nonatomic, retain) UIButton *registerButton;

@end

@implementation MTCMeViewController

-(void)dealloc{
    [super dealloc];
    [_registerButton release];

}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{

    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self config];
    }
    return self;

}
//隐藏navigationbar
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self creadView];
    
    
    
}

- (void)config {
    self.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"我" image:[UIImage imageNamed:@"me.png"]  tag:500];

    


}
//控件太多!!!!!!!!!!!! 累
- (void)creadView {

    UIView *viewOfHead = [[UIView alloc] init];
    viewOfHead.backgroundColor = [UIColor redColor];

    
    [self.view addSubview:viewOfHead];
    
    [viewOfHead mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).with.offset(0);
        make.right.equalTo(self.view).with.offset(0);
        make.left.equalTo(self.view).with.offset(0);
        make.height.offset(220);
        
        
    }];
    [viewOfHead release];

    UIButton *setButton = [UIButton buttonWithType:UIButtonTypeCustom];

    [setButton setImage:[UIImage imageNamed:@"set.png"] forState:UIControlStateNormal];
    
    [setButton addTarget:self action:@selector(handleSet) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:setButton];
    [setButton release];
    [setButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).with.offset(40);
        make.right.equalTo(self.view).with.offset(-20);
        make.height.offset(20);
        make.width.offset(20);
        
    }];
    
    UIImageView *headImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"head1.png"]];
    [self.view addSubview:headImage];
    [headImage release];
    [headImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(viewOfHead);
        make.top.equalTo(viewOfHead).with.offset(20);
        make.size.mas_equalTo(CGSizeMake(80, 80));
        
    }];
    
    self.registerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.registerButton setTitle:@"立即登录" forState:UIControlStateNormal];
    [self.view addSubview:self.registerButton];
    
    [self.registerButton addTarget:self action:@selector(handleButton1) forControlEvents:UIControlEventTouchUpInside];
    [self.registerButton release];
    
    
    [self.registerButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(headImage.mas_bottom).with.offset(10);
        make.centerX.equalTo(viewOfHead);
        make.height.offset(20);
        
    }];
    UILabel *laWin = [[UILabel alloc] init];
    laWin.text = @"赢积分 抢大礼!";
    laWin.textColor = [UIColor colorWithRed:255 /255.0 green:215 / 255.0 blue:88 / 255.0 alpha:1];
    [self.view addSubview:laWin];
    [laWin release];
    
    [laWin mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.registerButton.mas_bottom).with.offset(10);
        make.centerX.equalTo(viewOfHead);
        make.height.offset(20);
        
    }];
    NSArray *arr = @[ @"readme2.png", @"heartme1.png", @"writeme1.png", @"goldme1.png"];
    for (int i = 0; i < 4; i++) {
        UIButton *buttonfour = [[UIButton alloc] init];
        buttonfour.frame = CGRectMake(SCREEN_WIDTH / 4 * i, 220, SCREEN_WIDTH / 4, 80);
        [self.view addSubview:buttonfour];
        [buttonfour release];
        buttonfour.tag = i;
        buttonfour.backgroundColor = COLOR;
        [buttonfour setImage:[UIImage imageNamed:arr[i]] forState:UIControlStateNormal];
        
        if (buttonfour.tag == 1) {
            [buttonfour addTarget:self action:@selector(handleShouCang) forControlEvents:UIControlEventTouchUpInside];
            
        }
        
    }
    
    
    
    
    
    MTCMeTVCO *tableView = [[MTCMeTVCO alloc] init];
    
    [self.view addSubview:tableView.view];
    tableView.view.frame = CGRectMake(0, 300, SCREEN_WIDTH, SCREEN_HEIGHT- 300 - 49);

    
    


}
//登录
- (void)handleButton1{
    
    MTCMeRegisterViewController *registerMe = [[MTCMeRegisterViewController alloc] init];
    
    [self.navigationController pushViewController:registerMe animated:YES];
    
    
}
//设置
- (void)handleSet{

    MTCSetNTableViewController *set = [[MTCSetNTableViewController alloc] init];
    [self.navigationController pushViewController:set animated:YES];

}
//收藏
- (void)handleShouCang{

    MTCShouCangViewController *shouCang = [[MTCShouCangViewController alloc] init];
    [self.navigationController pushViewController:shouCang animated:YES];
    



}




@end
