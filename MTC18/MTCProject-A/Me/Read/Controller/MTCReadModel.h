//
//  MTCReadModel.h
//  MTCProject-A
//
//  Created by dllo on 16/6/22.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MTCReadModel : NSObject
//一个图时候
@property (nonatomic, copy) NSString *img;
//三个图中的两个
@property (nonatomic, copy) NSString *imgsrc;
//标题
@property (nonatomic, copy) NSString *title;
//来源
@property (nonatomic, copy) NSString *source;
//图的数组
@property (nonatomic, retain) NSMutableArray *imgnewextra;



@end
