//
//  MTCReadOnePicCell.h
//  MTCProject-A
//
//  Created by dllo on 16/6/22.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCBasicNewsTVC.h"

@class MTCReadModel;

@interface MTCReadOnePicCell : MTCBasicNewsTVC

@property (nonatomic, retain) MTCReadModel *model;

@end
