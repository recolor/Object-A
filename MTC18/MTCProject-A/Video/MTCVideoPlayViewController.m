//
//  MTCVideoPlayViewController.m
//  MTCProject-A
//
//  Created by dllo on 16/6/21.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCVideoPlayViewController.h"
#import "UMSocialData.h"
#import "UMSocialSnsService.h"
#import "UMSocial.h"
@interface MTCVideoPlayViewController ()<UMSocialUIDelegate>

@end

@implementation MTCVideoPlayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self creatShare];
}
- (void)creatShare{
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Share.png"] style:UIBarButtonItemStylePlain target:self action:@selector(handleShare)];
    
    


}

- (void)handleShare{
    [UMSocialData defaultData].extConfig.title = @"分享的title";
    [UMSocialData defaultData].extConfig.qqData.url = @"http://baidu.com";
    [UMSocialSnsService presentSnsIconSheetView:self
                                         appKey:@"57733dbfe0f55a903f001c99"
                                      shareText:@"X战警逆转未来，http://umeng.com/social"
                                     shareImage:[UIImage imageNamed:@"icon"]
                                shareToSnsNames:@[UMShareToWechatSession,UMShareToWechatTimeline,UMShareToSina,UMShareToQQ,UMShareToQzone]
                                       delegate:self];
    


}

@end
