//
//  MTCVideoCollectionViewCell.m
//  MTCProject-A
//
//  Created by dllo on 16/6/20.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCVideoCollectionViewCell.h"
#import "MTCVideoOfModel.h"
#import "Masonry.h"
#import "UIImageView+WebCache.h"


@interface MTCVideoCollectionViewCell()

@property (nonatomic, retain) UIImageView *topicImg;
@property (nonatomic, retain) UILabel *tname;
@property (nonatomic, retain) UILabel *title;
@property (nonatomic, retain) UIImageView *cover;
@property (nonatomic, retain) NSString *mp4url;
@property (nonatomic, retain) UIButton *picture;

@end

@implementation MTCVideoCollectionViewCell
- (void)dealloc{

    [super dealloc];
    [_model release];
    [_cover release];
    [_topicImg release];
    [_tname release];
    [_title release];
    [_mp4url release];
    [_picture release];




}


- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self config];
    }
    return self;

}
- (void)config {
    self.cover = [[UIImageView alloc] init];
    self.topicImg = [[UIImageView alloc] init];
    self.picture = [UIButton buttonWithType:UIButtonTypeSystem];
    [self.picture setImage:[UIImage imageNamed:@"play.png"] forState:UIControlStateNormal];
    
    self.tname = [[UILabel alloc] init];
    self.title = [[UILabel alloc] init];
    //多行显示
    [self.title setNumberOfLines:2];
    
    //字体
    self.tname.font = [UIFont systemFontOfSize:12];
    [self.cover addSubview:self.picture];
    [self.contentView addSubview:self.cover];
    [self.contentView addSubview:self.topicImg];
    [self.contentView addSubview:self.tname];
    [self.contentView addSubview:self.title];
    
    [self.cover mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView);
        make.left.equalTo(self.contentView);
        make.right.equalTo(self.contentView);
        make.height.equalTo(self.cover.mas_width).multipliedBy(3.0 / 4.0f);
        
        
    }];
    [self.picture mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.cover.mas_centerX);
        make.centerY.equalTo(self.cover.mas_centerY);
        
    }];
    self.picture.tintColor = [UIColor grayColor];

    [self.title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.cover.mas_bottom).with.offset(10);
        make.left.equalTo(self.contentView).with.offset(10);
        make.right.equalTo(self.contentView).with.offset(-10);
        make.bottom.equalTo(self.contentView).with.offset(-40);
        
    }];
    [self.topicImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.title.mas_bottom).with.offset(10);
        make.left.equalTo(self.contentView).with.offset(10);
        make.bottom.equalTo(self.contentView).with.offset(-10);
        make.width.offset(20);
        
    }];
    [self.tname mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.title.mas_bottom).with.offset(10);
        make.bottom.equalTo(self.contentView).with.offset(-10);
        make.left.equalTo(self.topicImg.mas_right).with.offset(10);
        make.width.offset(80);
        
    }];
    
    [_cover release];
    [_title release];
    [_tname release];
    [_topicImg release];
    [_picture release];
    


}
- (void)setModel:(MTCVideoOfModel *)model{
    if (_model != model) {
        [_model release];
        [_model retain];
    }
    
    
    
    self.title.text = model.title;
    self.tname.text = model.topicName;
    self.mp4url = model.mp4_url;
    
    
    [self.topicImg sd_setImageWithURL:[NSURL URLWithString:model.topicImg]];
    
    [self.cover sd_setImageWithURL:[NSURL URLWithString:model.cover]];





}







@end
