//
//  MTCVideoViewController.m
//  MTCProject-A
//
//  Created by dllo on 16/6/16.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCVideoViewController.h"
#import "MTCVideoOfModel.h"
#import "MTCVideoCollectionViewCell.h"
#import "NetworkingHandler.h"
#import "UIImageView+WebCache.h"
#import "MTCVideoPlayViewController.h"
@import WebKit;



@interface MTCVideoViewController ()<UICollectionViewDelegate, UICollectionViewDataSource>
@property (nonatomic, retain) UICollectionView *collectionView;
@property (nonatomic, retain) NSMutableArray *arrOfVideo;
@property (nonatomic, retain) WKWebView *webView;

@end

@implementation MTCVideoViewController
- (void)dealloc{
    [super dealloc];
    [_arrOfVideo release];
    [_collectionView release];
    [_webView release];



}


- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self= [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self config];
    }
    return self;

}




- (void)viewDidLoad {
    [super viewDidLoad];
    [self configLayout];
    [self configData];
    
}
- (void)config{
    self.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"视频" image:[UIImage imageNamed:@"play.png"] tag:400];
    self.view.backgroundColor = [UIColor whiteColor];
    
    
}

- (void)configLayout{
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.minimumLineSpacing = 10;
    layout.minimumInteritemSpacing = 10;
    layout.itemSize = CGSizeMake((SCREEN_WIDTH - 30) / 2, 250);
    //直接布局 上下左右
    layout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
    
    self.collectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:layout];
    [self.view addSubview:self.collectionView];
//
    [self.collectionView layoutIfNeeded];
//    self.automaticallyAdjustsScrollViewInsets = NO;
    self.collectionView.pagingEnabled = NO;
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    [self.collectionView registerClass:[MTCVideoCollectionViewCell class] forCellWithReuseIdentifier:@"pool"];
    self.collectionView.showsHorizontalScrollIndicator = NO;
    
//    self.collectionView.bounces = NO;
    self.collectionView.backgroundColor = [UIColor whiteColor];

    
    [_collectionView release];
    [layout release];
    
   
}
- (void)configData{

    self.arrOfVideo = [NSMutableArray array];
    NSString *strOfVideo = @"http://c.3g.163.com/recommend/getChanListNews?channel=T1457068979049&passport=FFh5WxU3p1EarBb67XYM4exu5Qgz9IwfxBMkTUgNz7o%3D&devId=PpbPMaO8nB1Bu3QwS33UWut4MJyEQ2%2FGVwsDoLfmO%2BdkAb1Kazpd5H3ScQnpE%2FUr&size=10&version=10.0&spever=false&net=wifi&lat=&lon=&ts=1465793361&sign=NmF5eKoIqdJRRmkZyoO9byMSKkzi87yTePQoGD0Guz548ErR02zJ6%2FKXOnxX046I&encryption=1&canal=appstore";
    
    NetworkingHandler *nt = [[NetworkingHandler alloc] init];
    [nt netWorkingHandlerGETWithURL:strOfVideo completion:^(id result, NSData *data, NSURLResponse *response, NSError *error) {
    
    
        NSArray *array = [result objectForKey:@"视频"];
        for (NSDictionary *dic in array) {
//            NSLog(@"%@", result);
            
                MTCVideoOfModel *model = [[MTCVideoOfModel alloc] init];
                [model setValuesForKeysWithDictionary:dic];
                
                [self.arrOfVideo addObject:model];
                [model release];
                
        }
        [self.collectionView reloadData];
        
    }];
    [nt release];
    
 
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{

    return 1;

}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{


    return self.arrOfVideo.count;


}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{

    MTCVideoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"pool" forIndexPath:indexPath];
    cell.backgroundColor = [UIColor whiteColor];
    
    cell.model = [self.arrOfVideo objectAtIndex:indexPath.row];
    
    
    
    return cell;



}
//播放视频
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    MTCVideoOfModel *model = self.arrOfVideo[indexPath.row];
    MTCVideoPlayViewController *play = [[MTCVideoPlayViewController alloc] init];
    
    _webView = [[WKWebView alloc] initWithFrame:play.view.bounds];
    [play.view addSubview:_webView];
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:model.mp4_url]]];
    [self.navigationController pushViewController:play animated:NO];
    self.navigationController.navigationBarHidden = NO;
    [play release];
    [_webView release];


}











@end
