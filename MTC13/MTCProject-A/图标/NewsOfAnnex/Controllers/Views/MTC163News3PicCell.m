//
//  MTC163News3PicCell.m
//  MTCProject-A
//
//  Created by dllo on 16/6/15.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTC163News3PicCell.h"
#import "Masonry.h"
#import "UIImageView+WebCache.h"
#import "MTC163NewsChannelModel.h"

@interface MTC163News3PicCell()

@property (nonatomic, retain) UIImageView *imageView1;
@property (nonatomic, retain) UIImageView *imageView2;
@property (nonatomic, retain) UIImageView *imageView3;
//标题
@property (nonatomic, retain) UILabel *title;
//新闻热点
@property (nonatomic, retain) UILabel *source;
//跟帖
@property (nonatomic, retain) UILabel *count;
@end


@implementation MTC163News3PicCell
- (void)dealloc{
    [_model release];
    [_imageView1 release];
    [_imageView2 release];
    [_imageView3 release];
    [_title release];
    [_source release];
    [_count release];
    [super dealloc];
    

}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self configView];
    }
    return self;

}
- (void)configView{
    self.imageView1 = [[UIImageView alloc]init];
    self.imageView2 = [[UIImageView alloc]init];
    self.imageView3 = [[UIImageView alloc]init];
    self.title = [[UILabel alloc]init];
    self.source = [[UILabel alloc]init];
    self.count = [[UILabel alloc]init];
    [self.contentView addSubview:self.imageView1];
    [self.contentView addSubview:self.imageView2];
    [self.contentView addSubview:self.imageView3];
    //选择规模内容来填充视图的大小。一部分的内容可以省略视图的范围。
    self.imageView1.contentMode = UIViewContentModeScaleAspectFill;
    self.imageView2.contentMode = UIViewContentModeScaleAspectFill;
    self.imageView3.contentMode = UIViewContentModeScaleAspectFill;
    self.imageView1.layer.masksToBounds = YES;
    self.imageView2.layer.masksToBounds = YES;
    self.imageView3.layer.masksToBounds = YES;
    
    
    [self.contentView addSubview:self.title];
//    [self.contentView addSubview:self.source];
//    [self.contentView addSubview:self.count];
    self.source.font = [UIFont systemFontOfSize:12];
    self.count.font = [UIFont systemFontOfSize:12];
    
    [self.title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).with.offset(10);
        make.left.equalTo(self.contentView.mas_left).with.offset(10);
        make.right.equalTo(self.contentView.mas_right).with.offset(-10);
        make.height.equalTo(@20);
        
    }];
    
    [self.imageView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.title.mas_bottom).with.offset(10);
        make.left.equalTo(self.contentView.mas_left).with.offset(10);
        make.bottom.equalTo(self.contentView.mas_bottom).with.offset(-10);
        make.width.offset((SCREEN_WIDTH - 40) / 3);
    }];
    
    [self.imageView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.imageView1.mas_right).with.offset(10);
        make.width.offset((SCREEN_WIDTH - 40) / 3);
        make.bottom.equalTo(self.contentView.mas_bottom).with.offset(-10);
        make.top.equalTo(self.title.mas_bottom).with.offset(10);
        
    }];
    
    [self.imageView3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.contentView.mas_bottom).with.offset(-10);
        make.right.equalTo(self.contentView.mas_right).with.offset(-10);
        make.top.equalTo(self.title.mas_bottom).with.offset(10);
        make.left.equalTo(self.imageView2.mas_right).with.offset(10);
        
    }];
    
//    [self.source mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self.contentView.mas_left).with.offset(10);
//        make.bottom.equalTo(self.contentView.mas_bottom).with.offset(-10);
//        make.top.equalTo(self.imageView1.mas_bottom).with.offset(10);
//        make.height.equalTo(@20);
//    }];
//    
//    [self.count mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.equalTo(self.contentView.mas_right).with.offset(-10);
//        make.bottom.equalTo(self.contentView.mas_bottom).with.offset(-10);
//        make.top.equalTo(self.imageView1.mas_bottom).with.offset(10);
//        make.height.equalTo(@20);
//        
//    }];
    
    [_imageView1 release];
    [_imageView2 release];
    [_imageView3 release];
    [_title release];
//    [_source release];
//    [_count release];
    


}
- (void)setModel:(MTC163NewsChannelModel *)model{
    if (_model != model) {
        [_model release];
        [_model retain];
    }
    
    
    [self.imageView1 sd_setImageWithURL:[NSURL URLWithString:model.img] placeholderImage:nil];
    
    self.title.text = model.title;
    if (![model.recSource isEqualToString:@"#"]) {
        self.source.text = model.recSource;
    }

    self.count.text = [model.replyCount.description stringByAppendingString:@"跟帖"];
    
    self.count.textAlignment = NSTextAlignmentRight;
    
    NSDictionary *model1 = [model.imgextra objectAtIndex:0];
    
    NSString *model1Value = [model1 objectForKey:@"imgsrc"];
    [self.imageView1 sd_setImageWithURL:[NSURL URLWithString:model1Value] placeholderImage:nil];
    NSDictionary *model2 = [model.imgextra objectAtIndex:1];
    NSString *model2Value = [model2 objectForKey:@"imgsrc"];
    
    [self.imageView2 sd_setImageWithURL:[NSURL URLWithString:model2Value] placeholderImage:nil];
    
    [self.imageView3 sd_setImageWithURL:[NSURL URLWithString:model.imgsrc] placeholderImage:nil];
    
    
 
}







@end
