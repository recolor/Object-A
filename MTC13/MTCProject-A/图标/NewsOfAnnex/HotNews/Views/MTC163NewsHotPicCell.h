//
//  MTC163NewsHotPicCell.h
//  MTCProject-A
//
//  Created by dllo on 16/6/16.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCBasicNewsTVC.h"
@class MTCModelOfHot;

@interface MTC163NewsHotPicCell : MTCBasicNewsTVC

@property (nonatomic, retain) MTCModelOfHot *model;

@end
