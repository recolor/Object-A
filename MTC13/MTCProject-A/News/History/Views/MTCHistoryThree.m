//
//  MTCHistoryThree.m
//  MTCProject-A
//
//  Created by dllo on 16/6/28.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCHistoryThree.h"
#import "Masonry.h"
#import "UIImageView+WebCache.h"
#import "MTCHistoryModel.h"

@interface MTCHistoryThree()

@property (nonatomic, retain)UILabel *title;
@property (nonatomic, retain)UILabel *source;
@property (nonatomic, retain)UIImageView *img;
@property (nonatomic, retain)UILabel *replycount;

@property(nonatomic, retain)UILabel *digest;


@end

@implementation MTCHistoryThree
-(void)dealloc{
    [_replycount release];
    [_img release];
    [_source release];
    [_title release];

    [_model release];
    [_digest release];
    [super dealloc];
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        
        [self config];
        
    }
    
    
    return self;
    
}
- (void)config{
    self.img = [[UIImageView alloc]init];
    [self.contentView addSubview:self.img];
   
    [self.img mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).with.offset(10);
        make.top.equalTo(self.contentView.mas_top).with.offset(40);
        make.bottom.equalTo(self.contentView.mas_bottom).with.offset(-40);
        make.right.equalTo(self.contentView.mas_right).with.offset(-10);
    }];
    
    self.digest = [[UILabel alloc]init];

    [self.img addSubview:self.digest];
    [self.digest mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.img.mas_left).with.offset(100);
        make.right.equalTo(self.img.mas_right).with.offset(-10);
        make.bottom.equalTo(self.img.mas_bottom).with.offset(-30);
        make.height.offset(15);
        
        
    }];
    
    
    
    
    
    self.title = [[UILabel alloc]init];
    [self.contentView addSubview:self.title];
    self.title.numberOfLines = 0;

    self.title.font = [UIFont systemFontOfSize:15];
    [self.title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).with.offset(10);
        make.top.equalTo(self.contentView.mas_top).with.offset(10);
        make.right.equalTo(self.contentView.mas_right).with.offset(-10);
        make.height.offset(20);
    }];
    
    self.source = [[UILabel alloc]init];
    
    [self.contentView addSubview:self.source];
    self.source.font = [UIFont systemFontOfSize:11];
    
    [self.source mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.img.mas_bottom).with.offset(10);
        make.left.equalTo(self.contentView.mas_left).with.offset(10);
        make.width.offset(150);
        make.bottom.equalTo(self.contentView.mas_bottom).with.offset(-15);
    }];
    
    
    self.replycount = [[UILabel alloc]init];
    self.replycount.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:self.replycount];
    self.replycount.font = [UIFont fontWithName:@"Arial" size:10];
 
    [self.replycount mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.img.mas_bottom).with.offset(10);
        make.right.equalTo(self.contentView).with.offset(-10);
        make.width.offset(30);
        make.height.offset(10);
    }];
    [self.img release];
    [self.title release];
    [self.source release];
    [self.replycount release];

    [self.digest release];
    
    
    
    
    
}
- (void)setModel:(MTCHistoryModel *)model{
    if (_model != model) {
        _model = [model retain];
        [_model release];
    }
    
    [self.img sd_setImageWithURL:[NSURL URLWithString:model.imgsrc]placeholderImage:nil];
    self.digest.text = model.digest;
    
    self.title.text = model.title;
    
    self.source.text = model.source;
    
    self.replycount.text = [model.replyCount.description stringByAppendingString:@"跟帖"];


}

@end
