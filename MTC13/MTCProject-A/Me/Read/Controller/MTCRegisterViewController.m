//
//  MTCRegisterViewController.m
//  MTCProject-A
//
//  Created by dllo on 16/6/22.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCRegisterViewController.h"

#import "Masonry.h"
#import "UMSocialSnsPlatformManager.h"
#import "UMSocialAccountManager.h"
@interface MTCRegisterViewController ()

@end

@implementation MTCRegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self creatView];
    self.view.backgroundColor = COLOR;
}
- (void)creatView{
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStylePlain target:self action:@selector(handleBack)];
    
    
    
//view1 最上面
    UIView *viewOfRegister = [[UIView alloc] init];
    [self.view addSubview:viewOfRegister];
    viewOfRegister.backgroundColor = [UIColor whiteColor];
    
    viewOfRegister.layer.cornerRadius = 4;
    viewOfRegister.layer.masksToBounds = YES;
  
    [viewOfRegister mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).with.offset(80);
        make.left.equalTo(self.view).with.offset(20);
        make.right.equalTo(self.view).with.offset(-20);
        make.height.offset(130);
        
    }];
    
//image1 头像
    UIImageView *imageOfAccount = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"me.png"]];
    [viewOfRegister addSubview:imageOfAccount];
    
    [imageOfAccount mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).with.offset(100);
        make.left.equalTo(self.view).with.offset(40);
        make.height.offset(20);
        make.width.offset(20);
        
        
    }];
//image2 锁
    UIImageView *imageOfSuo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"suo.png"]];
    [viewOfRegister addSubview:imageOfSuo];
    
    [imageOfSuo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).with.offset(40);
        make.top.equalTo(imageOfAccount.mas_bottom).with.offset(50);
        make.height.offset(20);
        make.width.offset(20);
        
    }];
//label 宽度为1
    UILabel *labelOfOneH = [[UILabel alloc] init];
    labelOfOneH.backgroundColor = COLOR;
    [viewOfRegister addSubview:labelOfOneH];
    
    [labelOfOneH mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(viewOfRegister).with.offset(64);
        make.left.equalTo(self.view).with.offset(70);
        make.right.equalTo(self.view).with.offset(-70);
        make.height.offset(1);
        
        
    }];
//登录 textfield
    UITextField *textOfRegister = [[ UITextField alloc] init];
    [viewOfRegister addSubview:textOfRegister];
    textOfRegister.text = @"网易邮箱/手机号";
    textOfRegister.textColor = [UIColor lightGrayColor];
    
    [textOfRegister mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(viewOfRegister).with.offset(5);
        make.left.equalTo(self.view).with.offset(70);
        make.right.equalTo(self.view).with.offset(-70);
        make.height.offset(50);
        
    }];
//密码textfiled
    UITextField *textOfSuo = [[ UITextField alloc] init];
    [viewOfRegister addSubview:textOfSuo];
    textOfSuo.text = @"密码";
    textOfSuo.textColor = [UIColor lightGrayColor];
    
    [textOfSuo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(imageOfAccount.mas_bottom).with.offset(36);
        make.left.equalTo(self.view).with.offset(70);
        make.right.equalTo(self.view).with.offset(-70);
        make.height.offset(50);
        
    }];
//登录 button
    UIButton *buttonRegister = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.view addSubview:buttonRegister];
    [buttonRegister setTitle:@"登录" forState:UIControlStateNormal];
    [buttonRegister setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    buttonRegister.layer.cornerRadius = 4;
    buttonRegister.layer.masksToBounds = YES;
    
    
    [buttonRegister mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(viewOfRegister.mas_bottom).with.offset(10);
        make.left.equalTo(self.view).with.offset(20);
        make.right.equalTo(self.view).with.offset(-20);
        make.height.offset(40);
        
    }];
    buttonRegister.backgroundColor = [UIColor whiteColor];
//label 还可以....
    UILabel *labelOfOther = [[UILabel alloc] init];
    labelOfOther.text = @"还可以选择以下的方式登录";
    labelOfOther.textColor = [UIColor lightGrayColor];
    labelOfOther.font = [UIFont systemFontOfSize:15];
    [self.view addSubview:labelOfOther];
    [labelOfOther mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(buttonRegister).with.offset(80);
        make.left.equalTo(self.view).with.offset(20);
        make.height.offset(20);
        make.width.offset(230);
        
    }];
    
//view 下面的
    UIView *viewOfWay = [[UIView alloc] init];
    [self.view addSubview:viewOfWay];
    viewOfWay.backgroundColor = [UIColor whiteColor];
    viewOfWay.layer.cornerRadius = 5;
    viewOfWay.layer.masksToBounds = YES;
    
    [viewOfWay mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(labelOfOther.mas_bottom).with.offset(10);
        make.left.equalTo(self.view).with.offset(20);
        make.right.equalTo(self.view).with.offset(-20);
        make.height.offset(180);
        
    }];
//新浪
    UIImageView *imageXinLang = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"xinlang.png"]];
    [viewOfWay addSubview:imageXinLang];
    
    [imageXinLang mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(viewOfWay.mas_centerX);
        make.top.equalTo(viewOfWay).with.offset(20);
        make.height.offset(64);
        
    }];
    
    
    
    
    
//微信
    UIImageView *imageWeiXin = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"weixin.png"]];
    [viewOfWay addSubview:imageWeiXin];
    
    [imageWeiXin mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(viewOfWay.mas_top).with.offset(20);
        make.left.equalTo(viewOfWay.mas_left).with.offset(30);
//        make.width.offset(64);
//        make.height.offset(64);
        
    }];
//QQ
    UIImageView *imageOfQQ = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"qq.png"]];
    [viewOfWay addSubview:imageOfQQ];
    
    [imageOfQQ mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(viewOfWay.mas_top).with.offset(20);
        make.right.equalTo(viewOfWay).with.offset(-30);
//        make.width.offset(64);
//        make.height.offset(64);

     }];
//微信登录
    UIButton *buttonWeiXin = [[UIButton alloc] init];
    [viewOfWay addSubview:buttonWeiXin];
    [buttonWeiXin setTitle:@"微信账号登录" forState:UIControlStateNormal];
    [buttonWeiXin setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    buttonWeiXin.titleLabel.font = [UIFont systemFontOfSize:15];
    
    [buttonWeiXin addTarget:self action:@selector(handleWeiXin) forControlEvents:UIControlEventTouchUpInside];
    
    
    [buttonWeiXin mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(imageWeiXin.mas_bottom).with.offset(10);
        make.centerX.equalTo(imageWeiXin.mas_centerX);
        make.height.offset(20);
        
    }];
//新浪登录
    UIButton *buttonXinLang = [[UIButton alloc] init];
    [viewOfWay addSubview:buttonXinLang];
    [buttonXinLang setTitle:@"新浪微博登录" forState:UIControlStateNormal];
    [buttonXinLang setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    buttonXinLang.titleLabel.font = [UIFont systemFontOfSize:15];
    
    [buttonXinLang addTarget:self action:@selector(handleXinLang) forControlEvents:UIControlEventTouchUpInside];
    
    
    [buttonXinLang mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(imageXinLang.mas_bottom).with.offset(10);
        make.centerX.equalTo(imageXinLang.mas_centerX);
        make.height.offset(20);
        
    }];

//QQ登录
    UIButton *buttonQQ = [[UIButton alloc] init];
    [viewOfWay addSubview:buttonQQ];
    [buttonQQ setTitle:@"QQ账号登录" forState:UIControlStateNormal];
    [buttonQQ setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    buttonQQ.titleLabel.font = [UIFont systemFontOfSize:15];
    
    [buttonQQ addTarget:self action:@selector(handleQQ) forControlEvents:UIControlEventTouchUpInside];
    
    
    [buttonQQ mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(imageOfQQ.mas_bottom).with.offset(10);
        make.centerX.equalTo(imageOfQQ.mas_centerX);
        make.height.offset(20);
        
    }];

//累
    UILabel *label1 = [[UILabel alloc] init];
    [viewOfWay addSubview:label1];
    
    label1.backgroundColor = COLOR;
    
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(buttonXinLang).with.offset(40);
        make.left.equalTo(viewOfWay).with.offset(20);
        make.right.equalTo(viewOfWay).with.offset(-20);
        make.height.offset(1);
        
        
    }];
//巨累
    UILabel *label2 = [[UILabel alloc] init];
    [viewOfWay addSubview:label2];
    label2.text = @"没有账号?";
    label2.font = [UIFont systemFontOfSize:15];
    
    
    [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(buttonXinLang).with.offset(60);
        make.left.equalTo(viewOfWay).with.offset(20);
        make.bottom.equalTo(viewOfWay).with.offset(-20);
        make.width.offset(75);
        
        
    }];

//没谁了
    UIButton *buttonWangYi = [[UIButton alloc] init];
    
    [viewOfWay addSubview:buttonWangYi];
    
    [buttonWangYi setTitle:@"注册网易邮箱" forState:UIControlStateNormal];
    [buttonWangYi setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    buttonWangYi.titleLabel.font = [UIFont systemFontOfSize:15];
    [buttonWangYi setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [buttonWangYi addTarget:self action:@selector(handleWangYi) forControlEvents:UIControlEventTouchUpInside];
    
    
    [buttonWangYi mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(buttonXinLang).with.offset(60);
        make.bottom.equalTo(viewOfWay).with.offset(-20);
        make.left.equalTo(label2.mas_right).with.offset(2);
        make.right.equalTo(viewOfWay).with.offset(-10);
    }];

//终于写完了 fake
    
    
    
    [viewOfRegister release];
    [imageOfAccount release];
    [imageOfSuo release];
    [labelOfOneH release];
    [textOfRegister release];
    [textOfSuo release];
    [buttonRegister release];
    [labelOfOther release];
    [viewOfWay release];
    [imageXinLang release];
    [imageWeiXin release];
    [imageOfQQ release];
    [buttonWeiXin release];
    [buttonXinLang release];
    [buttonQQ release];
    [label1 release];
    [label2 release];
    [buttonWangYi release];
    
}
- (void)handleBack{
    [self.navigationController popToViewController:self.navigationController.viewControllers[0] animated:YES];


}
- (void)handleWeiXin{

}
- (void)handleXinLang{
    UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToSina];
    
    snsPlatform.loginClickHandler(self,[UMSocialControllerService defaultControllerService],YES,^(UMSocialResponseEntity *response){
        
        //          获取微博用户名、uid、token等
        
        if (response.responseCode == UMSResponseCodeSuccess) {
            //删除授权调用下面的方法
            [[UMSocialDataService defaultDataService] requestUnOauthWithType:UMShareToSina  completion:^(UMSocialResponseEntity *response){
                NSLog(@"response is %@",response);
            }];
            
            
            UMSocialAccountEntity *snsAccount = [[UMSocialAccountManager socialAccountDictionary] valueForKey:snsPlatform.platformName];
            NSLog(@"\nusername = %@,\n usid = %@,\n token = %@ iconUrl = %@,\n unionId = %@,\n thirdPlatformUserProfile = %@,\n thirdPlatformResponse = %@ \n, message = %@",snsAccount.userName,snsAccount.usid,snsAccount.accessToken,snsAccount.iconURL, snsAccount.unionId, response.thirdPlatformUserProfile, response.thirdPlatformResponse, response.message);
        }});
    


}
- (void)handleQQ{
    UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:UMShareToQQ];
    
    snsPlatform.loginClickHandler(self,[UMSocialControllerService defaultControllerService],YES,^(UMSocialResponseEntity *response){
        
        //          获取微博用户名、uid、token等
        
        if (response.responseCode == UMSResponseCodeSuccess) {
            
            
            UMSocialAccountEntity *snsAccount = [[UMSocialAccountManager socialAccountDictionary] valueForKey:snsPlatform.platformName];
            NSLog(@"\nusername = %@,\n usid = %@,\n token = %@ iconUrl = %@,\n unionId = %@,\n thirdPlatformUserProfile = %@,\n thirdPlatformResponse = %@ \n, message = %@",snsAccount.userName,snsAccount.usid,snsAccount.accessToken,snsAccount.iconURL, snsAccount.unionId, response.thirdPlatformUserProfile, response.thirdPlatformResponse, response.message);
            
        }});


}
- (void)handleWangYi{


}


@end
