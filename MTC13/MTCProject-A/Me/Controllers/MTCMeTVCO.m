//
//  MTCMeTVCO.m
//  MTCProject-A
//
//  Created by dllo on 16/6/17.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCMeTVCO.h"
#import "MTCMeTVCCell.h"
#import "Masonry.h"
@interface MTCMeTVCO ()
@property (nonatomic, retain) NSArray *textArr;
@property (nonatomic, retain) NSArray *picArr;

@end

@implementation MTCMeTVCO

-(void)dealloc {
    [_textArr release];
    [_picArr release];
    [super dealloc];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self creatView];
    

    
    
}

- (void)creatView{

    [self.tableView registerClass:[MTCMeTVCCell class] forCellReuseIdentifier:@"pool"];
    self.tableView.backgroundColor = COLOR;
 
    self.textArr = @[@"我的消息", @"金币商城", @"我的任务", @"我的钱包", @"夜间模式", @"离线阅读", @"活动广场", @"我的邮箱", @"意见反馈"];
    self.picArr = @[@"menews.png", @"gold.png", @"task.png", @"wallet.png", @"night.png", @"download.png", @"readme.png", @"activity.png", @"mailbox"];
    
 
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{

    return 1;

}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return self.textArr.count;

}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MTCMeTVCCell *cell = [tableView dequeueReusableCellWithIdentifier:@"pool" forIndexPath:indexPath];
    
    cell.content = [self.textArr objectAtIndex:indexPath.row];
    cell.pic = [self.picArr objectAtIndex:indexPath.row];
    
   

    return cell;
    

}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 60;
    


}


@end
