//
//  MTCVideoCollectionViewCell.m
//  MTCProject-A
//
//  Created by dllo on 16/6/20.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCVideoCollectionViewCell.h"
#import "MTCVideoOfModel.h"
#import "Masonry.h"
#import "UIImageView+WebCache.h"


@interface MTCVideoCollectionViewCell()

@property (nonatomic, retain) UIImageView *topicImg;
@property (nonatomic, retain) UILabel *tname;
@property (nonatomic, retain) UILabel *title;


@end

@implementation MTCVideoCollectionViewCell
- (void)dealloc{

    [super dealloc];
    [_mp4_url release];
    [_topicImg release];
    [_tname release];
    [_title release];




}


- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self config];
    }
    return self;

}
- (void)config {
    self.mp4_url = [[UIView alloc] init];
    self.topicImg = [[UIImageView alloc] init];
    self.tname = [[UILabel alloc] init];
    self.title = [[UILabel alloc] init];
    //多行显示
    [self.title setNumberOfLines:2];
    
    //字体
    self.tname.font = [UIFont systemFontOfSize:12];
    
    [self.contentView addSubview:self.mp4_url];
    [self.contentView addSubview:self.topicImg];
    [self.contentView addSubview:self.tname];
    [self.contentView addSubview:self.title];
    
    [self.mp4_url mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView);
        make.left.equalTo(self.contentView);
        make.right.equalTo(self.contentView);
        make.height.equalTo(self.mp4_url.mas_width).multipliedBy(3.0 / 4.0f);
        
        
    }];
    [self.title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mp4_url.mas_bottom).with.offset(10);
        make.left.equalTo(self.contentView).with.offset(10);
        make.right.equalTo(self.contentView).with.offset(-10);
        make.bottom.equalTo(self.contentView).with.offset(-40);
        
    }];
    [self.topicImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.title.mas_bottom).with.offset(10);
        make.left.equalTo(self.contentView).with.offset(10);
        make.bottom.equalTo(self.contentView).with.offset(-10);
        make.width.offset(20);
        
    }];
    [self.tname mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.title.mas_bottom).with.offset(10);
        make.bottom.equalTo(self.contentView).with.offset(-10);
        make.left.equalTo(self.topicImg.mas_right).with.offset(10);
        make.width.offset(80);
        
    }];
    
    [_mp4_url release];
    [_title release];
    [_tname release];
    [_topicImg release];
    


}
- (void)setModel:(MTCVideoOfModel *)model{
    self.title.text = model.title;
    self.tname.text = model.tname;
    
    [self.topicImg sd_setImageWithURL:[NSURL URLWithString:model.topicImg]];
    
    





}







@end
