//
//  MTCVideoOfModel.h
//  MTCProject-A
//
//  Created by dllo on 16/6/20.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MTCVideoOfModel : NSObject
@property (nonatomic, copy) NSString *mp4_url;
@property (nonatomic, copy) NSString *tname;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *topicImg;



@end
