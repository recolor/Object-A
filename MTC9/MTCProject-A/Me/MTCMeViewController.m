//
//  MTCMeViewController.m
//  MTCProject-A
//
//  Created by dllo on 16/6/14.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCMeViewController.h"
#import "Masonry.h"
#import "MTCMeTVCO.h"


@interface MTCMeViewController ()

@end

@implementation MTCMeViewController
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{

    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self config];
    }
    return self;

}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self creadView];
    [self handleButton];
    
    
}

- (void)config {
    self.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"我" image:[UIImage imageNamed:@"me.png"]  tag:500];
    


}
//控件太多!!!!!!!!!!!! 累
- (void)creadView {

    UIView *viewOfHead = [[UIView alloc] init];
    viewOfHead.backgroundColor = [UIColor redColor];
//    self.view.backgroundColor = [UIColor colorWithRed:244 / 255.0f green:244 / 255.0f blue:244 / 255.0f alpha:1];
    
    [self.view addSubview:viewOfHead];
    
    [viewOfHead mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).with.offset(20);
        make.right.equalTo(self.view).with.offset(0);
        make.left.equalTo(self.view).with.offset(0);
        make.height.offset(200);
        
        
    }];
    [viewOfHead release];

    UIButton *setButton = [UIButton buttonWithType:UIButtonTypeCustom];

    [setButton setImage:[UIImage imageNamed:@"set.png"] forState:UIControlStateNormal];
    
    [self.view addSubview:setButton];
    [setButton release];
    [setButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).with.offset(40);
        make.right.equalTo(self.view).with.offset(-20);
        make.height.offset(20);
        make.width.offset(20);
        
    }];
    
    UIImageView *headImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"head1.png"]];
    [self.view addSubview:headImage];
    [headImage release];
    [headImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(viewOfHead);
        make.top.equalTo(viewOfHead).with.offset(20);
        make.size.mas_equalTo(CGSizeMake(80, 80));
        
    }];
    
    UIButton *registerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [registerButton setTitle:@"立即登录" forState:UIControlStateNormal];
    [self.view addSubview:registerButton];
    [registerButton release];
    [registerButton addTarget:self action:@selector(handleButton) forControlEvents:UIControlEventTouchUpInside];
    
    
    [registerButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(headImage.mas_bottom).with.offset(10);
        make.centerX.equalTo(viewOfHead);
        make.height.offset(20);
        
    }];
    UILabel *laWin = [[UILabel alloc] init];
    laWin.text = @"赢积分 抢大礼!";
    laWin.textColor = [UIColor colorWithRed:255 /255.0 green:215 / 255.0 blue:88 / 255.0 alpha:1];
    [self.view addSubview:laWin];
    [laWin release];
    
    [laWin mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(registerButton.mas_bottom).with.offset(10);
        make.centerX.equalTo(viewOfHead);
        make.height.offset(20);
        
    }];
    NSArray *arr = @[ @"readme.png", @"heartme.png", @"writeme.png", @"goldme.png"];
    for (int i = 0; i < 4; i++) {
        UIButton *buttonfour = [[UIButton alloc] init];
        buttonfour.frame = CGRectMake(SCREEN_WIDTH / 4 * i, 220, SCREEN_WIDTH / 4, 80);
        [self.view addSubview:buttonfour];
        buttonfour.backgroundColor = [UIColor whiteColor];
        [buttonfour setImage:[UIImage imageNamed:arr[i]] forState:UIControlStateNormal];
        
    }
    MTCMeTVCO *tableView = [[MTCMeTVCO alloc] init];
    
    [self.view addSubview:tableView.view];
    tableView.view.frame = CGRectMake(0, 300, SCREEN_WIDTH, 540);
    
    


}
//登录页面?? 没弄 第三方
- (void)handleButton{
    
    
    
 
}




//电量等等 风格
- (UIStatusBarStyle)preferredStatusBarStyle {
    
    return UIStatusBarStyleLightContent;
    
}







@end
