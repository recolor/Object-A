//
//  MTCMeTVCO.m
//  MTCProject-A
//
//  Created by dllo on 16/6/17.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCMeTVCO.h"
#import "MTCMeTVCCell.h"
#import "Masonry.h"
@interface MTCMeTVCO ()
@property (nonatomic, retain) NSArray *textArr;
@property (nonatomic, retain) NSArray *picArr;

@end

@implementation MTCMeTVCO

-(void)dealloc {
    [_textArr release];
    [_picArr release];
    [super dealloc];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self creatView];
    
    
    
}

- (void)creatView{

    [self.tableView registerClass:[MTCMeTVCCell class] forCellReuseIdentifier:@"pool"];
 
    self.textArr = @[@"我的消息", @"金币商城", @"我的任务", @"我的钱包", @"夜间模式", @"离线阅读", @"活动广场", @"我的邮箱", @"意见反馈"];
    self.picArr = @[@"menews.png", @"gold.png", @"task.png", @"wallet.png", @"night.png", @"download.png", @"readme.png", @"activity.png", @"mailbox", @"opinion.png"];
    
//    NSArray *picArr = @[@"menews.png", @"gold.png", @"task.png", @"wallet.png", @"night.png", @"download.png", @"readme.png", @"activity.png", @"mailbox", @"opinion.png"];
//    for (int i = 0; i < 9; i++) {
//     
//            
//            UIImageView *imageViewOfMe = [[UIImageView alloc] init];
//            
//        
//            [imageViewOfMe release];
//            
//            imageViewOfMe.image = [UIImage imageNamed:picArr[i]];
//            
//            [imageViewOfMe mas_makeConstraints:^(MASConstraintMaker *make) {
//                make.top.equalTo(self.view).with.offset(10);
//                make.left.equalTo(self.view).with.offset(10);
//                make.bottom.equalTo(self.view).with.offset(-10);
//                make.width.offset(20);
//                
//                
//            }];
//            UILabel *labelOfMe = [[UILabel alloc]init];
//            
//        
//            [labelOfMe release];
//            labelOfMe.text = self.textArr[i];
//            
//        
//            [labelOfMe mas_makeConstraints:^(MASConstraintMaker *make) {
//                make.top.equalTo(self.view).with.offset(10);
//                make.left.equalTo(imageViewOfMe.mas_right).with.offset(10);
//                make.bottom.equalTo(self.view).with.offset(-10);
//                make.width.offset(80);
//                
//            }];
//            
//      
//    }
 
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{

    return 1;

}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return self.textArr.count;

}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MTCMeTVCCell *cell = [tableView dequeueReusableCellWithIdentifier:@"pool" forIndexPath:indexPath];
    
    cell.content = [self.textArr objectAtIndex:indexPath.row];
    cell.pic = [self.picArr objectAtIndex:indexPath.row];
    

    return cell;
    

}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 60;
    


}


@end
