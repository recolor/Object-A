//
//  MTCReadThreeCell.m
//  MTCProject-A
//
//  Created by dllo on 16/6/23.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCReadThreeCell.h"
#import "Masonry.h"
#import "MTCReadModelTwo.h"
#import "UIImageView+WebCache.h"

@interface  MTCReadThreeCell()

@property (nonatomic, retain) UILabel *title;
@property (nonatomic, retain) UILabel *source;


@end

@implementation MTCReadThreeCell
-(void)dealloc{
    [super dealloc];
    [_image1 release];
    [_image2 release];
    [_image3 release];
    [_title release];
    [_source release];
    

}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self config];
    }
    return self;

}
- (void)config{
    self.image1 = [[UIImageView alloc] init];
    self.image2 = [[UIImageView alloc] init];
    self.image3 = [[UIImageView alloc] init];
    self.title = [[UILabel alloc] init];
    self.source = [[UILabel alloc] init];
    self.source.font = [UIFont systemFontOfSize:12];
    [self.title setNumberOfLines:2];
    
    [self.image1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).with.offset(5);
        make.left.equalTo(self.contentView).with.offset(5);
        make.bottom.equalTo(self.contentView).with.offset(-130);
        make.width.equalTo(self.image1.mas_height).multipliedBy(3.0 / 4.0f);
        
    }];
    [self.image2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).with.offset(5);
        make.left.equalTo(self.image1.mas_right).with.offset(5);
        make.right.equalTo(self.contentView).with.offset(-5);
        make.bottom.equalTo(self.image1.mas_centerY).with.offset(-5);
        
    }];
    [self.image3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.image2.mas_bottom).with.offset(5);
        make.left.equalTo(self.image1.mas_right).with.offset(5);
        make.right.equalTo(self.contentView).with.offset(-5);
        make.bottom.equalTo(self.contentView).with.offset(-130);
    }];
    [self.title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.image1.mas_bottom).with.offset(10);
        make.left.equalTo(self.contentView).with.offset(10);
        make.right.equalTo(self.contentView).with.offset(-10);
        make.height.offset(80);
        
    }];
    [self.source mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.title.mas_bottom).with.offset(10);
        make.left.equalTo(self.contentView).with.offset(10);
        make.bottom.equalTo(self.contentView).with.offset(-10);
        make.width.offset(80);
        
    }];

    [_image1 release];
    [_image2 release];
    [_image3 release];
    [_title release];
    [_source release];






}

- (void)setModel:(MTCReadModelTwo *)model{
    if (_model != model) {
        [_model release];
        [_model retain];
    }
    self.title.text = model.title;
    self.source.text = model.source;


}
























@end
