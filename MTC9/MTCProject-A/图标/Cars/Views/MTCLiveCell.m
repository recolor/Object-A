//
//  MTCLiveCell.m
//  MTCProject-A
//
//  Created by dllo on 16/6/23.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTCLiveCell.h"
#import "Masonry.h"
#import "MTCLiveModel.h"
#import "UIImageView+WebCache.h"

@interface MTCLiveCell()

@end


@implementation MTCLiveCell
- (void)dealloc{
    [super dealloc];
    

}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self config];
    }
    return self;

}

- (void)config{
   



}
- (void)setModel:(MTCLiveModel *)model{
    if (_model != model) {
        [_model release];
        [_model retain];
    }
    

}














@end
