//
//  pictureScrollView.m
//  UI06_UIScrollView
//
//  Created by Apple on 16/5/10.
//  Copyright © 2016年 Apple. All rights reserved.
//

#import "pictureScrollView.h"

@implementation pictureScrollView
- (instancetype )initWithFrame:(CGRect)frame{
    self = [super initWithFrame:  frame]
    ;
    if (self) {
        [self config];
    }
    return self;
}


-(void)config{
    self.showsHorizontalScrollIndicator = NO;
    self.pagingEnabled = YES;
    //缩放范围
    self.minimumZoomScale = 0.5;
    self.maximumZoomScale = 2;
}




- (void)addPictures:(NSArray <UIImage *>*)pictures{
    self.contentSize = CGSizeMake(CGRectGetWidth(self.bounds)*pictures.count, CGRectGetHeight(self.bounds));
    for (int i = 0; i < pictures.count; i++) {
        UIImageView *imageView =[[UIImageView alloc]initWithImage:pictures[i]];
        imageView.frame = CGRectMake(CGRectGetWidth(self.bounds) * i, 0, CGRectGetWidth(self.bounds), CGRectGetHeight(self.bounds));
        [self addSubview:imageView];
        [imageView release];
    }
}

@end
