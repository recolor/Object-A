//
//  MTC163NewsNormalCell.m
//  MTCProject-A
//
//  Created by dllo on 16/6/15.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "MTC163NewsNormalCell.h"
#import "Masonry.h"
#import "UIImageView+WebCache.h"
#import "MTC163NewsChannelModel.h"
@interface MTC163NewsNormalCell()

//标题
@property (nonatomic, retain) UILabel *title;
//新闻热点
@property (nonatomic, retain) UILabel *source;
//跟帖
@property (nonatomic, retain) UILabel *count;
//小图
@property (nonatomic, retain) UIImageView *img;


@end
@implementation MTC163NewsNormalCell

- (void)dealloc{
    [super dealloc];
    [_model release];
    [_title release];
    [_source release];
    [_count release];
    [_img release];
    
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self= [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self configCell];
        
    }
    return self;
    
    
    
}

- (void)configCell {
    self.img = [[UIImageView alloc] init];
    self.title = [[UILabel alloc] init];
    
    
    self.count = [[UILabel alloc] init];
    self.source = [[UILabel alloc] init];
    [self.contentView addSubview:self.img];
    [self.contentView addSubview:self.title];
    [self.contentView addSubview:self.count];
    [self.contentView addSubview:self.source];
    self.title.font = [UIFont systemFontOfSize:15];
    [self.title setNumberOfLines:2];
    
    self.source.font = [UIFont systemFontOfSize:12];
    self.count.font = [UIFont systemFontOfSize:12];
    
    [self.img mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).with.offset(10);
        make.left.equalTo(self.contentView.mas_left).with.offset(10);
        make.bottom.equalTo(self.contentView.mas_bottom).with.offset(-10);
        make.width.equalTo(self.img.mas_height).multipliedBy(4.0 / 3.0f);
        
    }];
    
    [self.title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.img.mas_right).with.offset(10);
        make.right.equalTo(self.contentView.mas_right).with.offset(-10);
        make.top.equalTo(self.contentView.mas_top).with.offset(10);
        make.bottom.equalTo(self.contentView.mas_centerY).with.offset(10);
    }];
    
    [self.source mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(100, 20));
        make.left.equalTo(self.img.mas_right).with.offset(10);
        make.top.equalTo(self.title.mas_bottom).with.offset(10);
    }];
    
    [self.count mas_makeConstraints:^(MASConstraintMaker *make) {

        make.right.equalTo(self.contentView.mas_right).with.offset(-10);
        make.top.equalTo(self.title.mas_bottom).with.offset(10);
        make.size.mas_equalTo(CGSizeMake(100, 20));
        
    }];
    
    [_img release];
    [_title release];
    [_source release];
    [_count release];
    
    
    
    
}




- (void)setModel:(MTC163NewsChannelModel *)model{
    if (_model != model) {
        [_model release];
        [_model retain];
    }

    
    self.title.text = model.title;
    self.title.numberOfLines = 0;
    self.count.text = [model.replyCount.description stringByAppendingString:@"跟帖"];
    self.count.textAlignment = NSTextAlignmentRight;
    
    [self.img sd_setImageWithURL:[NSURL URLWithString:model.img] placeholderImage:nil];
    self.img.layer.masksToBounds = YES;
    self.img.contentMode = UIViewContentModeScaleToFill;
    if (![model.recSource isEqualToString:@"#"]) {
        
        self.source.text = model.recSource;
    }
    
}



//- (void)dsadasd{
//
//    UIView *view = [[UIView alloc] init];
//    [self.contentView addSubview:view];
//    [view mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.contentView).with.offset(0);
//        make.left.equalTo(self.contentView).with.offset(10);
//        make.right.equalTo(self.contentView).with.offset(-10);
//        make.bottom.equalTo(self.contentView).with.offset(0);
//
//    }];
//    [view release];
//
//
//
//}





- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


@end
